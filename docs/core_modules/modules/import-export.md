Модуль import-export
==============
*версия 1.0.0*
### Общее описание
Модуль **import-export** предназначен для гибкого импорта данных сущности ActiveRecord из экселя в БД и экспорта данных ActiveRecord из БД в эксель. Основные возможности модуля:

**Общее**
 1. Быстрая настройка с помощью написания конфига для экспорта/импорта
 2. Гибкая обработка данных при импорте/экспорте с помощью вызова заранее определенных функций
 3. Процесс импорта/экспорта происходит в консольном контролере
 
**Импорт в БД:**
 1. Возможность импортировать данные нескольких сущностей ActiveRecord с одного excel файла
 2. Графический интерфейс для загрузки excel файлов при импорте с выбором нужного конфига
 3. Экран preview для предпросмотра данных из excel и установки соответствия  "поле конфига" - "колонка excel"
 4. Экран лога процесса импорта
 
**Экспорт в excel:**
 1. Удобный экспорт в эксель через запрос ActiveQuery (пример, Model::find()->export($configName))
 2. Возможность передачи внешних данных, которые будут учитываться при экспорте в эксель
 3. Простое обращение к данным связанных моделей
 4. Возможность генерировать динамический набор колонок и их значений
 5. Экран лога процесса экспорта

Содержит backend, console части.

### Процесс установки

- Import modules
- Поднять миграциии ./yii migrate
- Добавить в composer.json:
```
"moonlandsoft/yii2-phpexcel":"*",
"vova07/yii2-console-runner-extension":"*",
```
- Выполнить команду composer update
- Добавить конфиг для компонента отслеживания сообщений импорта/экспорта в раздел components файла /backend/config/main.php:
```
	'CheckImportExportMessage'=> [
	    'class'=>'backend\modules\importExport\components\CheckImportExportMessage'
	],
```
и добавить его в массив bootstrap того же файла: 
```
	'bootstrap' => [<...some vars...> , 'CheckImportExportMessage'],
```
### Импорт в БД

##### Быстрый старт
Для импорта в БД нам необходимо прописать конфиг возвращаемый функцией importConfig() в файле: /backend/modules/importExport/config/ImportExportConfig.php
Пример конфига: 
```
protected static function importConfig()
    {
        return [
            [
                'label' => 'Product Import',
                'columns' => [
                    [
                        Product::className(),
                        ImportExportBaseConfig::IMPORT_COLUMN_MODEL_IDENTIFIER,
                        'sku',
                    ],
                    [
                        Product::className(),
                        ImportExportBaseConfig::IMPORT_COLUMN_FIELD,
                        'short_desc',
                    ],
                ],
            ],
        ];
    }
```
Прописывая данный конфиг, мы определяем ключевое поле модели Product, с помощью указания типа поля IMPORT_COLUMN_MODEL_IDENTIFIER, по которому будем идентифицировать "модель в БД" -> "запись в excel"
И обычное поле short_desc модели.

После чего, перейдя в пункт меню в админке: Modules -> Import / Export -> Import to DB в выпадающем списке выбора конфига появится пункт Product Import с возможностью загрузить файл для него. И в последующем окне предпросмотра можно будет выбрать какая колонка excel отвечает за поле sku, а какая за поле short_desc.

**Обратите внимание:** Если в конфиге будут допущены ошибки, то при заходе в меню Import to DB вылетит 501 эксепшн с описанием ошибки

##### Детальное описание
**формат конфига:**

```
return [
   [
       'label' => 'ConfigName',  //Заголовок конфига в выпадающем списке в окне Import to DB
       'columns' => [            //Список колонок для импорта (порядок не важен)
           [
               Model::className()                                  //Имя класса модели
               ImportExportBaseConfig::COLUMN_MODEL_IDENTIFIER,    //Тип колонки
               'field_or_function',                      //Поле или функция модели
               [                                         //НЕ ОБЯЗАТЕЛЬНЫЙ массив настроек
                   'excelColumnLabel' => 'Field',                    //Заголовок колонки excel для автоматического выбора на preview
                   'previewDropdownLabel' => 'Field or Func',        //Заголовк поля в выпадающем списке на preview
               ]
           ],
           [
            // ...
            // Следующий конфиг колонки
            // ...
           ]
       ],
       'options' => [                                              //НЕ ОБЯЗАТЕЛЬНЫЙ массив общих настроек
            'onFound' => ImportExportBaseConfig::ON_FOUND_SKIP,    // default: ON_FOUND_UPDATE - если объект из эксель найден - пропускаем или обновляем
            'onSaveError' => ImportExportBaseConfig::ON_SAVE_ERROR_CONTINUE,   // default: ON_SAVE_ERROR_TERMINATE - если произошла ошибка при сохранении в одном объекте - пытаемся сохранить другие или сразу останавливаем импорт 
        ]
   ]
];
```
**описание некоторых полей:**
*тип колонки*: может быть следуюших значений: 
- IMPORT_COLUMN_MODEL_IDENTIFIER - идентификатор модели для нахождения соотвевия "модель в БД" -> "запись в excel". На каждую модель ActiveRecord  в конфиге должна быть одна колонка идентификатор
- IMPORT_COLUMN_FIELD - обычное поле модели, когда необходимо сопоставить "значение ячейкм excel" - "поле модели"
- IMPORT_COLUMN_FUNCTION_AFTER_SAVE - функция для гибкой обработки значения excel, которая выполняется после сохранением модели
- IMPORT_COLUMN_FUNCTION_BEFORE_SAVE - функция для гибкой обработки значения excel, которая выполняется перед сохранением модели

В конфиге указывается только название функции IMPORT_COLUMN_FUNCTION_AFTER_SAVE или IMPORT_COLUMN_FUNCTION_BEFORE_SAVE без круглых скобок, в моделе описывается тело функции. Функция принимает в себя 2 параметра: $excelValue - значение ячейки excel, $excelColumn - значение заголовка колонки excel
*пример функции*:

```
protected static function importConfig()
{
...
 [
    Product::className(),
    ImportExportBaseConfig::IMPORT_COLUMN_FUNCTION_BEFORE_SAVE,
    'setLabelAndAlias',
    [
        'excelColumnLabel' => 'Название',
        'previewDropdownLabel' => 'Label',
    ]
  ],
...
}

class Product extends ActiveRecord  
{
...
 public function setLabelAndAlias($excelValue, $excelColumn){
        $this->label = $excelValue;
        $this->alias = Inflector::transliterate($excelValue);
    }
}
```

### Экспорт в excel

##### Быстрый старт
1. В моделе, данные которой мы хотим экспортировать в excel подключаем trait: 
 **use ExportTrait;**
2. Для импорта в БД нам необходимо прописать конфиг возвращаемый функцией exportConfig() в файле: /backend/modules/importExport/config/ImportExportConfig.php
Пример конфига:
```
protected static function exportConfig()
{
    return [
        [
            'label' => 'ExportConfigLabel',
            'columns' =>
                [
                    [
                        ImportExportBaseConfig::EXPORT_COLUMN_FILED,
                        'sku',
                    ],
                    [
                        ImportExportBaseConfig::EXPORT_COLUMN_FILED,
                        'packing[0]->label',
                    ],
                ]
        ]
    ];
}
```
3. Прописываем ActiveQuery запрос с вызовом завершающей функции ->export
```
Product::find()->with('packing')->export('ExportConfigLabel');
```
Экспорт запускается в фоновом режиме в консоле, по его завершению появится Flash сообщение.

##### Детальное описание
**формат конфига:**
```
return [
 [
     'label' => 'ExportConfigName',            //Название конфига
     'columns' => [                            //Набор колонок
         [
             ImportExportBaseConfig::EXPORT_COLUMN_FILED,        //Тип колонки
             'model_or_function',                                //Поле или функция модели
             [                                                   //НЕ ОБЯЗАТЕЛЬНЫЕ опции
                 'excelColumnLabel' => 'SKU',                    //Заголовок колонки Excel
             ]
         ],
     ],
 ]
];
```

*тип колонки*: может быть следуюших значений: 
- EXPORT_COLUMN_FILED - обычное поле модели
- EXPORT_COLUMN_FUNCTION - функция для гибкой обработки данных модели, должна возвращать тип string
- EXPORT_COLUMN_DYNAMIC - применяется когда необходимо сгенерировать диапазон колонок/значений не связанных на прямую с полями модели. В таком случае вторым аргкментом конфига колонки выступает массив с ключами: 'columnsLabelFunction' и 'valueByColumnNameFunction', значениями которых, являются соответвенно функции модели для получения списка колонок и для получения значения колонки по ее названию.  

**пример генерации диапазона колонок:**
```
protected static function exportConfig()
{
...
[
    ImportExportBaseConfig::EXPORT_COLUMN_DYNAMIC,
    [
        'columnsLabelFunction' => 'getColumnsLabel',
        'valueByColumnNameFunction' => 'getColumnsValue',
    ],
]
...
}


class Product extends ActiveRecord  
{
...
    public function getColumnsLabel(){
        return ['columnLabel1', 'columnLabel2'];
    }

    public function getColumnsValue($columnLabel){
        return $columnLabel.' some column value';
    }
}
```

**работа со связанными моделями в конфиге:**
Если надо получить данные связанной модели, в конфиге просто можно указать связь, так если б вы указывали через операнд -> при работе с переменными.
Пример:
```
protected static function exportConfig()
{
    ...
        [
            ImportExportBaseConfig::EXPORT_COLUMN_FILED,
            'packing[0]->label',
        ],
    ...
}
```

Будет получено значение label первого объекта Packing в массиве (аналогично обычно в коде $product->packing[0]->label)

**передача внешних данных в экспорт:**
Если для экспорта необходимы сторонние данные, их можно просто передать в функцию ->export() после названия конфига.
*пример:*
```
  $param1 = 'some string';
  $param2 =  Product::find()->one();
  Product::find()->with('packing')->export('ExportConfigName', $param1, $param2);
```
Параметров может быть неограниченное кол-во. Все они будут передаваться в функции типов колонки: EXPORT_COLUMN_FUNCTION и обе функции EXPORT_COLUMN_DYNAMIC дополнительными аргументами.
**важно:** объекты в функции приходят как **массивы** 