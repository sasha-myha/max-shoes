Модуль socialShareContent
=========================

Модуль содержит SocialShareContentBehavior, позволяющий сохранять контент для шейринга в социальных сетях.
Работает как с обычными моделями, так и с моделями StaticPage.

Как установить?
Подключаем модуль и поднимаем миграции.

Как использовать?
1. В модели на бэке добавить behavior
```php
public function behaviors()
{
    return [
        'socialShareContent' => [
            'class' => SocialShareContentBehavior::className(),
        ],
    ];
}
```
После этого в редактировании записи, добавится вкладка Social Share Content.
2. На фронте в экшене добавить код
```php
SocialShareContentRegister::register($model);
```
После этого, в head страницы добавятся метатеги для социальных сетей.

Модуль состоит из backend, common и frontend частей.
