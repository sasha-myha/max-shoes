<?php

use console\components\Migration;

/**
 * Class m200724_124817_create_monsterleads_request_table migration
 */
class m200724_124817_create_monsterleads_request_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%monsterleads_request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'monsterleads_api_status' => $this->tinyInteger()->notNull()->comment('1-new, 2-send, 3-error'),
                'type' => $this->integer()->notNull(),
                'phone' => $this->string()->null(),
                'name' => $this->string()->null(),
                'additional_info' => $this->text()->null(),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
