<?php

use console\components\Migration;

/**
 * Class m170518_081227_create_file_meta_data_table_translation migration
 */
class m170518_081227_create_file_meta_data_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%file_meta_data_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%file_meta_data}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'alt' => $this->string()->comment('Alt'),
            ],
            $this->tableOptions
        );


        $this->addPrimaryKey('pk-file_meta_data_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-file_meta_data_translation-model_id-file_meta_data-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

