<?php

use console\components\Migration;

/**
 * Class m170117_052859_create_builder_widget_attribute_table migration
 */
class m170117_052859_create_builder_widget_attribute_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%builder_widget_attribute}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'widget_id' => $this->integer()->notNull(),
                'attribute' => $this->string()->notNull(),
                'value' => $this->text()->null(),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-builder_widget_attribute-widget_id-builder_widget-id',
            $this->tableName,
            'widget_id',
            '{{%builder_widget}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-builder_widget_attribute-widget_id-builder_widget-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
