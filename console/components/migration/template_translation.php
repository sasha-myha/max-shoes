<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $tableName string the new migration table name */

echo "<?php\n";
?>

use console\components\Migration;

/**
 * Class <?= $className ?> migration
 */
class <?= $className ?> extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%<?= $tableName . '_translation' ?>}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%<?= $tableName ?>}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                // examples:
                //'label' => $this->string()->comment('Label'),
                //'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        <?php
            $fkTableName = $tableName;
            if (strlen($tableName) > 19) {
                if (strpos($tableName, '_') !== false) {
                    $fkTableName = makeFkTableName('_', $tableName);
                } elseif (strpos($tableName, '-') !== false) {
                    $fkTableName = makeFkTableName('-', $tableName);
                }
            }
        ?>

        $this->addPrimaryKey('pk-<?= $fkTableName . '_translation' ?>', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-<?= $fkTableName . '_translation' ?>-model_id-<?= $fkTableName ?>-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

<?php
    function makeFkTableName($delimiter, $tableName) {
        $tableNameArr = explode($delimiter, $tableName);
        $fkTableNameArr = [];
        foreach ($tableNameArr as $value) {
            array_push($fkTableNameArr, $value[0]);
        };

        return implode($delimiter, $fkTableNameArr);
    }
?>
