<?php
namespace console\controllers;

use common\models\MonsterleadsRequest;
use GuzzleHttp\Client;
use yii\console\Controller;

/**
 * Class IpController
 *
 * @package console\controllers
 */
class MonsterLeadsApiController extends Controller
{
    /**
     * @param int $timeout
     */
    public function actionSend()
    {
        $models = MonsterleadsRequest::find()
            ->where([
                'type' => MonsterleadsRequest::TYPE_RANDEVY,
                'monsterleads_api_status' => MonsterleadsRequest::MONSTERLEADS_API_NEW
            ])
            ->all();

        $client = new Client([
            'base_uri' => 'http://api.monsterleads.pro/method/',
        ]);

        foreach ($models as $model) {

            $response = $client->request('POST', 'order.add', [
                'form_params' => [
                    'format'  => 'json',
                    'api_key' => '7a4f3181c3cb6b5476fbceacfbdc8718',
                    'client'     => $model->name,
                    'tel'     => $model->phone,
                    'ip'     => $model->additional_info,
                    'tel'     => $model,
                ]
            ]);
        }

    }
}
