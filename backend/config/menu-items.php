<?php

return [
    require(__DIR__ . '/core_modules/menu-items.php'),
    [
        'label' => 'settings',
        'items' => [
            [
                'label' =>'configurations',
                'url' => ['/configuration/default/index'],
            ],
            [
                'label' => 'page seo',
                'url' => ['/seo/page-seo/index'],
            ],
            [
                'label' => 'internationalization',
                'items' => [
                    [
                        'label' => 'translations',
                        'url' => ['/i18n/default/index'],
                    ],
                    [
                        'label' => 'languages',
                        'url' => ['/language/language/index'],
                    ],
                ],
            ],
        ],
    ],
    [
        'label' => 'User',
        'items' => [
            [
                'label' => 'user',
                'url' => ['/admin/user/index'],
            ],
            [
                'label' => 'IP log',
                'url' => ['/admin/ip-auth-log/index'],
            ],
            [
                'label' => 'IP block',
                'url' => ['/admin/ip-block/index'],
            ],
        ]
    ],
    [
        'label' => 'MonsterLeads',
        'items' => [
            [
                'label' => 'Randevy',
                'url' => ['/mosterleads/monsterleads-request/index'],
            ],
        ]
    ],
];
