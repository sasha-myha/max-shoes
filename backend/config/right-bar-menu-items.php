<?php

return [
    [
        'label' => 'SEO tags',
        'url' => ['/meta/tag/index'],
    ],
    [
        'label' => 'Robots.txt',
        'url' => ['/seo/robots/index'],
    ],
    [
        'label' => 'Redirects',
        'url' => ['/redirects/redirects/index'],
    ],
    [
        'label' => 'Logout',
        'url' => ['/admin/default/logout'],
        'linkOptions' => ['data-method' => 'post']
    ]
];
