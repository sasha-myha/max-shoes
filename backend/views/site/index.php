<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?= \tolik505\tree\TreeWidget::widget([
        'items' => require(Yii::getAlias('@backend') . '/config/menu-items.php'),
        'options' => [
            'minOpenLevels' => 5
        ]
    ]); ?>
</div>
