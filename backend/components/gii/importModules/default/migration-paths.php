<?php
/**
 * @var $modules array
 */

echo "<?php\n";
?>

return [
<?php foreach ($modules as $moduleId): ?>
    '@backend/modules/<?= $moduleId ?>/migrations',
<?php endforeach; ?>
];
