<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\importModules;

use backend\components\gii\exportModules\BaseModule;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Class CoreModule
 *
 * @package backend\components\gii\exportModules
 */
class CoreModule extends BaseModule
{
    /**
     * @return array
     */
    public function getFilesForImport()
    {
        $files = [];
        foreach ($this->parts as $part) {
            $files = ArrayHelper::merge($files, $part->getFilesGetter()->getFilesForImport());
        }

        return $files;
    }
}
