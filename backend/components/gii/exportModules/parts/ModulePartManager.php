<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;

use backend\components\gii\exportModules\BaseModule;
use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\ModuleDirectoryFilesGetter;
use Yii;

/**
 * Class ModulePartManager
 *
 * @package backend\components\gii\exportModules\parts
 */
abstract class ModulePartManager
{
    /**
     * @var BaseModule
     */
    public $module;

    /**
     * @var FilesGetter
     */
    public $filesGetter;

    /**
     * ModulePartManager constructor.
     *
     * @param BaseModule $module
     */
    public function __construct(BaseModule $module)
    {
        $this->module = $module;
        $this->filesGetter = $this->getFilesGetter();
    }

    /**
     * @return string
     */
    abstract public function getAppModulePartPath();

    /**
     * @return string
     */
    abstract public function getCoreModulePartPath();

    /**
     * @return FilesGetter
     */
    abstract public function getFilesGetter(): FilesGetter;

    /**
     * @return string
     */
    public function getCoreModulePartFullPath()
    {
        return Yii::getAlias('@root') . '/core_modules/' . $this->module->id . '/' . $this->getCoreModulePartPath();
    }

    /**
     * @param string $path
     */
    public function remove($path = null)
    {
        if ($this->filesGetter instanceof ModuleDirectoryFilesGetter) {
            $path = $path ?? $this->getAppModulePartPath();
            $files = glob($path . '/*');
            foreach ($files as $file) {
                is_dir($file) ? $this->remove($file) : unlink($file);
                $this->module->log[] = "$file was removed";
            }
            if (is_dir($path)) {
                rmdir($path);
                $this->module->log[] = "$path was removed";
            }
        } else {
            foreach ($this->filesGetter->getFilesForExport() as $pathItem => $corePath) {
                unlink($pathItem);
                $this->module->log[] = "$pathItem was removed";
            }
        }
    }
}
