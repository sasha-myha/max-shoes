<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts\fileGetters;

use backend\components\gii\exportModules\parts\ModulePartManager;


/**
 * Class FilesGetter
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
abstract class FilesGetter
{
    /**
     * @var ModulePartManager
     */
    protected $modulePartManager;

    /**
     * FilesGetter constructor.
     *
     * @param ModulePartManager $modulePartManager
     */
    public function __construct(ModulePartManager $modulePartManager)
    {
        $this->modulePartManager = $modulePartManager;
    }

    /**
     * @return array
     */
    abstract public function getFilesForExport();

    /**
     * @param $path
     * @param array $paths
     *
     * @return array
     */
    protected function getFilesFromDirectory($path, &$paths = [])
    {
        $files = glob($path . '/*');
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->getFilesFromDirectory($file, $paths);
            } else {
                $paths[] = $file;
            }
        }

        return $paths;
    }

    /**
     * @return array
     */
    public function getFilesForImport()
    {
        $modulePartManager = $this->modulePartManager;
        $files = [];
        foreach ($this->getFilesFromDirectory($modulePartManager->getCoreModulePartFullPath()) as $item) {
            $array = explode("{$modulePartManager->module->id}/{$modulePartManager->getCoreModulePartPath()}", $item);
            if (isset($array[1])) {
                $files[$item] = $modulePartManager->getAppModulePartPath() . $array[1];
            }
        }

        return $files;
    }
}
