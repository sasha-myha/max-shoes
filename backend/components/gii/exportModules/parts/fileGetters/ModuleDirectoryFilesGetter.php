<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts\fileGetters;


/**
 * Class ModuleDirectoryFilesGetter
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class ModuleDirectoryFilesGetter extends FilesGetter
{
    /**
     * @return array
     */
    public function getFilesForExport()
    {
        $modulePartManager = $this->modulePartManager;
        $files = [];
        foreach ($this->getFilesFromDirectory($modulePartManager->getAppModulePartPath()) as $item) {
            $array = explode("modules/{$modulePartManager->module->id}", $item);
            if (isset($array[1])) {
                $files[$item] = $modulePartManager->getCoreModulePartFullPath() . $array[1];
            }
        }

        return $files;
    }
}
