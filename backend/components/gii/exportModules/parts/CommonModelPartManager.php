<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;


use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\SeparatedFilesGetter;
use Yii;

/**
 * Class CommonModelPartManager
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class CommonModelPartManager extends ModulePartManager
{
    /**
     * @return string
     */
    public function getAppModulePartPath()
    {
        return Yii::getAlias('@common') . '/models';
    }

    /**
     * @return string
     */
    public function getCoreModulePartPath()
    {
        return 'common';
    }

    /**
     * @return FilesGetter
     */
    public function getFilesGetter(): FilesGetter
    {
        return new SeparatedFilesGetter($this);
    }
}
