<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;


use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\SeparatedFilesGetter;
use Yii;

/**
 * Class ConsoleControllersPartManager
 *
 * @package backend\components\gii\exportModules\parts\fileGetters
 */
class ConsoleControllersPartManager extends ModulePartManager
{
    /**
     * @return string
     */
    public function getAppModulePartPath()
    {
        return Yii::getAlias('@console') . '/controllers';
    }

    /**
     * @return string
     */
    public function getCoreModulePartPath()
    {
        return 'console/controllers';
    }

    /**
     * @return FilesGetter
     */
    public function getFilesGetter(): FilesGetter
    {
        return new SeparatedFilesGetter($this);
    }
}
