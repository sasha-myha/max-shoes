<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules\parts;


use backend\components\gii\exportModules\parts\fileGetters\FilesGetter;
use backend\components\gii\exportModules\parts\fileGetters\ModuleDirectoryFilesGetter;
use Yii;

/**
 * Class FrontendThemesPartManager
 *
 * @package backend\components\gii\exportModules\parts
 */
class FrontendThemesPartManager extends ModulePartManager
{
    /**
     * @return string
     */
    public function getAppModulePartPath()
    {
        return Yii::getAlias('@frontend') . '/themes/basic/modules/' . $this->module->id;
    }

    /**
     * @return string
     */
    public function getCoreModulePartPath()
    {
        return 'frontend/themes';
    }

    /**
     * @return FilesGetter
     */
    public function getFilesGetter(): FilesGetter
    {
        return new ModuleDirectoryFilesGetter($this);
    }
}
