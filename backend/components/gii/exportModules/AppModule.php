<?php
/**
 * Created by anatolii
 */

namespace backend\components\gii\exportModules;


use common\components\interfaces\CoreModuleBackendInterface;
use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

/**
 * Class AppModule
 *
 * @package backend\components\gii\exportModules
 */
class AppModule extends BaseModule
{
    /**
     * @param string $moduleId
     *
     * @return string | CoreModuleBackendInterface
     */
    public static function getBackendModuleClassName($moduleId)
    {
        return "\\backend\\modules\\$moduleId\\Module";
    }

    /**
     * @return array
     */
    public function getFilesForExport()
    {
        $files = [];
        foreach ($this->parts as $part) {
            $files = ArrayHelper::merge($files, $part->getFilesGetter()->getFilesForExport());
        }

        return $files;
    }

    public function remove()
    {
        try {
            foreach ($this->parts as $part) {
                $part->remove();
            }
            $this->log[] = "Module '$this->id' was removed successfully";
        } catch (ErrorException $exception) {
            $this->log[] = "An error has occurred while removing '$this->id' module";
            $this->log[] = $exception->getMessage();
            $this->log[] = $exception->getTraceAsString();
        }
    }

    /**
     * @return array
     */
    public static function getAppModuleIds()
    {
        $ids = [];
        $dirs = array_filter(glob(Yii::getAlias('@backend') . '/modules/*'), 'is_dir');
        foreach ($dirs as $item) {
            $array = explode('/modules/', $item);
            if (count($array) == 2) {
                $moduleId = $array[1];
                $backendModuleClassName = static::getBackendModuleClassName($moduleId);
                if (class_exists($backendModuleClassName)) {
                    $interfaces = class_implements($backendModuleClassName);
                    if (isset($interfaces[CoreModuleBackendInterface::class])) {
                        $ids[$moduleId] = $moduleId;
                    }
                }
            }
        }

        return $ids;
    }
}
