<?php
namespace backend\modules\configuration;

/**
 * Configuration module definition class
 *
 * @package backend\modules\configuration
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\configuration\controllers';
}
