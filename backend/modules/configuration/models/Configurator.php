<?php
/**
 * @author walter
 */

namespace backend\modules\configuration\models;

use backend\components\ImperaviContent;
use kartik\datecontrol\DateControl;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;

/**
 * Class Configurator
 *
 * @package backend\modules\configuration\models
 */
class Configurator extends Configuration
{
    /**
     * @var bool
     */
    public $hint = false;
    /**
     * @var array
     */
    public $rules = [];
    /**
     * Items for dropdown list
     *
     * @var array
     */
    public $items = [];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type'], 'required'],
            [['id'], 'string', 'max' => 100],

            [['type'], 'required'],
            [['type'], 'integer'],

            [['preload', 'published', 'show'], 'integer'],

            [['description'], 'string', 'max' => 255],

            [['value'], 'integer', 'on' => 'integer'],
            [['value'], 'double', 'on' => 'double'],
            [['value'], 'string', 'on' => 'string'],
            [['value'], 'boolean', 'on' => 'boolean'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function getValueFieldConfig()
    {
        $hint = $this->hint ? $this->hint : null;
        switch ($this->type) {
            case static::TYPE_STRING:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_TEXT:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    'hint' => $hint,
                ];
            case static::TYPE_HTML:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => ImperaviContent::className(),
                    'hint' => $hint,
                ];
            case static::TYPE_INTEGER:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_DOUBLE:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_BOOLEAN:
                return [
                    'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    'hint' => $hint,
                    'options' => [
                        'label' => $this->description
                    ]
                ];
            case static::TYPE_FILE:
                return [
                    'type' => ActiveFormBuilder::INPUT_FILE,
                    //'hint' => $description . '<p>' . Html::a(FPM::originalSrc($this->value), FPM::originalSrc($this->value)) . '</p>',
                ];
            case static::TYPE_DATE:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => DateControl::className(),
                    'options' => [
                        'type' => DateControl::FORMAT_DATE,
                    ],
                ];
            case static::TYPE_DATE_TIME:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => DateControl::className(),
                    'options' => [
                        'type'=>DateControl::FORMAT_DATETIME,
                    ],
                ];
            case static::TYPE_DROPDOWN:
                return [
                    'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                    'items' => $this->items,
                ];
        }
        return [
            'type' => ActiveFormBuilder::INPUT_TEXT,
        ];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = false, $attributeNames = null)
    {
        if ($this->rules) {
            $rules = ArrayHelper::merge($this->rules(), $this->rules);
            $model = DynamicModel::validateData($this->attributes, $rules);
            if ($model->hasErrors()) {
                $this->addErrors($model->errors);
                return false;
            }
        }

        return parent::save();
    }
}
