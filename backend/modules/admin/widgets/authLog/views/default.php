<?php
/**
 * @var \backend\modules\admin\widgets\authLog\AuthLogWidget $this
 * @var \common\models\UserAuthLog[] $models
 */
?>
<table class="table table-bordered">
    <tr>
        <th><?= \help\bt('Date') ?></th>
        <th><?= \help\bt('Error') ?></th>
        <th><?= \help\bt('IP') ?></th>
        <th><?= \help\bt('Host') ?></th>
        <th><?= \help\bt('Url') ?></th>
        <th><?= \help\bt('User Agent') ?></th>
    </tr>

    <?php foreach ($models as $model): ?>
        <tr>
            <td><?= Yii::$app->formatter->asDatetime($model->date) ?></td>
            <td><?= $model->getErrorLabel() ?></td>
            <td><?= $model->ip ?></td>
            <td><?= $model->host ?></td>
            <td><?= Yii::$app->formatter->asUrl($model->url) ?></td>
            <td><?= $model->userAgent ?></td>
        </tr>
    <?php endforeach; ?>
</table>
