<?php

namespace backend\modules\mosterleads;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\mosterleads\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
