<?php

namespace backend\modules\mosterleads\models;

use backend\modules\configuration\models\Configuration;
use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%monsterleads_request}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $phone
 * @property string $name
 * @property string $additional_info
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class MonsterleadsRequest extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%monsterleads_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'integer'],
            [['additional_info'], 'string'],
            [['phone', 'name'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
            [['published'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'phone' => 'Phone',
            'name' => 'Name',
            'additional_info' => 'Additional Info',
            'published' => 'Published',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Monsterleads Request');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'type',
                        'filter' => \common\models\MonsterleadsRequest::getTypeList(),
                        'value' => function ($data) {
                            return \common\models\MonsterleadsRequest::getTypeTitle($data->type);
                        },
                    ],
                    'phone',
                    'name',
                    // 'additional_info:ntext',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'type',
                    'phone',
                    'name',
                    [
                        'attribute' => 'additional_info',
                        'format' => 'html',
                    ],
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new MonsterleadsRequestSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\MonsterleadsRequest::getTypeList()
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    
                ],
            ],
            'additional_info' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'additional_info',
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
