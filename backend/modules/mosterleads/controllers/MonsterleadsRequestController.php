<?php

namespace backend\modules\mosterleads\controllers;

use backend\components\BackendController;
use backend\modules\mosterleads\models\MonsterleadsRequest;

/**
 * MonsterleadsRequestController implements the CRUD actions for MonsterleadsRequest model.
 */
class MonsterleadsRequestController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return MonsterleadsRequest::className();
    }
}
