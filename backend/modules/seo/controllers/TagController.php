<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 24.05.17
 * Time: 12:57
 */

namespace backend\modules\seo\controllers;


use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class TagController extends \notgosu\yii2\modules\metaTag\controllers\TagController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ]);
    }
}