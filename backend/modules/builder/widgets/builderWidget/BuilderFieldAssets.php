<?php
namespace backend\modules\builder\widgets\builderWidget;

use yii\web\AssetBundle;

/**
 * Class BuilderFieldAssets
 *
 * @package backend\modules\content\widgets\builderForm
 */
class BuilderFieldAssets extends AssetBundle
{
    public $sourcePath = '@backend/modules/builder/widgets/builderWidget/assets';

    public $css = ['css/builder.css'];

    public $js = ['js/builder.js'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
}
