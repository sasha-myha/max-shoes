<?php
namespace backend\modules\builder\widgets\previewWidget;

use yii\web\AssetBundle;

/**
 * Class PreviewAssets
 *
 * @package backend\modules\content\widgets\previewWidget
 */
class PreviewAssets extends AssetBundle
{
    public $sourcePath = '@backend/modules/builder/widgets/previewWidget/assets';

    public $css = ['css/preview.css'];

    public $js = ['js/preview.js'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset'
    ];
}
