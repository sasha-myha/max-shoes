<?php
/**
 * Author: hanterrian
 */

namespace backend\modules\builder\widgets\previewWidget;

use common\components\BuilderModel;
use common\models\builder\ImageBuilderModel;
use common\models\builder\StringBuilderModel;
use common\models\builder\TextBuilderModel;
use yii\base\Widget;

/**
 * Class PreviewWidget
 *
 * @package backend\modules\builder\widgets\previewWidget
 */
class PreviewWidget extends Widget
{
    /** @var BuilderModel[] */
    public $widgets = [];

    public function run()
    {
        $renders = [];

        $renders[] = $this->render('default');

        foreach ($this->widgets as $widget) {
            $widget->proccessFiles();

            switch ($widget::className()) {
                case StringBuilderModel::className():
                    $renders[] = $this->render('string', ['model' => $widget]);
                    break;
                case TextBuilderModel::className():
                    $renders[] = $this->render('text', ['model' => $widget]);
                    break;
                case ImageBuilderModel::className():
                    $renders[] = $this->render('image', ['model' => $widget]);
                    break;
            }
        }

        return implode("\n", $renders);
    }
}
