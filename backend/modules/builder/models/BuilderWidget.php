<?php

namespace backend\modules\builder\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%builder_widget}}".
 *
 * @property integer $id
 * @property string $target_class
 * @property integer $target_id
 * @property string $target_sign
 * @property string $target_attribute
 * @property string $widget_class
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $widget_visible
 *
 * @property BuilderWidgetAttribute[] $builderWidgetAttributes
 */
class BuilderWidget extends ActiveRecord implements BackendModel
{
    public $data = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%builder_widget}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['target_class', 'target_id', 'target_attribute'], 'required'],
            [['target_id', 'position', 'widget_visible'], 'integer'],
            [['target_class', 'target_sign', 'target_attribute', 'widget_class'], 'string', 'max' => 255],
            [['position'], 'default', 'value' => 0],
            [['widget_visible'], 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'target_class' => 'Target Class',
            'target_id' => 'Target ID',
            'target_sign' => 'Target Sign',
            'target_attribute' => 'Target Attribute',
            'widget_class' => 'Widget Class',
            'position' => 'Position',
            'widget_visible' => 'Visible',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilderWidgetAttributes()
    {
        return $this->hasMany(BuilderWidgetAttribute::className(), ['widget_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('builder/widget', 'Builder Widget');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'target_class',
                    'target_id',
                    'target_sign',
                    'target_attribute',
                    'widget_class',
                    'widget_visible:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'target_class',
                    'target_id',
                    'target_sign',
                    'target_attribute',
                    'widget_class',
                    'widget_visible:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BuilderWidgetSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'target_class' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'target_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'target_sign' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'target_attribute' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'widget_class' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'widget_visible' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }


}
