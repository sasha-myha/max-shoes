<?php

namespace backend\modules\builder\models;

use backend\modules\builder\components\BuilderBehavior;
use backend\modules\builder\widgets\builderWidget\BuilderFieldWidget;
use common\components\model\ActiveRecord;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class BuilderConfig extends Component
{
    public $previewWidget = null;

    public $models = [];

    /**
     * @var BuilderConfig|null
     */
    protected static $instance = null;

    /**
     * @return BuilderConfig|null
     */
    protected static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param ActiveRecord $model
     * @param $attribute
     * @param array $params
     *
     * @return BuilderConfig|null
     */
    public static function config(ActiveRecord $model, $attribute, $params = [])
    {
        $config = self::getInstance();

        if (isset($params['previewWidget'])) {
            $config->previewWidget = ArrayHelper::getValue($params, 'previewWidget', BuilderFieldWidget::className());
        }

        if (isset($params['models'])) {
            $config->models = ArrayHelper::getValue($params, 'models', []);
        } else {
            $behaviors = $model->getBehaviors();
            foreach ($behaviors as $behavior) {
                if ($behavior instanceof BuilderBehavior) {
                    /** @var BuilderBehavior $behavior */

                    $widgets = $behavior->widgets;

                    $config->models = ArrayHelper::getValue($widgets, $attribute, []);
                }
            }
        }

        return $config;
    }

    /**
     * @return array
     */
    public function getModels()
    {
        $models = [];

        foreach ($this->models as $key => $model) {
            $models[$key] = new $model;
        }

        return $models;
    }
}
