<?php
/**
 * Created by PhpStorm.
 * User: maxim
 * Date: 24.05.17
 * Time: 15:07
 */

namespace backend\modules\language\controllers;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use vintage\i18n\controllers\DefaultController;

class TranslationController extends DefaultController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ]);
    }
}