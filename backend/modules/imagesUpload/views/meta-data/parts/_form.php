<?php

use backend\components\FormBuilder;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \backend\modules\imagesUpload\models\FileMetaData $model
 * @var string                                            $action
 * @var array                                             $translationModels
 */

?>
<?= Html::errorSummary(
    ArrayHelper::merge([$model], $translationModels),
    ['class' => 'alert alert-danger']
); ?>

<?php /** @var FormBuilder $form */
$form = FormBuilder::begin([
    'action'                 => $action,
    'enableClientValidation' => false,
    'options'                => [
        'id'      => 'meta-data-form',
        'enctype' => 'multipart/form-data',
        'data'    => ['pjax' => true],
    ],
]); ?>

<?= $form->prepareRows($model, $model->getFormConfig(), $translationModels); ?>

<div class="col-sm-8 margined centered">
    <?= Html::submitButton(Yii::t('back/fileMetaData', 'Save'), ['class' => 'btn btn-success']); ?>
    <?= Html::a(Yii::t('back/fileMetaData', 'Cancel'), '#', ['class' => 'btn btn-warning cancel-crop']); ?>
</div>

<?php FormBuilder::end(); ?>
