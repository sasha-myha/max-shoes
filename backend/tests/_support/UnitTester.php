<?php
namespace backend\tests;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    /**
     * @param \common\components\model\ActiveRecord $model
     * @return array
     */
    public function readRelatedFormConfig($model)
    {
        $formConfig = $model->getRelatedFormConfig();
        $return = [];
        foreach ($formConfig as $config) {
            $relation = $config['relation'];
            $className = $model->getRelation($relation)->modelClass;
            $relatedModels = $model->$relation;
            if (!is_array($relatedModels)) {
                $relatedModels = $relatedModels->all();
            }
            $model->relModels[$relation] = $relatedModels ? $relatedModels : [new $className];
            $return[$relation] = $config;
        }

        return $return;
    }

    public function seeInDb($tableName, $params)
    {
        $tableName = str_replace('{{%', '', $tableName);
        $tableName = str_replace('}}', '', $tableName);

        return $this->seeInDatabase($tableName,$params);
    }
}
