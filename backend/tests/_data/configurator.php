<?php
use backend\modules\configuration\models\Configurator;

return [
    [
        'id'            => 'test',
        'value'         => 'test data',
        'type'          => Configurator::TYPE_STRING,
        'description'   => 'This is test field',
        'preload'       => false,
        'published'     => true,
        'show'          => true,
    ],
];
