<?php
namespace backend\tests\unit\modules\admin\models;

use common\components\UserIdentity;
use common\models\User;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\UserFixture;
use backend\modules\admin\models\LoginForm;

/**
 * Test case for [[\backend\modules\admin\models\LoginForm]]
 * @see \backend\modules\admin\models\LoginForm
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class LoginFormTest extends DbTestCase
{
    /**
     * @var LoginForm
     */
    private $form;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
       return [
           'user' => ['class' => UserFixture::className()],
       ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->form = new LoginForm([
            'email' => 'tester@test.test',
            'password' => 'test'
        ]);
    }

    public function testGetUser()
    {
        $entity = User::findOne([
            'email' => 'tester@test.test',
            'status' => User::STATUS_ACTIVE
        ]);
        $expected = new UserIdentity($entity);
        $actual = $this->form->getUser();

        $this->assertEquals($expected, $actual);
    }

    public function testValidatePassword()
    {
        $this->form->validatePassword('password', []);
        $this->assertFalse($this->form->hasErrors());
    }

    public function testLogin()
    {
        $this->assertTrue($this->form->login());
    }
}
