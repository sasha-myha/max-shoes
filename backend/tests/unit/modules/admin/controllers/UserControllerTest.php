<?php
namespace backend\tests\unit\modules\admin\controllers;

use Yii;
use yii\web\Response;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\UserFixture;
use common\models\User;
use backend\components\BackendController;
use backend\modules\admin\controllers\UserController;

/**
 * Test case for [[\backend\modules\admin\controllers\UserController]]
 * @see \backend\modules\admin\controllers\UserController
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class UserControllerTest extends DbTestCase
{
    /**
     * @var UserController
     */
    private $controller;


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'user' => ['class' => UserFixture::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->controller = new UserController('test', Yii::$app);
        Yii::$app->controller = $this->controller;
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(BackendController::className(), $this->controller);
    }

    public function testGetModelClass()
    {
        $this->assertEquals(
            \backend\modules\admin\models\User::className(),
            $this->controller->getModelClass()
        );
    }

    public function testActionDelete()
    {
        $userId = 1;
        $response = $this->controller->actionDelete($userId);

        $this->assertInstanceOf(Response::className(), $response);
        $this->assertStringEndsWith('index', $response->headers['location']);
        $this->tester->seeInDb(User::tableName(), [
            'id' => $userId,
            'status' => User::STATUS_DELETED
        ]);
    }
}
