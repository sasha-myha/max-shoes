<?php
namespace backend\tests\unit\components;

use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\LanguageFixture;
use backend\tests\fake\models\TestRelationTable;
use backend\tests\fake\models\TestTable;
use backend\tests\fixtures\TestRelationTableFixture;
use backend\tests\fixtures\TestTableFixture;

/**
 * Test case for [[backend\components\TranslateableTrait]]
 * @see \backend\components\TranslateableTrait
 *
 * @property \backend\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TranslateableTraitTest extends DbTestCase
{
    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'language' => ['class' => LanguageFixture::className()],
            'testTable' => ['class' => TestTableFixture::className()],
            'testRelTable' => ['class' => TestRelationTableFixture::className()],
        ];
    }

    public function testGetTranslationModels()
    {
        $model = TestTable::findOne(1);

        $actual = $model->getTranslationModels();
        $expected = TestRelationTable::find()
            ->where(['!=', 'language', 'en-US'])
            ->andWhere(['in', 'id', [1, 2, 3]])
            ->indexBy('language')
            ->all();

        $this->assertEquals($expected, $actual);
    }
}
