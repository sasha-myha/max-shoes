<?php
namespace backend\tests\fake\models;

use common\components\model\Translateable;

/**
 * Active record model for `test_table`
 *
 * @property integer $id
 * @property string $test_field
 *
 * @property TestRelationTable[] $thisIsTestRelation
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestTableRelatedForm extends TestTable implements Translateable
{
    /**
     * @inheritdoc
     */
    public static function getTranslationAttributes()
    {
        return [
            'test_field'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(TestRelationTable::className(), ['rel_table_id' => 'id']);
    }
}
