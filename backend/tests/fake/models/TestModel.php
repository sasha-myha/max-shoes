<?php
namespace backend\tests\fake\models;

use backend\components\Model;

/**
 * Fake model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class TestModel extends Model
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $description;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'description'], 'safe']
        ];
    }
}
