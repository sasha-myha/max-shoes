<?php
namespace backend\tests\fake\yii;

/**
 * Fake request component
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class Request extends \yii\web\Request
{
    private $_data;


    /**
     * Setter for data
     *
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * @inheritdoc
     */
    public function getIsPost()
    {
       return true;
    }

    /**
     * @inheritdoc
     */
    public function post($name = null, $defaultValue = null)
    {
        if ($name === null) {
            return $this->_data;
        }

        return isset($this->_data[$name])
            ? $this->_data[$name]
            : $defaultValue;
    }
}
