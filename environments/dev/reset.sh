#!/bin/bash

RC='\033[0m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'

printf "${YELLOW}Reset old commits...\n${RC}"

git reset $(echo "Initial commit" | git commit-tree HEAD^{tree})
git reflog expire --all --expire-unreachable=now
git gc --aggressive --prune=now

if [ ! -z "$@" ]; then
  printf "${YELLOW}Set remote repository new address...\n${RC}"

  git remote set-url origin $@
  git push -u origin master
fi

printf "${GREEN}Done!\n${RC}"

rm -- "$0"
