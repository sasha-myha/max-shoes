<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%monsterleads_request}}".
 *
 * @property integer $id
 * @property integer $monsterleads_api_status
 * @property integer $type
 * @property string $phone
 * @property string $name
 * @property string $additional_info
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 */
class MonsterleadsRequest extends ActiveRecord 
{
    const TYPE_RANDEVY = 1;

    const MONSTERLEADS_API_NEW = 1;
    const MONSTERLEADS_API_SEND = 2;
    const MONSTERLEADS_API_ERROR = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%monsterleads_request}}';
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_RANDEVY => 'Рандеву женские дела'
        ];
    }

    public static function getTypeTitle($type)
    {
        return self::getTypeList()[$type] ?? 'type don\'t select';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }
}
