<?php
namespace common\models\builder;

use backend\modules\builder\components\BuilderImageUpload;
use common\components\BuilderModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class ImageBuilderModel
 *
 * @package backend\modules\builder\models\builder
 */
class ImageBuilderModel extends BuilderModel
{
    public $image;

    public function getName()
    {
        return 'Image';
    }

    public function rules()
    {
        $rules = ArrayHelper::merge(parent::rules(), [
            [['image'], 'safe'],
        ]);

        return $this->prepareRules($rules);
    }

    public function getAttributeLabels()
    {
        $labels = [
            'image' => 'Image',
        ];

        return $this->prepareLabels($labels);
    }

    public function getConfig()
    {
        return ArrayHelper::merge(parent::getDefaultConfig(), [
            'image' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => BuilderImageUpload::className(),
                'options' => [
                    'aspectRatio' => false, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ],
            ],
        ]);
    }

    public function getLocalized()
    {
        return [];
    }
}
