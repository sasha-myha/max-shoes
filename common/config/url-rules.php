<?php
return [
    'test' => 'site/index',
    'robots.txt' => 'site/robots',
    'sitemap.xml' => 'sitemap/default/index',

    '' => 'monsterlead/rendez-vous/transit',
    'rendez-vous' => 'monsterlead/rendez-vous/landing',
    'rendez-vous/thank-you' => 'monsterlead/rendez-vous/thank-you'
];
