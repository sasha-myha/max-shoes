<?php
namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\web\IdentityInterface;
use yii2tech\authlog\AuthLogIdentityBehavior;

/**
 * Class IpLogLoginFormBehavior
 *
 * @property Model $owner
 *
 * @package common\components
 */
class IpLogLoginFormBehavior extends Behavior
{
    /**
     * @var string|callable name of the owner method or a PHP callback, which should return the current identity instance.
     * If string - considered as name of the [[owner]] method, otherwise as independent PHP callback.
     * If not set - internal [[findIdentity()]] method will be used.
     * Signature of the callback:
     *
     * ```php
     * IdentityInterface function() {...}
     * ```
     */
    public $findIdentity;

    /**
     * @var array list of the owner attributes, which are used to validate identity authentication.
     */
    public $verifyIdentityAttributes = ['password'];

    public $blockIpSequence = 10;

    /**
     * @var IdentityInterface|AuthLogIdentityBehavior|null|boolean
     */
    private $_identity = false;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Model::EVENT_AFTER_VALIDATE => 'afterValidate',
        ];
    }

    /**
     * @return IdentityInterface|null
     */
    public function setIdentity($identity)
    {
        $this->_identity = $identity;
    }

    /**
     * @return IdentityInterface|AuthLogIdentityBehavior|IpLogBehavior|null
     */
    public function getIdentity()
    {
        if ($this->_identity === false) {
            if ($this->findIdentity === null) {
                $this->_identity = $this->findIdentity();
            } else {
                if (is_string($this->findIdentity)) {
                    $this->_identity = call_user_func([$this->owner, $this->findIdentity]);
                } else {
                    $this->_identity = call_user_func($this->findIdentity);
                }
            }
        }
        return $this->_identity;
    }

    /**
     * Finds current identity assuming there is [[findByLoginName()]] identity method.
     *
     * @return IdentityInterface|null identity instance, `null` if not found.
     */
    protected function findIdentity()
    {
        $loginNameAttribute = 'username';
        $loginNameValue = trim($this->owner->{$loginNameAttribute});
        if (empty($loginNameValue)) {
            return null;
        }
        $identityClass = Yii::$app->user->identityClass;
        return $identityClass::findByLoginName($loginNameValue);
    }

    /**
     * Handles owner 'afterValidate' event.
     *
     * @param \yii\base\Event $event event instance.
     */
    public function afterValidate($event)
    {
        $identity = $this->getIdentity();

        if (!is_object($identity)) {
            return;
        }

        foreach ($this->verifyIdentityAttributes as $attribute) {
            if ($this->owner->hasErrors($attribute) && !empty($this->owner->{$attribute})) {
                $identity->logIpError();

                if ($this->blockIpSequence !== false) {
                    if ($identity->blockIpSequence($this->blockIpSequence)) {
                        $identity->blockIp();
                    }
                }
            }
        }
    }
}
