<?php

return [
    [
        'id'            => 1,
        'extension'     => 'dat',
        'base_name'     => 'first',
        'alt_tag'       => 'test-alt',
    ],
    [
        'id'            => 2,
        'extension'     => 'dat',
        'base_name'     => 'second',
        'alt_tag'       => 'test-alt',
    ],
    [
        'id'            => 3,
        'extension'     => 'dat',
        'base_name'     => 'third',
        'alt_tag'       => 'test-alt',
    ],
];
