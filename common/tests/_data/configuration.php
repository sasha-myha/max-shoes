<?php
use backend\modules\configuration\models\Configurator;

return [
    [
        'id'            => 'test-1',
        'value'         => 'test data 1',
        'type'          => Configurator::TYPE_STRING,
        'preload'       => true,
        'published'     => true,
        'show'          => true,
    ],
    [
        'id'            => 'test-2',
        'value'         => 'test data 2',
        'type'          => Configurator::TYPE_STRING,
        'preload'       => true,
        'published'     => true,
        'show'          => true,
    ],
    [
        'id'            => 'test-3',
        'value'         => 'test data 3',
        'type'          => Configurator::TYPE_STRING,
        'preload'       => false,
        'published'     => true,
        'show'          => true,
    ],
    [
        'id'            => 'test-4',
        'value'         => 'test data 4',
        'type'          => Configurator::TYPE_STRING,
        'preload'       => false,
        'published'     => true,
        'show'          => true,
    ],
];
