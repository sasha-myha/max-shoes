<?php
namespace common\tests\unit\models\base;

use Yii;
use common\models\EntityToFile;
use common\tests\base\unit\DbTestCase;
use common\tests\fixtures\FpmFileFixture;
use common\tests\fixtures\EntityToFileFixture;

/**
 * Test case for [[\common\models\base\EntityToFile]]
 * @see \common\models\base\EntityToFile
 *
 * @property \common\tests\UnitTester $tester
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class EntityToFileTest extends DbTestCase
{
    /**
     * @var string
     */
    private $basePath;
    /**
     * @var array
     */
    private $files = [];


    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'fpm-file' => ['class' => FpmFileFixture::className()],
            'entity-to-file' => ['class' => EntityToFileFixture::className()],
        ];
    }

    /**
     * Creates files for testing
     */
    private function initFiles()
    {
        $files = $this->getFixture('fpm-file')->getFiles();
        $this->basePath = Yii::getAlias('@tests/_output/uploads/0/');

        foreach ($files as $file) {
            $fileName = strtr('{id}-{name}.{extension}', [
                '{id}' => $file['id'],
                '{name}' => $file['base_name'],
                '{extension}' => $file['extension']
            ]);
            $this->files[] = $fileName;
            fopen($this->basePath . $fileName, 'w');
        }
    }

    public function testAdd()
    {
        $entityName = 'testEntity';
        $entityID = 7;
        $fileId = 1;

        $actual = EntityToFile::add($entityName, $entityID, $fileId);
        $this->assertInstanceOf(EntityToFile::className(), $actual);
        $this->tester->seeInDb(EntityToFile::tableName(), [
            'entity_model_name' => $entityName,
            'entity_model_id' => $entityID,
            'file_id' => $fileId
        ]);
    }

    public function testTempAdd()
    {
        $entityName = 'testEntity';
        $entityID = 7;
        $fileId = 1;
        $sign = Yii::$app->getSecurity()->generateRandomKey();

        $actual = EntityToFile::add($entityName, $entityID, $fileId, $sign);
        $this->assertInstanceOf(EntityToFile::className(), $actual);
        $this->tester->seeInDb(EntityToFile::tableName(), [
            'entity_model_name' => $entityName,
            'entity_model_id' => 0,
            'file_id' => $fileId,
            'temp_sign' => $sign
        ]);
    }

    public function testDeleteImages()
    {
        $this->initFiles();

        $IDs = [3, 4, 5];
        EntityToFile::deleteImages('testEntity', $IDs);

        foreach ($IDs as $id) {
            $this->tester->dontSeeInDatabase(EntityToFile::tableName(), [
                'entity_model_name' => 'testEntity',
                'entity_model_id' => $id,
            ]);
        }
        foreach ($this->files as $file) {
            $this->assertFileNotExists($file, $this->basePath);
        }
    }

    public function testUpdateImages()
    {
        $modelId = 777;
        EntityToFile::updateImages($modelId, Yii::$app->params['sign']);

        $this->tester->seeInDb(EntityToFile::tableName(), [
            'entity_model_name' => 'testUpdateImages',
            'entity_model_id' => $modelId,
            'temp_sign' => ''
        ]);
    }
}
