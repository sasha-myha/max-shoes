<?php
namespace common\tests\fake\models;

use Yii;

/**
 * Fake static page model
 *
 * @property string $first
 * @property string $second
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class StaticPage extends \common\models\base\StaticPage
{
    const FIELD_FIRST = 'test-3';
    const FIELD_SECOND = 'test-4';

    /**
     * @var string
     */
    public $first;
    /**
     * @var string
     */
    public $second;


    /**
     * @inheritdoc
     */
    public function getKeys()
    {
        return [
            self::FIELD_FIRST,
            self::FIELD_SECOND,
        ];
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        /* @var \common\components\ConfigurationComponent $config */
        $config = Yii::$app->get('config');

        $this->first = $config->get(self::FIELD_FIRST);
        $this->second = $config->get(self::FIELD_SECOND);

        return $this;
    }
}
