<?php
namespace common\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\common\models\Language]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class LanguageFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\Language';
    /**
     * @inheritdoc
     */
    public $dataFile = '@common/tests/_data/language.php';
}
