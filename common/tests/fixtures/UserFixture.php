<?php
namespace common\tests\fixtures;

use yii\test\ActiveFixture;

/**
 * Fixture for [[\common\models\User]] model
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class UserFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\User';
    /**
     * @inheritdoc
     */
    public $dataFile = '@common/tests/_data/user.php';
}
