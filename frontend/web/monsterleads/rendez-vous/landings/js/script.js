window.onload = function(){
    initializeClock('time1', deadline);
    initializeClock('time2', deadline);
    initializeClock('time3', deadline);

};

$(function(){
  // функция "виден ли объект"
  function isVisible (block, plusToTop, plusToBottom) {
    plusToTop = (plusToTop == undefined) ? 0 : plusToTop;
    plusToBottom = (plusToBottom == undefined) ? block.height() : plusToBottom;
    var docViewTop, docViewBottom, blockTop, blockBottom;
    docViewTop = $(window).scrollTop(); 
    docViewBottom = docViewTop + $(window).height(); 
    blockTop = block.offset().top + plusToTop;
    blockBottom = blockTop + plusToBottom;  
    if ((docViewBottom >= blockBottom)&&(docViewTop <= blockTop)) {
      return true;
    } else {
      return false;
    }
  }

  // функция: мигающие нолики
  function blinkZeros() {
    zeros.animate({opacity: 1}, 1150).animate({opacity: 0}, 1150, blinkZeros);
  };
  // функция: фитиль
  function spark() {
    var $fuze = $('.fuze');
    $fuze.animate({width: "-=150"}, 13000, "linear").siblings('.spark').animate({left: "-=150"}, 13000, "linear", function() {
      $fuze.animate({width: "+=150"}, 800, "linear").siblings('.spark').animate({left: "+=150"}, 800, "linear", function() {spark()});
    });
  }
  // функция: жвачка
  function bubleGum() {
    logoimg.animate({width: "+=12", height: "+=12", top: "-=6", right: "-=6"}, 1150)
              .animate({width: "-=12", height: "-=12", top: "+=6", right: "+=6"}, 1150)             
    setTimeout(bubleGum, 2300);
  }
  // функция: появление картинок растений, которые в составе жвачки
  function visibleComponents() {
    var components = $('.components');
    var opacity = +components.find('.comp1').css('opacity');
    if (!opacity) {
      var blockVisible = isVisible(components.find('.comp1'), 40, 50);
      if  (blockVisible){
        components.find('.comp1').animate({opacity: 1}, 500);
        setTimeout(function() {components.find('.comp2').animate({opacity: 1}, 500);}, 500);
        setTimeout(function() {components.find('.comp3').animate({opacity: 1}, 500);}, 1000);
        setTimeout(function() {components.find('.comp4').animate({opacity: 1}, 500);}, 1500);
        setTimeout(function() {components.find('.comp5').animate({opacity: 1}, 500);}, 2000);
        setTimeout(function() {components.find('.comp6').animate({opacity: 1}, 500);}, 2500);
        setTimeout(function() {components.find('.comp7').animate({opacity: 1}, 500);}, 3000);
      }
    } else {
      return;
    }
  }
  // появление нижних блоков
  function visibleBlocks() {
    var blockDisplay = $('.question1').css('display').match(/[^\s]*"/);;
    if (!(blockDisplay == "none")) {
      var blockVisible = isVisible($('.block5-couple'), 50, 300);
      if (blockVisible) {
        $('.question1').fadeIn(800);
        setTimeout(function() {$('.question2').fadeIn(800);}, 800);
      }
    } else {
      return;
    }
  }
  // функция анимации заголовков
  function animationText() {
    var elements = $('.anim-text');
    elements.each(function() {
      var $this = $(this)
      if ($this.hasClass('animated')) return;
      var visible = isVisible($this);
      if (visible) {
        var position = $this.hasClass('anim-from-left');
        if (position) {
          $this.addClass('animated animated-left');
        } else {
          position = $this.hasClass('anim-from-right')
          if (position) {
            $this.addClass('animated animated-right');
          }
        }
      }
    });
  }
  var btn_animated = true; //переменная которая следит закончилась анимации при нажитии на кнопку
  $('.go-bot-btn').click(function() {
    btn_animated = false;
    setTimeout((function() {$("html, body").animate({ scrollTop: $(document).height() }, "slow", function() {btn_animated = true;});}), 150);
  });
  
  // применения стилей если всключен js
  $('.components li').css('opacity', '0'); //скрываем ингредиенты
  $('div.question').hide(); // скрываем блоки вопросов
  var zeros = $('.p-zero'); // эта и следующая строчка - скрываем нолики
  zeros.css('opacity', 0);
  var logoimg = $('.logo-gum img'); //обертываем картинку жвачки
  logoimg.css({  // уменьшаем изначальный размер жвачки
    width: '-=6',
    height: '-=6',
    top: '+=3',
    right: '+=3'
  });
  
  // стили заголовков если включен js
  $('.anim-from-right').css({
    position: 'relative',
    right: '-400px',
    opacity: '0'
  })
  $('.anim-from-left').css({
    position: 'relative',
    left: '-400px',
    opacity: '0'
  })
  
  // действия при скроллинге страницы
  $(window).scroll(function(){
    if (btn_animated) {
      visibleComponents();
      visibleBlocks();
      animationText();
    }
  });
  
  // включаем-выключаем анимацию
  blinkZeros();
  spark();
  bubleGum();
});
