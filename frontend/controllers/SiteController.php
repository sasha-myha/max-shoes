<?php
namespace frontend\controllers;

use common\models\MonsterleadsRequest;
use common\models\Robots;
use frontend\components\FrontendController;
use GuzzleHttp\Client;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Renders index page
     *
     * @return string
     */
    public function actionIndex()
    {
        $models = MonsterleadsRequest::find()
            ->where(['type' => MonsterleadsRequest::TYPE_RANDEVY, 'monsterleads_api_status' => MonsterleadsRequest::MONSTERLEADS_API_NEW])
            ->all();
        var_dump($models);
        exit;
        $client = new Client([
            'base_uri' => 'http://api.monsterleads.pro/method/',
        ]);
        $response = $client->request('POST', 'order.add', [
            'form_params' => [
                'format' => 'json',
                'api_key' => '7a4f3181c3cb6b5476fbceacfbdc8718',
                'tel' => ''
            ]
        ]);

        echo $response->getStatusCode(); // 200
        echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'
    }

    /**
     * Generates robots.txt file
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRobots()
    {
        /* @var Robots $robots */
        $robots = Robots::find()->one();

        if (!$robots) {
            throw new NotFoundHttpException();
        }

        $this->layout = false;

        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_RAW;
        $headers = $response->headers;
        $headers->add('Content-Type', 'text/plain');

        $text = $robots->text;
        $text .= "\nSitemap: " . Url::to(['/sitemap/default/index'], true);

        return $this->renderContent($text);
    }
}
