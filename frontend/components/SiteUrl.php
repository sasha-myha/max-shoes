<?php
namespace frontend\components;

use common\components\model\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UrlManager;


/**
 * Site controller
 */
class SiteUrl
{
    /**
     * @param $route
     * @param $params
     *
     * @return string
     */
    public static function createUrl($route, $params)
    {
        return Url::to(
            ArrayHelper::merge(
                [$route],
                $params
            )
        );
    }

    public static function createRendezVousUrl($params = [])
    {
        return self::createUrl('/site/index', $params);
    }

}
