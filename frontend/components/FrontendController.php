<?php
/**
 * @author walter
 */

namespace frontend\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class FrontendController
 * @package frontend\components
 */
class FrontendController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $baseUrl = Yii::$app->getRequest()->getAbsoluteUrl();

            preg_match("/(http|https):\/\/(www.)*/", $baseUrl, $match);
            if ($match) {
                if (ArrayHelper::getValue($match, 2) == 'www.') {
                    $url = str_replace('www.', '', $baseUrl);
                    return $this->redirect($url, 301);
                }
            }

            if ($pos = strpos($baseUrl, '/index.php')) {
                return $this->redirect(Yii::$app->getHomeUrl());
            }
            
            return true;
        }
        return false;
    }
}
