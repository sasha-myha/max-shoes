<?php
namespace frontend\tests\unit\widgets\microdata;

use common\tests\base\unit\TestCase;
use frontend\widgets\microdata\BreadcrumbsMicrodataWidget;
use Yii;
use yii\web\View;

/**
 * Test case for [[BreadcrumbsMicrodataWidget]]
 * 
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class BreadcrumbsMicrodataWidgetTest extends TestCase
{
    protected function registerDependencies()
    {
        Yii::$container->set(View::class, function ($container, $params, $config) {
            $config['params']['breadcrumbs'][] = [
                'label' => 'test item',
                'url' => '/test',
            ];
            return new View($config);
        });
    }

    public function testInit()
    {
        $this->expectException('yii\base\Exception');
        new BreadcrumbsMicrodataWidget();
    }

    public function testRun()
    {
        $widget = new BreadcrumbsMicrodataWidget(['homePageTitle' => 'home']);

        $expected = '<script type="application/ld+json">'
            . '{"@context":"http://schema.org","@type":"BreadcrumbList","itemListElement":[{"@type":"ListItem","position":1,"item":{"@id":"http://localhost/","name":"home"}},{"@type":"ListItem","position":2,"item":{"@id":"localhost/test","name":"test item"}}]}'
            .'</script>';
        $actual = $widget->run();

        $this->assertEquals($expected, $actual);
    }
}
