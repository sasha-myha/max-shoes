<?php
use \frontend\modules\monsterlead\components\MonsterleadUrl;
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Женский возбудитель Rendez Vous</title>
    <!-- Favicon -->
    <link rel="icon" href="/monsterleads/rendez-vous/landings/img/696.png"  type="image/png">
    <link rel="shortcut icon" href="/monsterleads/rendez-vous/landings/img/696.png"  type="image/png">

    <!-- OG -->
    <meta property="og:url" content="http://d.rendezwouz.com/" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Женский возбудитель Rendez Vous" />
    <meta property="og:image" content="http://infocdn.org/img/products_new/696.png" />
    <meta property="og:site_name" content="Женский возбудитель Rendez Vous" />
    <meta property="og:description" content="«Rendez Vous» – уникальное средство, способное быстро и эффективно разбудить женскую чувственность. Компоненты препарата вызывают у женщины мощное половое возбуждение и усиливают влечение к мужчине. Помимо этого средство позволяет получить больше удовольствия от секса - L-аргинин и женьшень стимулируют микроциркуляцию в области половых органов и обеспечивают приток крови к ним, что усиливает приятные ощущения и способствует достижению оргазма." />

    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="/monsterleads/rendez-vous/landings/css/style.css-5.css" >
    <script type="text/javascript" src="/monsterleads/rendez-vous/landings/js/jquery-1.10.2.min.js" ></script>
<!--    <script type="text/javascript" src="/monsterleads/rendez-vous/landings/js/placeholder.js" ></script>-->
    <script type="text/javascript" src="/monsterleads/rendez-vous/landings/js/timer.js" ></script>
    <script type="text/javascript" src="/monsterleads/rendez-vous/landings/js/script.js" ></script>

</head>
<body>

<div class="wrapper">
    <div class="block1">
        <div class="container">
            <div class="logo">
                <div class="logo-small">&nbsp;</div>
                <div class="logo-big">Rendez Vous</div>
                <!--    <div class="logo-gum"><img src="/monsterleads/rendez-vous/landings/js//files/randevy_5/img/gum.png" alt=""></div> -->
                <!-- <div class="logo-img"></div> -->
            </div>
            <div class="clearfix">
                <div class="block1-right">
                    <div class="result-guaranty">
                        <div class="guaranty-percent">
                            <div class="p-one"></div
                            ><div class="p-zero zero1"></div
                            ><div class="p-zero zero2"></div
                            ><div class="p-percent"></div>
                        </div>
                        <div class="guaranty-text"></div>
                    </div>
                    <div class="pluses">
                        <ul>
                            <li class="plus1">Начинает действовать<br><span>ЧЕРЕЗ 5 МИНУТ ПОСЛЕ ПРИЕМА</span></li>
                            <li class="plus2">Производится только из<br><span>НАТУРАЛЬНЫХ КОМПОНЕНТОВ</span></li>
                            <li class="plus3">Обладает очень быстрым и сильным<br><span>ВОЗБУЖДАЮЩИМ ЭФФЕКТОМ</span></li>
                        </ul>
                    </div>
                </div>
                <div class="block1-left">
                    <div class="price-block">
                        <span class="price-text">Цена всего</span>
                        <span class="new-price"><span class="hotprice noflag">  <span class="price_only1206"></span><span class="price_currency"></span>*</span></span>
                        <span class="old-price">Старая цена <span class="hotprice noflag"><span class="price_old1206"></span><span class="price_currency"></span></span></span>
                    </div>
                    <div class="btn btn-right go-bot-btn"></div>
                    <div class="countdown">
                        <p>До конца акции осталось</p>
                        <div class="time" id="time1">
                            <span class="zero days">00</span>
                            <span class="hours">00</span>
                            <span class="minutes">00</span>
                            <span class="seconds">00</span>
                        </div>
                        <div class="time-desc">
							<span class="zero">дни</span
                            ><span>часы</span
                            ><span>минуты</span
                            ><span>секунды</span>
                        </div>
                    </div>
                </div>
                <div class="fuze-anim">
                    <div class="fuze"></div>
                    <div class="spark"></div>
                </div>
            </div>
            <div class="block1-bottom">
                <div class="components">
                    <ul>
                        <li class="comp1">
                            <div class="comp-img"></div>
                            <div class="comp-text">ЕВРИКОМА</div>
                        </li>
                        <li class="comp2">
                            <div class="comp-img"></div>
                            <div class="comp-text">ЛЕВЗЕЯ</div>
                        </li>
                        <li class="comp3">
                            <div class="comp-img"></div>
                            <div class="comp-text">ЙОХИМБЕ</div>
                        </li>
                        <li class="comp4">
                            <div class="comp-img"></div>
                            <div class="comp-text">ПОМЕРАНЕЦ</div>
                        </li>
                        <li class="comp5">
                            <div class="comp-img"></div>
                            <div class="comp-text">Элеутерококк<br>колючий</div>
                        </li>
                        <li class="comp6">
                            <div class="comp-img"></div>
                            <div class="comp-text">МЕНТОЛ</div>
                        </li>
                        <li class="comp7">
                            <div class="comp-img"></div>
                            <div class="comp-text">ГУАРАНА</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="block2">
        <div class="container">
            <div class="block2-top">
                <h2>ВАША ВТОРАЯ ПОЛОВИНКА ПОСТОЯННО ГОВОРИТ НЕТ?</h2>
                <h3>Устали от отговорок?</h3>
                <h4>У вас не получается расслабиться и думать о партнере?</h4>
                <p>Пора попробовать  «Rendez Vous». Неважно, мужчина вы или женщина. С этим средством вы сможете раскрепостить
                    своего партнера, мало кто устоит перед действием «Rendez Vous». Ваш партнер захочет вас и вы навсегда забудете, что такое слово «НЕТ»</p>
            </div>
            <div class="block2-middle">
                <h3>У вас «снова не получилось»?<br> Думаете о работе<br> и не можете сосредоточиться<br> и получить удовольствие?</h3>
                <p>Проблемы с потенцией или неуверенность в постели?<br> Забудьте обо всех своих комплексах, теперь они в прошлом!<br> Rendez Vous поможет вам стать идеальным любовником<br> и получить истинное удовольствие от своего партнера.</p>
                <p class="special">Проблемы?</p>
            </div>
            <div class="block2-bottom">
                <div class="block2-text">
                    <h3>Понравилась коллега, но не можете сделать первый шаг?</h3>
                    <h3 class="head-bigger">Пусть первый шаг сделает она!</h3>
                    <p>«Rendez Vous» поможет девушке раскрепоститься, настроит ее на нужный лад и уберет все ее комплексы.</p>
                </div>
                <div class="clearfix">
                    <div class="countdown">
                        <div class="time" id="time2">
                            <span class="zero days">00</span>
                            <span class="hours">00</span>
                            <span class="minutes">00</span>
                            <span class="seconds">00</span>
                        </div>
                        <div class="time-desc">
							<span class="zero">дни</span
                            ><span>часы</span
                            ><span>минуты</span
                            ><span>секунды</span>
                        </div>
                    </div>
                    <div class="btn btn-left go-bot-btn"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="block3">
        <div class="container">
            <h2 class="anim-text anim-from-right">«Rendez Vous» РЕКОМЕНДУЮТ</h2>
            <div class="recommend">
                <div class="magazines"></div>
                <div class="shelf"></div>
            </div>


            <div class="specialist-opinion clearfix specialist2">
                <div class="opinion-wrapper">
                    <h3 class="anim-text anim-from-left">МНЕНИЕ СПЕЦИАЛИСТА</h3>
                    <div class="opinion">
                        <p>Научному сообществу давно известны <strong>невероятные свойства растений, входящих в Rendez Vous,</strong> выпускаемый в России и СНГ. Однако, еще несколько лет назад собрать воедино все компоненты, растущие буквально <strong>на разных концах земного шара</strong> было слишком трудозатратным для массового производства занятием. Медицинским корпорациям ничего не оставалось, кроме как направить свои исследования в сторону создания «химических аналогов» компонентов, входящих в состав Rendez Vous. Результаты разработок сегодня известны всему миру как <strong>ведущие средства для улучшения либидо</strong> обоих полов.</p>
                        <p>Но я делаю большую ставку <strong>на помощь организму исключительно натуральными компонентами</strong> и рекомендую своим пациентам «Rendez Vous»</p>
                    </div>
                    <div class="specialist">Доктор медицинских наук клиники НИИ Питание <b>Ткаченко А.С.</b></div>
                </div>
                <div class="specialist-icon"></div>
            </div>
        </div>
    </div>
    <div class="block4">
        <div class="container">
            <div class="comments">
                <h2 class="anim-text anim-from-right">ОНИ УЖЕ ПОПРОБОВАЛИ</h2>
                <div class="comment">
                    <p>Не обремененный успехом у девушек, я был одновременно счастлив и очень взволнован, когда встретил «ту единственную». Угадайте что? <b>Банальное волнение и сыграло со мной злую шутку</b>. Не хочу сейчас грузить тем, что пережил, но скажу одно: только ваш продукт подарил мне <b>невероятную уверенность в себе</b> и в «своем друге» и только с ним мне удалось <b>полностью раскрепоститься</b>. В общем рекомендую его всем, кто еще только набирается опыта!</p>
                    <div class="meta-author">Игорь Беланов, 23 года</div>
                </div>
                <div class="comment">
                    <p>Про Rendez Vous мне рассказала подруга, которая в какой-то момент <b>сменила репутацию невзрачной серой мышки на искушенную повелительницу мужчин</b>. Как только попробовала — я поняла, что мой секс без Rendez Vous не сравнится и с 10% впечатлений, которые я научилась получать с ней. Честно говоря, я бы её пачками съедала, благо моя старшая сестра биохимик и она подтвердила, что <b>все компоненты натуральны и не приносят вреда, в отличии от таблеток</b>. И что люди их уже 1000 лет ели)))</p>
                    <div class="meta-author">Ирина Пирожкова, 23 года</div>
                </div>
                <div class="comment">
                    <p>Мне 58 и поверьте <b>мои проблемы в постели старше многих из вас</b>. Уверяю, я перепробовал 95% того, что сейчас предлагается на рынке «мужского здоровья» в том числе и ваш продукт. Еще бы: <b>натуральные, природные компоненты без химии</b> в наше время редкость. Обычно я не трачу свое время на написание отзывов, но здесь не поблагодарить просто не мог. Это <b>чувство невероятной «уверенности», влечения и страсти</b> плюс отсутствие неприятных побочек. Все как будто мне снова 18. Главное — <b>эффект длится гораздо дольше и он более натуральный</b>. В общем примите мою искреннюю благодарность.</p>
                    <div class="meta-author">Антон Муховец, 58 лет</div>
                </div>
            </div>
        </div>
    </div>
    <div class="block5">
        <div class="container">
            <div class="questions">
                <div class="question question1">
                    <h3>НЕ ВРЕДНО ЛИ?</h3>
                    <p>«Rendez Vous» <b>не является<br> лекарственным средством</b>. Продукция прошла<br> сертификацию и <b>не имеет противопоказаний.<br></b>    Более того, кроме природных действующих<br> компонентов (радиола розовая, левзея,<br> элеутерококк колючий и померанец),<br> Rendez Vous <b>содержит<br> витамины и минералы,<br></b> оказывающие полезное<br> действие на организм<br> в целом.</p>
                </div>
                <div class="question question2">
                    <h3>Как работает Rendez Vous?</h3>
                    <p> «Rendez Vous» <b>помогает решить целый спектр проблем.</b> У мужчин это снижение либидо, ослабление потенции, преждевременная эякуляция<br>    и неуверенность в себе; а у женщин: отсутствие оргазма, замкнутость в постели и как следствие — пониженное<br> влечение. С «Rendez Vous»  Вы научитесь <b>получать настоящее наслаждение от секса!</b></p>
                </div>
                <div class="block5-couple"></div>
            </div>
        </div>
    </div>
    <div class="block6">
        <div class="container">
            <div class="block6-left">
                <div class="price-block">
                    <span class="price-text">Цена всего</span>
                    <span class="new-price"><span class="hotprice noflag">  <span class="price_only1206"></span><span class="price_currency"></span>*</span></span>
                    <span class="old-price">Старая цена <span class="hotprice noflag"><span class="price_old1206"></span><span class="price_currency"></span></span></span>
                </div>
                <div class="countdown">
                    <p>До конца акции осталось</p>
                    <div class="time" id="time3">
                        <span class="zero days">00</span>
                        <span class="hours">00</span>
                        <span class="minutes">00</span>
                        <span class="seconds">00</span>
                    </div>
                    <div class="time-desc">
						<span class="zero">дни</span
                        ><span>часы</span
                        ><span>минуты</span
                        ><span>секунды</span>
                    </div>
                </div>
            </div>
            <div class="block6-pack">
                <div class="discount">
                    <div class="discount-image animation-round"></div>
                    <span class="discount-amount">50</span><span class="discount-percent">%</span>
                    <p>скидка</p>
                </div>
            </div>
            <div class="form-block">
                <form action="<?= MonsterleadUrl::rendezVousThankYouUrl() ?>" method="post" class="order-form" id="order_form">

                    <input type="hidden" name='type' required="" class='text' value='<?= \common\models\MonsterleadsRequest::TYPE_RANDEVY ?>' />

                    <div class="row"><span class="icon icon-name"></span>
                        <input name='name' required="" class='text' value='' placeholder="Введите имя" />
                    </div>
                    <div class="row"><span class="icon icon-phone"></span>
                        <input name='phone' required="" id="phone1" class='text' placeholder="Введите номер телефона" value=''/>
                    </div>

                    <div class="row">* цена актуальна при покупке комплекта</span>
                        <!-- + доставка + НДС -->
                    </div>
                    <!-- <div class="row">Итого <span class="itog">  <span class="price_only1206"></span><span class="price_currency"></span>*</span></div> -->

                    <div class="btn-holder"><button type="submit"></button></div>

                </form>
            </div>
        </div>
    </div>
<!--    <div class="footer container">-->
<!--        <div class="footer_text"></div>-->
<!--    </div>-->
</div>



</body>
</html>