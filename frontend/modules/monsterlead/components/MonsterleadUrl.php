<?php
namespace frontend\modules\monsterlead\components;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Site controller
 */
class MonsterleadUrl
{
    /**
     * @param $route
     * @param $params
     *
     * @return string
     */
    public static function createUrl($route, $params)
    {
        return Url::to(
            ArrayHelper::merge(
                [$route],
                $params
            )
        );
    }

    public static function rendezVousLandingUrl($params = [])
    {
        $params = ArrayHelper::merge($params, \Yii::$app->request->get());
        return self::createUrl('/monsterlead/rendez-vous/landing', $params);
    }

    public static function rendezVousThankYouUrl($params = [])
    {
        $params = ArrayHelper::merge($params, \Yii::$app->request->get());
        return self::createUrl('/monsterlead/rendez-vous/thank-you', $params);
    }

}
