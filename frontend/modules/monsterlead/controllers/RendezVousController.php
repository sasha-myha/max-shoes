<?php

namespace frontend\modules\monsterlead\controllers;

use common\models\MonsterleadsRequest;
use yii\web\Controller;

/**
 * Default controller for the `monsterlead` module
 */
class RendezVousController extends Controller
{
    public $layout = false;

    public function beforeAction($action)
    {
        \Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionTransit()
    {
        return $this->render('transit');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionLanding()
    {
        return $this->render('landing');
    }

    /**
     * Send lead
     * @return string
     */
    public function actionThankYou()
    {
        $phone = \Yii::$app->request->post('phone');
        $name = \Yii::$app->request->post('name');
        $type = \Yii::$app->request->post('type');

        if ($phone) {
            $model = new MonsterleadsRequest();
            $model->monsterleads_api_status = MonsterleadsRequest::MONSTERLEADS_API_NEW;
            $model->type = $type;
            $model->phone = $phone;
            $model->name = $name;
            $model->save(false);
        }

        return $this->render('thank-you');
    }
}
