<?php

use console\components\Migration;

/**
 * Class m170228_231817_create_event_table migration
 */
class m170228_231817_create_event_table extends Migration
{
    /**
     * @var string migration table name
     */
    public $tableName = '{{%event}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->unique()->comment('Alias'),
                'description' => $this->text()->null()->comment('Description'),
                'content' => $this->text()->null()->comment('Content'),
                'type' => $this->string()->notNull()->comment('Type'),
                'place_label' => $this->string()->notNull()->defaultValue('')->comment('Place label'),
                'place_coords' => $this->string()->notNull()->defaultValue('')->comment('Place map coordinates'),
                'datetime_start' => $this->dateTime()->null()->comment('Event start date and time'),
                'datetime_end' => $this->dateTime()->null()->comment('Event end date and time'),
                // Max: 99999999999.99
                // Note: sometimes currencies have three or four decimal places, you may need to alter the column.
                'price' => $this->decimal(13, 2)->notNull()->defaultValue(0)->comment('Price'),
                'price_currency' => $this->string()->notNull()->defaultValue('')->comment('Price currency'),
                'publish_at' => $this->dateTime()->notNull()->comment('Publish at'),
                'created_at' => $this->integer()->notNull()->comment('Created at'),
                'updated_at' => $this->integer()->notNull()->comment('Updated at'),
                'published' => $this->smallInteger(1)->notNull()->defaultValue(1)->comment('Published'),
                'edit_lock' => $this->bigInteger()->notNull()->defaultValue(0)->comment('Edit lock'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
