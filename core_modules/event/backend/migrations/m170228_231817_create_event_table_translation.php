<?php

use console\components\Migration;

/**
 * Class m170228_231817_create_event_table_translation migration
 */
class m170228_231817_create_event_table_translation extends Migration
{
    /**
     * @var string migration related table name
     */
    public $tableName = '{{%event_translation}}';

    /**
     * @var string main table name, to make constraints
     */
    public $tableNameRelated = '{{%event}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'description' => $this->text()->comment('Description'),
                'content' => $this->text()->comment('Content'),
                'place_label' => $this->string()->defaultValue('')->notNull()->comment('Place label'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('pk-event_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-event_translation-model_id-event-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

