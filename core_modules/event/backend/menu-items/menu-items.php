<?php

return [
    'label' => Yii::t('back/event', 'Events'),
    'items' => [
        [
            'label' => Yii::t('back/event', 'List'),
            'url' => ['/event/event/index'],
        ],
        [
            'label' => Yii::t('back/event', 'Create'),
            'url' => ['/event/event/create'],
        ],
        [
            'label' => Yii::t('back/event', 'Settings'),
            'url' => ['/event/event-page/update'],
        ],
    ]
];
