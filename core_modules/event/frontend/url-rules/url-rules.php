<?php

return [
    'event/<page:\d+>' => 'event/event/index',
    'event' => 'event/event/index',
    'event/view/<alias:[a-z0-9-]+>' => 'event/event/view',
];
