<?php

use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @var $this yii\web\View
 * @var $searcher \frontend\modules\event\models\EventSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $page common\models\EventPageSettings
 */

$this->title = $page->eventIndexPageTitle;
?>

<h1><?= $this->title ?></h1>

<div class="event-form">

    <?php $form = ActiveForm::begin(['method' => 'get']); ?>

    <div class="row">
        <?= $form->field($searcher, 'label', ['options' => ['class' => 'col-sm-6']])->textInput() ?>
        <?= $form->field($searcher, 'type', ['options' => ['class' => 'col-sm-6']])->textInput() ?>
        <?= $form->field($searcher, 'description', ['options' => ['class' => 'col-sm-6']])->textInput() ?>
        <?= $form->field($searcher, 'place_label', ['options' => ['class' => 'col-sm-6']])->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= ListView::widget([
    'layout' => '{items}',
    'emptyText' => Yii::t('front/event', 'No items found'),
    'dataProvider' => $dataProvider,
    'options' => ['tag' => 'div', 'class' => 'event-items'],
    'itemOptions' => ['tag' => 'div', 'class' => 'event-item'],
    'itemView' => '_item',
]); ?>

<?= LinkPager::widget(['pagination' => $dataProvider->getPagination()]) ?>
