<?php

namespace frontend\modules\event\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use common\models\EventPageSettings;
use frontend\modules\event\models\Event;
use frontend\modules\event\models\EventSearch;
use frontend\components\FrontendController;
use frontend\components\ConfigurationMetaTagRegister;

/**
 * Class EventController
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package frontend\modules\event\controllers
 */
class EventController extends FrontendController
{
    /**
     * Render index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $page = (new EventPageSettings())->get();
        $searcher = new EventSearch();
        $searcher->pageSize = $page->eventIndexPageSize;

        $data['searcher'] = $searcher;
        $data['dataProvider'] = $searcher->searchAll(Yii::$app->getRequest()->getQueryParams());
        $data['page'] = $page;

        ConfigurationMetaTagRegister::register($page);

        return $this->render('index', $data);
    }

    /**
     * Render view page.
     *
     * @param string $alias model alias.
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($alias)
    {
        /* @var Event $model */
        $model = Event::find()
            ->joinWith(['image',])
            ->andWhere(['event.published' => 1, 'event.alias' => $alias])
            ->andWhere(['<=', 'publish_at', date('Y-m-d H:i')])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Page not found');
        }

        MetaTagRegister::register($model);

        return $this->render('view', compact('model'));
    }
}
