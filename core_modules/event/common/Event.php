<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use creocoder\translateable\TranslateableBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property int $id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $content
 * @property string $type
 * @property string $place_label
 * @property string $place_coords
 * @property string $datetime_start
 * @property string $datetime_end
 * @property string $price
 * @property string $price_currency
 * @property string $publish_at
 * @property int $created_at
 * @property int $updated_at
 * @property int $published
 * @property int $edit_lock
 *
 * @property EventTranslation[] $translations
 * @property EntityToFile $image
 */
class Event extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @var string upload file constants.
     */
    const SAVE_ATTRIBUTE_IMAGE = 'EventImage';

    /**
     * @var string event type constants.
     */
    const TYPE_BUSINESS = 'business';
    const TYPE_EDUCATIONAL = 'educational';
    const TYPE_NEGOTIATION = 'negotiation';
    const TYPE_ENTERTAINMENT = 'entertainment';

    /**
     * @var string event currency constants.
     */
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_USD = 'USD';

    /**
     * Get all available event types indexed by their constants values.
     * This static method used in both front and back end applications.
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_BUSINESS => Yii::t('common/event', 'Business'),
            self::TYPE_EDUCATIONAL => Yii::t('common/event', 'Educational'),
            self::TYPE_NEGOTIATION => Yii::t('common/event', 'Negotiation'),
            self::TYPE_ENTERTAINMENT => Yii::t('common/event', 'Entertainment'),
        ];
    }

    /**
     * Get all available event currencies indexed by their constants values.
     * This static method used in both front and back end applications.
     *
     * @return array
     */
    public static function getCurrencies()
    {
        return [
            self::CURRENCY_UAH => Yii::t('common/event', 'UAH'),
            self::CURRENCY_RUB => Yii::t('common/event', 'RUB'),
            self::CURRENCY_USD => Yii::t('common/event', 'USD'),
        ];
    }

    /**
     * Get defined type value.
     *
     * @return string|null
     */
    public function getDefinedType()
    {
        return ArrayHelper::getValue(static::getTypes(), $this->type);
    }

    /**
     * Get defined price currency value.
     *
     * @return string|null
     */
    public function getDefinedCurrency()
    {
        return ArrayHelper::getValue(static::getCurrencies(), $this->price_currency);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
            'description',
            'place_label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(EventTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
