<?php

namespace common\models;

use Yii;
use common\models\base\StaticPage;

/**
 * Class EventPageSettings
 *
 * @author Bogdan Fedun <delagics@gmail.com>
 * @package common\models
 */
class EventPageSettings extends StaticPage
{
    /**
     * @var string event page settings constants.
     * Note: constants names have nothing to do to Yii 2 application runtime events.
     */
    const EVENT_INDEX_PAGE_TITLE = 'eventIndexPageTitle';
    const EVENT_INDEX_PAGE_SIZE = 'eventIndexPageSize';

    /**
     * @var string event index page title.
     */
    public $eventIndexPageTitle;

    /**
     * @var int event index page size.
     */
    public $eventIndexPageSize;

    /**
     * @return array
     */
    public function getKeys()
    {
        return [
            self::EVENT_INDEX_PAGE_TITLE,
            self::EVENT_INDEX_PAGE_SIZE,
        ];
    }

    /**
     * @return $this
     */
    public function get()
    {
        /** @var \common\components\ConfigurationComponent $config */
        $config = Yii::$app->config;

        $this->eventIndexPageTitle = $config->get(self::EVENT_INDEX_PAGE_TITLE);
        $this->eventIndexPageSize = $config->get(self::EVENT_INDEX_PAGE_SIZE);

        return $this;
    }
}
