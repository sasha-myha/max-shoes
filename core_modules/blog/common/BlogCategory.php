<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%blog_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $published
 *
 * @property BlogCategoryTranslation[] $translations
 */
class BlogCategory extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * Receive blog category URL.
     *
     * @return string
     */
    public function getBlogCategoryUrl()
    {
        return static::createUrl('/blog/blog/index', ['category' => $this->alias]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogCategoryTranslation::className(), ['model_id' => 'id']);
    }
}
