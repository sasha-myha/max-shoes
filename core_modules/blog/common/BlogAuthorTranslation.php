<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%blog_author_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $firstname
 * @property string $lastname
 * @property string $bio
 */
class BlogAuthorTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_author_translation}}';
    }
}
