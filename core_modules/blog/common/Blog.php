<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use creocoder\translateable\TranslateableBehavior;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%blog}}".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $content
 * @property integer $published
 * @property string $publish_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BlogTranslation[] $translations
 * @property EntityToFile $image
 */
class Blog extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    const SAVE_ATTRIBUTE_IMAGE = 'BlogImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition([
                'image.entity_model_name' => static::formName(),
                'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE
            ])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
