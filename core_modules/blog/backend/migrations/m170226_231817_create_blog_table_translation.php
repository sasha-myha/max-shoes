<?php

use console\components\Migration;

/**
 * Class m170226_231817_create_blog_table_translation migration
 */
class m170226_231817_create_blog_table_translation extends Migration
{
    /**
     * @var string migration related table name
     */
    public $tableName = '{{%blog_translation}}';

    /**
     * @var string main table name, to make constraints
     */
    public $tableNameRelated = '{{%blog}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'description' => $this->text()->comment('Description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('pk-blog_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-blog_translation-model_id-blog-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

