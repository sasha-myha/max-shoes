<?php

use console\components\Migration;

/**
 * Class m170226_234803_create_blog_author_table migration
 */
class m170226_234803_create_blog_author_table extends Migration
{
    /**
     * @var string migration table name
     */
    public $tableName = '{{%blog_author}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'firstname' => $this->string()->notNull()->comment('First name'),
                'lastname' => $this->string()->notNull()->comment('Last name'),
                'gender' => $this->integer()->notNull()->defaultValue(0)->comment('Gender'),
                'bio' => $this->text()->null()->comment('Bio'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
