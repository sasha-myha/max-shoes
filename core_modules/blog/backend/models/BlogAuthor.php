<?php

namespace backend\modules\blog\models;

use Yii;
use creocoder\translateable\TranslateableBehavior;
use metalguardian\formBuilder\ActiveFormBuilder;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use backend\components\ImperaviContent;
use backend\components\BackendModel;
use backend\components\TranslateableTrait;

/**
 * This is the model class for table "{{%blog_author}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property integer $gender
 * @property string $bio
 *
 * @property BlogAuthorTranslation[] $translations
 */
class BlogAuthor extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_author}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['gender'], 'integer'],
            [['bio'], 'string'],
            [['firstname', 'lastname'], 'string', 'max' => 255],
            [['gender'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blog/author', 'ID'),
            'firstname' => Yii::t('back/blog/author', 'First name'),
            'lastname' => Yii::t('back/blog/author', 'Last name'),
            'gender' => Yii::t('back/blog/author', 'Gender'),
            'bio' => Yii::t('back/blog/author', 'Bio'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'firstname',
            'lastname',
            'bio',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogAuthorTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/blog/author', 'Blog Author');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'firstname',
                    'lastname',
                    'gender',
                    // 'bio:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'firstname',
                    'lastname',
                    'gender',
                    [
                        'attribute' => 'bio',
                        'format' => 'html',
                    ],
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BlogAuthorSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'firstname' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'lastname' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                ],
            ],
            'gender' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'bio' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'bio',
                ]
            ],
        ];

        return $config;
    }
}
