<?php

namespace backend\modules\blog\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%blog_category_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class BlogCategoryTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_category_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/blog/category', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/blog/category', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/blog/category', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
        ];
    }
}
