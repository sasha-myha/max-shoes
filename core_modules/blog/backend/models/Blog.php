<?php

namespace backend\modules\blog\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\behaviors\TimestampBehavior;
use creocoder\translateable\TranslateableBehavior;
use dosamigos\selectize\SelectizeTextInput;
use metalguardian\formBuilder\ActiveFormBuilder;
use metalguardian\dateTimePicker\Widget as DateTimePicker;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use common\models\base\EntityToFile;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\models\Blog as CommonBlog;
use common\models\BlogAuthor as CommonBlogAuthor;
use backend\components\BackendModel;
use backend\components\TranslateableTrait;
use backend\components\ImperaviContent;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\blog\behaviors\BlogCategoryBehavior;

/**
 * This is the model class for table "{{%blog}}".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $content
 * @property integer $published
 * @property string $publish_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BlogTranslation[] $translations
 */
class Blog extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;

    /**
     * @var array blog categories.
     */
    public $blogCategories;

    /**
     * Attribute for imageUploader
     */
    public $image;

    /**
     * Temporary sign which used for saving images before model save
     *
     * @var string
     */
    public $sign;

    public function init()
    {
        parent::init();
        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'publish_at', 'alias'], 'required'],
            [['content', 'description'], 'string'],
            [['published', 'author_id'], 'integer'],
            [['publish_at'], 'datetime', 'format' => 'y-MM-dd HH:mm:ss'], // ICU Date/Time Format Syntax
            [['label', 'alias'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['alias'], 'unique'],
            [['sign'], 'string', 'max' => 255],
            [['blogCategories'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blog', 'ID'),
            'author_id' => Yii::t('back/blog', 'Author'),
            'label' => Yii::t('back/blog', 'Label'),
            'alias' => Yii::t('back/blog', 'Alias'),
            'content' => Yii::t('back/blog', 'Content'),
            'description' => Yii::t('back/blog', 'Description'),
            'published' => Yii::t('back/blog', 'Published'),
            'publish_at' => Yii::t('back/blog', 'Publish at'),
            'created_at' => Yii::t('back/blog', 'Created at'),
            'updated_at' => Yii::t('back/blog', 'Updated at'),
            'image' => Yii::t('back/blog', 'Image'),
            'blogCategories' => Yii::t('back/blog', 'Categories'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
            'description',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'category' => [
                'class' => BlogCategoryBehavior::className(),
                'attribute' => 'blogCategories',
                'relationName' => 'blogCategoryRelations',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/blog', 'Blog');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'content:ntext',
                    'published:boolean',
                    'publish_at',
                    'alias',
                    // 'description:ntext',
                    'author_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'publish_at',
                    'alias',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'author_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BlogSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'description',
                ]
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'author_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => $this->getAuthorsList(),
                'options' => [
                    'prompt' => Yii::t('back/blog', 'Choose author'),
                ],
            ],
            'blogCategories' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => SelectizeTextInput::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'blogCategories',
                    'clientOptions' => [
                        'options' => $this->getMappedCategoriesList('id', 'label'),
                        'valueField' => 'id',
                        'labelField' => 'label',
                        'searchField' => 'label',
                        'preload' => true,
                        'maxItems' => 1,
                        'plugins' => ['remove_button'],
                        'inputClass' => 'form-control selectize-input',
                    ]
                ],
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'publish_at' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options' => [
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'image' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'image',
                    'saveAttribute' => CommonBlog::SAVE_ATTRIBUTE_IMAGE,
                    'aspectRatio' => 2 / 1,
                    'multiple' => false,
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    /**
     * Get available authors list, suitable for selection in drop down lists.
     *
     * @return array
     * @see \common\models\BlogAuthor::getFullname()
     */
    public function getAuthorsList()
    {
        $authors = CommonBlogAuthor::find()->select(['id', 'firstname', 'lastname'])->all();

        return ArrayHelper::map($authors, 'id', 'fullname');
    }

    /**
     * Get available categories list mapped from `$from` to `$to` method params.
     * Method written specially for Selectize plugin.
     *
     * @param string $from BlogCategory attribute name which values will be presented as resulting JSON array keys.
     * @param string $to BlogCategory attribute name which values will be presented as resulting JSON array values.
     * @return \yii\web\JsExpression|string
     */
    public function getMappedCategoriesList($from, $to)
    {
        $output = [];
        $categories = BlogCategory::find()->select([$from, $to])->isPublished()->asArray()->all();
        $categories = ArrayHelper::map($categories, $from, $to);

        if ($categories) {
            foreach ($categories as $id => $label) {
                $output[] = [$from => $id, $to => $label];
            }
        }

        return new JsExpression(Json::encode($output));
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(BlogAuthor::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategoryRelations()
    {
        return $this->hasMany(BlogCategoryRelation::className(), ['blog_id' => 'id']);
    }
}
