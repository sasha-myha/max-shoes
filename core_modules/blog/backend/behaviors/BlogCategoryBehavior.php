<?php

namespace backend\modules\blog\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use backend\modules\blog\models\BlogCategoryRelation;

/**
 * Class BlogCategoryBehavior
 *
 * @property ActiveRecord $owner
 * @package backend\modules\blog\behaviors
 */
class BlogCategoryBehavior extends Behavior
{
    /**
     * @var string owner attribute to which behavior is applied.
     */
    public $attribute;

    /**
     * @var string owner relation name that fetches category IDs from junction table.
     */
    public $relationName;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    /**
     * Preparing attribute value for selectize plugin
     *
     * @return void
     */
    public function afterFind()
    {
        $found = $this->findSaved();

        if ($found) {
            $categories = ArrayHelper::getColumn($found, 'category_id');
            $this->owner->{$this->attribute} = implode(',', $categories);
        }
    }

    /**
     * Deals with saving, updating and deleting relations
     *
     * @return void.
     */
    public function afterSave()
    {
        $value = $this->owner->{$this->attribute};
        $chosen = $value ? explode(',', $value) : [];// Nothing to do, new record, and no chosen categories.

        if (!$this->owner->getIsNewRecord()) {
            // Assume owner record is not new, remove all possible relations.
            $this->afterDelete();
        }

        $this->insertAll($chosen);
    }

    /**
     * Remove all connected relations.
     *
     * @return void
     */
    public function afterDelete()
    {
        $this->owner->unlinkAll($this->relationName, true);
    }

    /**
     * Find saved category relations.
     *
     * @return array
     */
    private function findSaved()
    {
        return $this->owner->getRelation($this->relationName)
            ->select(['category_id'])
            ->orderBy(['position' => SORT_ASC])
            ->asArray()
            ->all();
    }

    /**
     * Insert new relations into the database.
     *
     * @param array $categories list of category IDs connected with owner record.
     * @return void
     */
    private function insertAll($categories)
    {
        if (empty($categories)) {
            return;
        }

        foreach ($categories as $pos => $catId) {
            $model = $this->createNewPopulatedModel($this->owner->primaryKey, $catId, $pos);
            !$model->validate() ?: $this->owner->link($this->relationName, $model);
        }
    }

    /**
     * Create new BlogCategoryRelation model with populated attributes.
     *
     * @param int $blog_id model attribute value.
     * @param int $category_id model attribute value.
     * @param int $position model attribute value.
     * @return BlogCategoryRelation
     */
    private function createNewPopulatedModel($blog_id, $category_id, $position)
    {
        $model = new BlogCategoryRelation();
        $model->setAttributes(compact('blog_id', 'category_id', 'position'));

        return $model;
    }
}
