<?php

namespace frontend\modules\blog\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use common\models\BlogPageSettings;
use frontend\modules\blog\models\Blog;
use frontend\components\FrontendController;
use frontend\components\ConfigurationMetaTagRegister;
use frontend\modules\blog\models\BlogSearch;

/**
 * Class BlogController
 *
 * @package frontend\modules\blog\controllers
 */
class BlogController extends FrontendController
{
    /**
     * Render index page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $page = (new BlogPageSettings())->get();
        $searcher = new BlogSearch();
        $searcher->fixedPostId = $page->blogIndexPageFixedPost;
        $searcher->pageSize = $page->blogIndexPageSize;

        $data = $searcher->searchAll(Yii::$app->getRequest()->getQueryParams());
        $data['page'] = $page;

        ConfigurationMetaTagRegister::register($page);

        return $this->render('index', $data);
    }

    /**
     * Render view page.
     *
     * @param string $alias model alias.
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView($alias)
    {
        /* @var Blog $model */
        $model = Blog::find()
            ->joinWith(['image', 'blogCategories', 'blogAuthor'])
            ->andWhere(['blog.published' => 1, 'blog.alias' => $alias])
            ->andWhere(['<=', 'publish_at', date('Y-m-d H:i')])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('Page not found');
        }

        MetaTagRegister::register($model);

        return $this->render('view', compact('model'));
    }
}
