<?php

namespace frontend\modules\blog\models;

use Yii;
use metalguardian\fileProcessor\helpers\FPM;
use common\models\Blog as BaseBlog;
use common\models\BlogAuthor;
use common\models\BlogCategory;
use common\models\BlogCategoryRelation;

/**
 * This is the model class for table "{{%blog}}".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $label
 * @property string $alias
 * @property string $description
 * @property string $content
 * @property integer $published
 * @property string $publish_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \common\models\BlogTranslation[] $translations
 * @property \common\models\EntityToFile $featuredImage
 */
class Blog extends BaseBlog
{
    /**
     * Get URL string for listing Blog models.
     *
     * @param array $params index route params.
     * @param bool|string $scheme the URI scheme to use in the generated URL.
     * @return string
     */
    public function getIndexUrl($params = [], $scheme = false)
    {
        return static::createUrl('/blog/blog/index', $params, $scheme);
    }

    /**
     * Get URL string for viewing Blog model.
     *
     * @param bool|string $scheme the URI scheme to use in the generated URL.
     * @return string
     */
    public function getViewUrl($scheme = false)
    {
        return static::createUrl('/blog/blog/view', ['alias' => $this->alias], $scheme);
    }

    /**
     * Get image for Blog list(index) page.
     *
     * @param string $module module image size definitions for fileProcessor module.
     * @param string $size module image size definition for fileProcessor module.
     * @param string $method name of FPM method.
     * @param array $options image tag options.
     * @return null|string
     */
    public function getIndexImage($module = 'blog', $size = 'image', $method = 'image', $options = [])
    {
        return isset($this->image->file_id)
            ? FPM::$method($this->image->file_id, $module, $size, $options)
            : null;
    }

    /**
     * Get Open Graph image, with absolute URL.
     *
     * @param string $module module image size definitions for fileProcessor module.
     * @param string $size module image size definition for fileProcessor module.
     * @return string
     */
    public function getOpenGraphImage($module = 'blog', $size = 'image')
    {
        return Yii::$app->getRequest()->getHostInfo() . $this->getIndexImage($module, $size, 'src');
    }

    /**
     * Get Open Graph description meta tag content.
     *
     * @return string
     */
    public function getOpenGraphDescription()
    {
        return strip_tags($this->description);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategoryRelations()
    {
        return $this->hasMany(BlogCategoryRelation::className(), ['blog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategories()
    {
        return $this->hasMany(BlogCategory::className(), ['id' => 'category_id'])
            ->via('blogCategoryRelations');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogAuthor()
    {
        return $this->hasOne(BlogAuthor::className(), ['id' => 'author_id']);
    }
}
