<?php

namespace backend\modules\faq\controllers;

use backend\components\BackendController;
use backend\modules\faq\models\FaqQuestion;

/**
 * FaqQuestionController implements the CRUD actions for FaqQuestion model.
 */
class FaqQuestionController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FaqQuestion::className();
    }
}
