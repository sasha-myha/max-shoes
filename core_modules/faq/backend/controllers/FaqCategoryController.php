<?php

namespace backend\modules\faq\controllers;

use backend\components\BackendController;
use backend\modules\faq\models\FaqCategory;

/**
 * FaqCategoryController implements the CRUD actions for FaqCategory model.
 */
class FaqCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FaqCategory::className();
    }
}
