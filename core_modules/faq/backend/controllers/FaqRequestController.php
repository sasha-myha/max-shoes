<?php

namespace backend\modules\faq\controllers;

use backend\components\BackendController;
use backend\modules\faq\models\FaqRequest;

/**
 * FaqRequestController implements the CRUD actions for FaqRequest model.
 */
class FaqRequestController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FaqRequest::className();
    }
}
