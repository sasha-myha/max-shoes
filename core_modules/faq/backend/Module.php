<?php

namespace backend\modules\faq;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\faq\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
