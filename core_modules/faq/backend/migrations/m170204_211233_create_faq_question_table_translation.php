<?php

use console\components\Migration;

/**
 * Class m170204_211233_create_faq_question_table_translation migration
 */
class m170204_211233_create_faq_question_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%faq_question_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%faq_question}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'question' => $this->text()->comment('Question'),
                'answer' => $this->text()->comment('Answer'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-faq_question_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-faq_question_translation-model_id-faq_question-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

