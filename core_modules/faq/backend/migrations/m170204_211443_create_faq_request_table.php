<?php

use console\components\Migration;

/**
 * Class m170204_211443_create_faq_request_table migration
 */
class m170204_211443_create_faq_request_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%faq_request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'category_id' => $this->integer()->null()->comment('Category'),
                'name' => $this->string()->null()->comment('Name'),
                'email' => $this->string()->null()->comment('Email'),
                'phone' => $this->string()->null()->comment('Phone'),
                'question' => $this->text()->null()->comment('Question'),
                'request_date' => $this->dateTime()->notNull()->comment('Date'),
                'process' => $this->boolean()->notNull()->defaultValue(0)->comment('Process'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-faq_request-category_id-faq_category-id',
            $this->tableName,
            'category_id',
            '{{%faq_category}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-faq_request-category_id-faq_category-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
