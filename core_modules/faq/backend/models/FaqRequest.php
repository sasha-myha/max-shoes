<?php

namespace backend\modules\faq\models;

use Yii;
use common\components\model\ActiveRecord;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%faq_request}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $question
 * @property string $request_date
 * @property integer $process
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FaqCategory $category
 */
class FaqRequest extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%faq_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'process', 'published', 'position'], 'integer'],
            [['question'], 'string'],
            [['request_date'], 'required'],
            [['request_date'], 'date'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\FaqCategory::className(),
                'targetAttribute' => 'id'
            ],
            [['process'], 'default', 'value' => 0],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('faq/back', 'ID'),
            'category_id' => Yii::t('faq/back', 'Category'),
            'name' => Yii::t('faq/back', 'Name'),
            'email' => Yii::t('faq/back', 'Email'),
            'phone' => Yii::t('faq/back', 'Phone'),
            'question' => Yii::t('faq/back', 'Question'),
            'request_date' => Yii::t('faq/back', 'Date'),
            'process' => Yii::t('faq/back', 'Process'),
            'published' => Yii::t('faq/back', 'Published'),
            'position' => Yii::t('faq/back', 'Position'),
            'created_at' => Yii::t('faq/back', 'Created At'),
            'updated_at' => Yii::t('faq/back', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/faq', 'Faq Request');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'category_id',
                        'value' => function ($data) {
                            /** @var FaqRequest $data */
                            return ArrayHelper::getValue($data, 'category.label');
                        },
                        'filter' => \common\models\FaqCategory::getItems(),
                    ],
                    'name',
                    'email:email',
                    'phone',
                    // 'question:ntext',
                    'request_date',
                    'process:boolean',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'category_id',
                        'value' => ArrayHelper::getValue($this, 'category.label'),
                    ],
                    'name',
                    'email:email',
                    'phone',
                    [
                        'attribute' => 'question',
                        'format' => 'html',
                    ],
                    'request_date',
                    'process:boolean',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new FaqRequestSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\FaqCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'question' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \backend\components\ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'question',
                ]
            ],
            'request_date' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => \metalguardian\dateTimePicker\Widget::className(),
                'options' => [
                    'options' => [
                        'class' => 'form-control',
                    ],
                ],
            ],
            'process' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }


}
