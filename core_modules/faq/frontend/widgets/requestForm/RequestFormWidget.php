<?php

namespace frontend\modules\faq\widgets\requestForm;

use frontend\modules\faq\models\FaqCategory;
use frontend\modules\faq\models\FaqRequestForm;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

/**
 * Class RequestFormWidget
 *
 * @package frontend\modules\faq\widgets
 */
class RequestFormWidget extends Widget
{
    /** @var FaqRequestForm|null */
    public $model = null;

    /** @var FaqCategory|null */
    public $category = null;

    /** @var string */
    public $view = 'default';

    public function run()
    {
        /** @var FaqRequestForm $model */
        $model = $this->model;

        if (is_null($model)) {
            $model = new FaqRequestForm();
            $model->category_id = ArrayHelper::getValue($this->category, 'id');
        }

        return $this->render($this->view, ['model' => $model]);
    }
}
