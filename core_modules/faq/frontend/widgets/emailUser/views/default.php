<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\FaqRequest $model
 */
?>
<h1><?= Yii::t('faq/front', 'Thanks for your question') ?></h1>
<p>
    <?= Yii::t('faq/front', 'We will respond to it in the near future') ?>
</p>
