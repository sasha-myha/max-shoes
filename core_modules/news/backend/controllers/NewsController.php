<?php

namespace backend\modules\news\controllers;

use backend\components\BackendController;
use backend\modules\news\models\News;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return News::className();
    }
}
