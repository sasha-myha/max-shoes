<?php

namespace backend\modules\news\controllers;

use backend\modules\configuration\components\ConfigurationController;
use backend\modules\news\models\NewsStaticPage;

/**
 * NewsStaticPageController implements the CRUD actions for NewsStaticPage model.
 */
class NewsStaticPageController extends ConfigurationController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return NewsStaticPage::className();
    }
}
