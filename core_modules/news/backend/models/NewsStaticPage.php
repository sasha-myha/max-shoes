<?php

namespace backend\modules\news\models;

use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\NewsStaticPage as CommonNewsStaticPage;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
/**
* Class NewsStaticPage*/
class NewsStaticPage extends ConfigurationModel
{
    /**
    * Attribute for imageUploader
    */
    public $image;

    public $showAsConfig = false;
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/news-static-page', 'News Static Page');
    }

    /**
    * @return array
    */
    public function getFormRules()
    {
        return [
            [CommonNewsStaticPage::LABEL, 'string'],
            [CommonNewsStaticPage::DESCRIPTION, 'string'],
            [CommonNewsStaticPage::PAGINATION_COUNT, 'required'],
            [CommonNewsStaticPage::PAGINATION_COUNT, 'integer'],
        ];
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
            CommonNewsStaticPage::LABEL => Configuration::TYPE_STRING,
            CommonNewsStaticPage::DESCRIPTION => Configuration::TYPE_TEXT,
            CommonNewsStaticPage::PAGINATION_COUNT => Configuration::TYPE_INTEGER,
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
            CommonNewsStaticPage::LABEL => \Yii::t('back/news-static-page', 'Label'),
            CommonNewsStaticPage::DESCRIPTION => \Yii::t('back/news-static-page', 'Description'),
            CommonNewsStaticPage::PAGINATION_COUNT => \Yii::t('back/news-static-page', 'Pagination Count'),
            'image' => \Yii::t('back/news-static-page', 'Image'),
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
            CommonNewsStaticPage::LABEL => '',
            CommonNewsStaticPage::DESCRIPTION => '',
            CommonNewsStaticPage::PAGINATION_COUNT => '',
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            CommonNewsStaticPage::LABEL,
            CommonNewsStaticPage::DESCRIPTION,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ]);
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Main' => [
                    CommonNewsStaticPage::LABEL,
                    CommonNewsStaticPage::DESCRIPTION,
                    CommonNewsStaticPage::PAGINATION_COUNT,
                    'image' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'image',
                            'saveAttribute' => CommonNewsStaticPage::SAVE_ATTRIBUTE_IMAGE,
                            'aspectRatio' => false, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                ],
            ]
        ];

        return $config;
    }
}
