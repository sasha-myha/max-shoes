<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class NewsStaticPage extends StaticPage
{
    const LABEL = 'newsStaticPageLabel';
    const DESCRIPTION = 'newsStaticPageDescription';
    const PAGINATION_COUNT = 'newsStaticPagePaginationCount';

	const SAVE_ATTRIBUTE_IMAGE = 'NewsStaticPageImage';

    public $label;
    public $description;
    public $paginationСount;

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::LABEL,
            self::DESCRIPTION,
            self::PAGINATION_COUNT,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->label = $config->get(self::LABEL);
        $this->description = $config->get(self::DESCRIPTION);
        $this->paginationСount = $config->get(self::PAGINATION_COUNT);

        return $this;
    }
}
