<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $content
 * @property integer $publish_date
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published
 * @property integer $position
 *
 * @property NewsToNewsCategory[] $newsToNewsCategories
 * @property NewsCategory[] $newsCategories
 * @property NewsTranslation[] $translations
 * @property EntityToFile $image
 */
class News extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    const SAVE_ATTRIBUTE_IMAGE = 'NewsImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsToNewsCategories()
    {
        return $this->hasMany(NewsToNewsCategory::className(), ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsCategories()
    {
        return $this->hasMany(NewsCategory::className(), ['id' => 'category_id'])
            ->viaTable(NewsToNewsCategory::tableName(), ['news_id' => 'id']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['image.entity_model_name' => static::formName(), 'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE])
            ->alias('image')
            ->orderBy('image.position DESC');
    }

    public static function getListPageUrl($params = [])
    {
        return static::createUrl('/news/news/index', $params);
    }

    public function getPageUrl($params = [])
    {
        if (!isset($params['alias'])) {
            $params['alias'] = $this->alias;
        }
        return static::createUrl('/news/news/view', $params);
    }

    public static function getNewsListQuery($categoryAlias = false)
    {
        $query = static::find()
            ->alias('news')
            ->joinWith('newsCategories cat')
            ->where([
                'news.published' => true
            ]);
        if ($categoryAlias) {
            $query->andWhere([
                'cat.alias' => $categoryAlias,
                'cat.published' => true
            ]);
        }
        return $query->orderBy(['news.publish_date' => SORT_DESC]);
    }

    public function getPublishDate()
    {
        return \Yii::$app->formatter->asDatetime($this->publish_date, 'medium');
    }

    public static function findByAlias($alias)
    {
        return static::find()
            ->where([
                'alias' => $alias,
                'published' => true
            ])
            ->one();
    }
}
