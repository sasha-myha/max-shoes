<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%news_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 * @property string $content
 * @property EntityToFile $image
 */
class NewsTranslation extends ActiveRecord 
{
    const SAVE_ATTRIBUTE_IMAGE = 'NewsTranslationImage';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_translation}}';
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['image.entity_model_name' => static::formName(), 'image.attribute' => static::SAVE_ATTRIBUTE_IMAGE])
            ->alias('image')
            ->orderBy('image.position DESC');
    }
}
