<?php
/**
 * @author walter
 */

namespace frontend\modules\news\widgets\categoriesList;

use common\models\NewsCategory;
use yii\base\Widget as BaseWidget;

class Widget extends BaseWidget
{
    public $activeCategoryAlias = false;

    public function run()
    {
        $models = NewsCategory::findAllCategories();
        return $this->render('default', ['models' => $models]);
    }
}