<?php
use metalguardian\fileProcessor\helpers\FPM;

/**
 * @author walter
 *
 * @var $model \common\models\News
 */
?>
<h1>
    <?= $model->label; ?>
</h1>
<div>
    <p><?= $model->getPublishDate(); ?></p>
</div>
<img src="<?= FPM::originalSrc($model->image->file_id ?? null, 'admin', 'file'); ?>" alt="" />
<p><?= $model->content; ?></p>