<?php

namespace common\models;

use common\components\model\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%currency_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property Currency $model
 */
class CurrencyTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('app', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Currency::className(), ['id' => 'model_id']);
    }
}
