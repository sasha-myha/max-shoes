<?php
namespace frontend\modules\currency\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use frontend\modules\currency\models\Currency;

/**
 * Class CurrencySwitcher
 *
 * @package frontend\modules\currency\widgets
 */
class CurrencySwitcher extends Widget
{

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $models = Currency::getCurrencies();
        foreach ($models as $currency) {
            $this->items[] = Html::a($currency->label, Currency::getChangeUrl(['currency' => $currency->code]));
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        return Html::tag('li', implode('</li><li>', $this->items));
    }
}
