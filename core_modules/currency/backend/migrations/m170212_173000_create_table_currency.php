<?php

use console\components\Migration;

/**
 * Class m170212_173000_create_table_currency migration
 */
class m170212_173000_create_table_currency extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%currency}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),

                'code' => $this->string()->notNull()->comment('Code'),
                'rate' => $this->float()->notNull()->defaultValue(1)->comment('Rate'),
                'default' => $this->boolean()->notNull()->defaultValue(0)->comment('Default'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->createIndex('published', $this->tableName, 'published');
        $this->createIndex('position', $this->tableName, 'position');

        $this->batchInsert(
            $this->tableName,
            [
                'id',
                'label',
                'code',
                'rate',
                'default',
                'published',
                'position',
                'created_at',
                'updated_at'
            ],
            [
                [
                    'id' => 1,
                    'label' => 'Hryvnya',
                    'code' => 'UAH',
                    'rate' => 1,
                    'default' => 1,
                    'published' => 1,
                    'position' => 0,
                    'created_at' => time(),
                    'updated_at' => time(),
                ],
                [
                    'id' => 2,
                    'label' => 'US Dollar',
                    'code' => 'USD',
                    'rate' => 8.15,
                    'default' => 0,
                    'published' => 1,
                    'position' => 0,
                    'created_at' => time(),
                    'updated_at' => time(),
                ],
                [
                    'id' => 3,
                    'label' => 'Euro',
                    'code' => 'EUR',
                    'rate' => 10.5,
                    'default' => 0,
                    'published' => 1,
                    'position' => 0,
                    'created_at' => time(),
                    'updated_at' => time(),
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable($this->tableName);
        $this->dropTable($this->tableName);
    }
}
