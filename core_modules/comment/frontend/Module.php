<?php

namespace frontend\modules\comment;

use common\components\interfaces\CoreModuleFrontendInterface;

class Module extends \yii2mod\comments\Module implements CoreModuleFrontendInterface
{
    //public $controllerNamespace = 'frontend\modules\comment\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
