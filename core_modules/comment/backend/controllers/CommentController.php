<?php

namespace backend\modules\comment\controllers;

use backend\components\BackendController;
use backend\modules\comment\models\Comment;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Comment::className();
    }
}
