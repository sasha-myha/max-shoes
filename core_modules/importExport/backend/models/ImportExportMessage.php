<?php

namespace backend\modules\importExport\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%import_export_message}}".
 *
 * @property integer $id
 * @property string $type
 * @property string $handler
 * @property string $message
 * @property integer $is_show
 * @property integer $is_shown
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class ImportExportMessage extends ActiveRecord implements BackendModel
{

    const MESSAGE_TYPE_SUCCESS = 'success';
    const MESSAGE_TYPE_ERROR = 'error';

    const MESSAGE_HANDLER_IMPORT = 1;
    const MESSAGE_HANDLER_EXPORT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%import_export_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'message', 'handler'], 'required'],
            [['is_show', 'is_shown'], 'integer'],
            [['type', 'handler'], 'string', 'max' => 255],
            [['message'], 'string'],
            [['is_show'], 'default', 'value' => 1],
            [['is_shown'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/import-export', 'ID'),
            'type' => Yii::t('back/import-export', 'Type'),
            'handler' => Yii::t('back/import-export', 'Handler'),
            'message' => Yii::t('back/import-export', 'Message'),
            'is_show' => Yii::t('back/import-export', 'Is Show'),
            'is_shown' => Yii::t('back/import-export', 'Is Shown'),
            'created_at' => Yii::t('back/import-export', 'Created At'),
            'updated_at' => Yii::t('back/import-export', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'message',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    public static function getImportModelsQuery()
    {
        return self::find()->where([
            'handler' => self::MESSAGE_HANDLER_IMPORT,
        ])
            ->orderBy([
                'id' => SORT_DESC
            ]);
    }

    public static function getExportModelsQuery()
    {
        return self::find()->where([
            'handler' => self::MESSAGE_HANDLER_EXPORT,
            'is_show' => 0,
        ])
            ->orderBy([
                'id' => SORT_DESC
            ]);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return null;
    }


    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/import-export', 'ImportExport Message');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'type',
                    'message',
                    'is_show:boolean',
                    'is_shown:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'type',
                    'message',
                    'is_show:boolean',
                    'is_shown:boolean',
                ];
                break;
        }

        return [];
    }


    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'type' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'message' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'is_show' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'is_shown' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
        ];

        return $config;
    }
}
