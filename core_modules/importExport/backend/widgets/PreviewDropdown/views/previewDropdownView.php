<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.02.2017
 * Time: 15:52
 */

use yii\helpers\Html;

echo Html::dropDownList('excel-preview-select[]', $selected, $items, ['class' => 'excel-preview-select form-control']);