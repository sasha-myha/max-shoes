<?php
namespace backend\modules\importExport\widgets\Excel;

/**
 * Class Excel
 * @package backend\modules\productAdvanceGrid\widgets\Excel
 */
class Excel extends \moonland\phpexcel\Excel
{
    public function writeFile($sheet)
    {
        if (!isset($this->format)) {
            $this->format = 'Excel2007';
        }
        $objectwriter = \PHPExcel_IOFactory::createWriter($sheet, $this->format);
        $path = 'php://output';
        if (isset($this->savePath) && $this->savePath != null) {
            $path = $this->savePath . '/' . $this->getFileName();
        }
        $objectwriter->save($path);
    }
}
