<?php

namespace backend\modules\importExport\controllers;

use backend\modules\importExport\config\ImportExportConfig;
use backend\modules\importExport\helpers\ImportExportHelper;
use backend\modules\importExport\models\ImportExportMessage;
use backend\modules\importExport\models\ImportForm;
use moonland\phpexcel\Excel;
use vova07\console\ConsoleRunner;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\Controller;

class ImportExportController extends Controller
{
    public function actionIndex()
    {
        $model = new ImportForm();
        $session = \Yii::$app->session;

        //preview
        if ($model->load(\Yii::$app->request->post())) {
            $importType = $model->importType;
            $importFilePath = ImportExportHelper::uploadAndGetPath($model, 'importFile');
            $importFileFormat = ImportExportHelper::getExcelFormat($importFilePath);

            //get data from excel
            $dataForPreview = Excel::import($importFilePath, [
                'format' => $importFileFormat
            ]);

            $dataForPreview = array_slice((array)$dataForPreview, 0, ImportExportConfig::PREVIEW_LINE_NUMBER);
            $dataForPreview = ImportExportHelper::fixExcelPreview($dataForPreview);
            $selectedConfig = ImportExportConfig::getImportConfig()[$importType];

            $session->set('import-file-path', $importFilePath);
            $session->set('import-file-format', $importFileFormat);

            return $this->render('importPreview', [
                'dataForPreview' => $dataForPreview,
                'selectedConfig' => $selectedConfig,
            ]);
        }
        //run import
        $postData = \Yii::$app->request->post();
        if ((isset($postData['excel-preview-select'])) && (isset($postData['import-selected-config']))) {
            $matchColumns = Json::encode($postData['excel-preview-select']);
            $selectedConfig = $postData['import-selected-config'];

            $importFilePath = $session->get('import-file-path');
            $importFileFormat = $session->get('import-file-format');

            $consoleRunner = new ConsoleRunner(['file' => \Yii::getAlias('@app') . '/../yii']);
            //base64 for safe bash command params
            $consoleRunner->run('import-export/import '
                . base64_encode($selectedConfig)
                . ' '
                . base64_encode($matchColumns)
                . ' '
                . base64_encode($importFilePath)
                . ' '
                . base64_encode($importFileFormat));

            \Yii::$app->session->setFlash('success', \Yii::t(
                'back/import-export',
                'Files sent to the imports. We will inform you when it is finished (message will be displayed when you refresh the page)'
            ));
            return $this->redirect(ImportForm::getCancelUrl());
        }

        //general view
        ImportExportConfig::checkImportConfig();
        return $this->render('importForm', ['model' => $model]);
    }


    public function actionImportLog()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ImportExportMessage::getImportModelsQuery(),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('importLog', ['dataProvider' => $dataProvider]);
    }

    public function actionExportLog()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ImportExportMessage::getExportModelsQuery(),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $this->render('exportLog', ['dataProvider' => $dataProvider]);
    }
}
