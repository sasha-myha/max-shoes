<?php

namespace backend\modules\importExport\config;

use backend\modules\importExport\config\base\ImportExportBaseConfig;
use backend\modules\product\models\Product;
use backend\modules\product\models\ProductPacking;

/**
 * Created by PhpStorm.
 * User: art
 * Date: 28.04.2017
 * Time: 11:47
 */
class  ImportExportConfig extends ImportExportBaseConfig
{
    //general settings
    const PREVIEW_LINE_NUMBER  = 10;
    const MESSAGE_LOG_SUCCESS_COLOR = '#90C695';
    const MESSAGE_LOG_ERROR_COLOR = '#F1A9A0';
    const EXPORT_FOLDER = '/uploads/export';


    /**
     * return array
     * example:
     *  return [
     *      [
     *          'label' => 'Product Import',                                //Label in dropdown list on Import Form
     *          'columns' => [                                              //List of columns config (order is not important)
     *              [
     *                  Product::className(),                               //Model class name
     *                  ImportExportBaseConfig::COLUMN_MODEL_IDENTIFIER,    //Column type
     *                  'sku',                                              //Field or function in model
     *                  [                                                   //NOT REQUIRED Other column options
     *                      'excelColumnLabel' => 'SKU',                    //Label in excel file to auto choose in preview
     *                      'previewDropdownLabel' => 'Product SKU',        //Label in column dropdown in preview
     *                  ]
     *              ],
     *          ],
     *          'options' => [                                              //Options not required
     *               'onFound' => ImportExportBaseConfig::ON_FOUND_SKIP,    // default: ImportExportBaseConfig::ON_FOUND_UPDATE
     *               'onSaveError' => ImportExportBaseConfig::ON_SAVE_ERROR_CONTINUE,   // default: ImportExportBaseConfig::ON_SAVE_ERROR_TERMINATE
     *           ]
     *      ]
     *  ];
     */
    protected static function importConfig()
    {
        return [];
    }


    /**
     * @return array
     *  example:
     *  return [
     *      [
     *          'label' => 'ExportConfigName',                                //Label for call config
     *          'columns' => [                                              //List of columns config
     *              [
     *                  ImportExportBaseConfig::EXPORT_COLUMN_FILED,        //Column type
     *                  'sku',                                              //Field or function in model
     *                  [                                                   //NOT REQUIRED Other column options
     *                      'excelColumnLabel' => 'SKU',                    //Excel column label
     *                  ]
     *              ],
     *          ],
     *      ]
     *  ];
     */
    protected static function exportConfig()
    {
        return [];
    }
}