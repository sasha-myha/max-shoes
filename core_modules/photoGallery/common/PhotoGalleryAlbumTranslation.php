<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%photo_gallery_album_translation}}".
 *
 * @property integer $model_id
 * @property string  $language
 * @property string  $label
 * @property string  $description
 * @property string  $content
 * @property string  $author
 */
class PhotoGalleryAlbumTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_gallery_album_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label'       => Yii::t('back/photo', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/photo', 'Description') . ' [' . $this->language . ']',
            'content'     => Yii::t('back/photo', 'Content') . ' [' . $this->language . ']',
            'author'      => Yii::t('back/photo', 'Author') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'content'], 'string'],
            [['label', 'author'], 'string', 'max' => 255],
        ];
    }
}
