<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%photo_gallery_entity}}".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string  $label
 * @property string  $alt
 * @property string  $description
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property FpmFile $file
 */
class PhotoGalleryEntity extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo_gallery_entity}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'alt',
            'description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(FpmFile::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PhotoGalleryEntityTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }
}
