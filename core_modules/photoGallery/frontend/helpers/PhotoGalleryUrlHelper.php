<?php

namespace frontend\modules\photoGallery\helpers;

use common\components\model\Helper;

class PhotoGalleryUrlHelper
{
    use Helper;

    /**
     * Method returns route for photo albums list page
     *
     * @return string
     */
    public static function getPhotoGalleryListRoute(): string
    {
        return '/photoGallery/photo-gallery/list';
    }

    /**
     * Method returns route for photo albums view page
     *
     * @return string
     */
    public static function getPhotoGalleryViewRoute(): string
    {
        return '/photoGallery/photo-gallery/view';
    }

    /**
     * Method returns url for photo albums list page
     *
     * @return string
     */
    public static function getPhotoGalleryListUrl(): string
    {
        return static::createUrl(static::getPhotoGalleryListRoute(), []);
    }

    /**
     * Method returns url for photo albums children list page
     *
     * @param array $params
     *
     * @return string
     */
    public static function getPhotoGalleryParentListUrl(array $params): string
    {
        return static::createUrl(static::getPhotoGalleryListRoute(), $params);
    }

    /**
     * Method returns url for photo albums view page
     *
     * @param array $params
     *
     * @return string
     */
    public static function getPhotoGalleryViewUrl(array $params): string
    {
        return static::createUrl(static::getPhotoGalleryViewRoute(), $params);
    }
}