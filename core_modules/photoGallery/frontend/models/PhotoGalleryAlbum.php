<?php

namespace frontend\modules\photoGallery\models;

use common\components\model\DefaultQuery;
use common\components\Translate;
use frontend\modules\photoGallery\helpers\PhotoGalleryUrlHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class PhotoGalleryAlbum extends \common\models\PhotoGalleryAlbum
{
    use Translate;

    /**
     * Method returns photo album model for given alias
     *
     * @param string $alias
     *
     * @return PhotoGalleryAlbum | null
     */
    public static function findGalleryByAlias(string $alias)
    {
        $query = static::_getDefaultQuery();
        $query->andWhere(['t.alias' => $alias]);

        return $query->one();
    }

    /**
     * Method returns albums dataprovider
     *
     * @param null | int $photoAlbumId You can specify parent album id to get data provider only for this album
     * @param int        $pageSize
     *
     * @return ActiveDataProvider
     */
    public static function getPhotoGalleryAlbumsDataprovider($photoAlbumId = null, int $pageSize = 10): ActiveDataProvider
    {
        $query = static::_getDefaultQuery();

        $query->andWhere(['t.parent_id' => $photoAlbumId]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['defaultPageSize' => $pageSize],
        ]);
    }

    /**
     * Default search query for photo albums
     *
     * @return DefaultQuery
     */
    private static function _getDefaultQuery(): DefaultQuery
    {
        return PhotoGalleryAlbum::find()
            ->alias('t')
            ->where(['t.published' => true])
            ->andWhere('t.parent_id IS NULL OR parent.published IS TRUE')
            ->orderBy(['t.created_at' => SORT_ASC])
            ->joinWith(['parent'])
            ->distinct(true);
    }

    /**
     * Relation for parent photo album
     *
     * @return ActiveQuery
     */
    public function getParent(): ActiveQuery
    {
        return $this->hasOne(PhotoGalleryAlbum::className(), ['id' => 'parent_id'])
            ->alias('parent');
    }

    /**
     * Method returns data provider for photos for current photo album
     *
     * @param int $pageSize
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function getPhotosProvider(int $pageSize = 10): ActiveDataProvider
    {
        $query = EntityToFile::find()
            ->alias('t')
            ->andWhere([
                't.entity_model_id'   => $this->id,
                't.attribute'         => PhotoGalleryAlbum::PHOTO_ALBUM_IMAGES,
                't.entity_model_name' => $this->formName(),
            ])
            ->with([
                'metaData',
                'file file',
            ])
            ->orderBy(['t.position' => SORT_ASC]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['defaultPageSize' => $pageSize],
        ]);
    }

    /**
     * Method return creation date for photo album in given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getCreatedDate(string $format = 'dd.MM.yyyy HH:mm'): string
    {
        return Yii::$app->getFormatter()->asDatetime($this->created_at, $format);
    }

    /**
     * Method returns url for current photo album children albums list page
     *
     * @return string
     */
    public function getAlbumListUrl(): string
    {
        return PhotoGalleryUrlHelper::getPhotoGalleryParentListUrl(['albumAlias' => $this->alias]);
    }

    /**
     * Method returns url for current photo album view page
     *
     * @return string
     */
    public function getAlbumViewUrl(): string
    {
        return PhotoGalleryUrlHelper::getPhotoGalleryViewUrl(['albumAlias' => $this->alias]);
    }
}