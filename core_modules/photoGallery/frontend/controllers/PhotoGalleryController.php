<?php

namespace frontend\modules\photoGallery\controllers;

use frontend\components\FrontendController;
use frontend\modules\photoGallery\models\PhotoGalleryAlbum;
use Yii;
use yii\web\NotFoundHttpException;

class PhotoGalleryController extends FrontendController
{
    /**
     * Method renders list of all albums OR list of child albums for requested album
     *
     * @param string $albumAlias requested album alias
     *
     * @return string
     */
    public function actionList(string $albumAlias = ''): string
    {
        $dataprovider = PhotoGalleryAlbum::getPhotoGalleryAlbumsDataprovider(
            PhotoGalleryAlbum::findGalleryByAlias($albumAlias)->id ?? null
        );

        //TODO: render list
    }

    /**
     * Method renders photos for requested photo album
     *
     * @param string $albumAlias requested album alias
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView(string $albumAlias): string
    {
        if (!($model = PhotoGalleryAlbum::findGalleryByAlias($albumAlias))) {
            throw new NotFoundHttpException(Yii::t('front/photoGallery', 'Photo album not found.'));
        }

        $dataprovider = $model->getPhotosProvider();

        //TODO: render album photos
    }
}