<?php

namespace backend\modules\photoGallery\helpers;

use common\components\model\Helper;

class PhotoGalleryUrlHelper
{
    use Helper;

    public static function getSaveMetaDataRoute(): string
    {
        return '/photoGallery/photo-gallery-entity/save-meta-data';
    }

    public static function getShowMetaDataRoute(): string
    {
        return '/photoGallery/photo-gallery-entity/meta-form';
    }

    public static function getSaveMetaDataUrl(array $params): string
    {
        return static::createUrl(static::getSaveMetaDataRoute(), $params);
    }

    public static function getShowMetaDataFormUrl(array $params): string
    {
        return static::createUrl(static::getShowMetaDataRoute(), $params);
    }
}