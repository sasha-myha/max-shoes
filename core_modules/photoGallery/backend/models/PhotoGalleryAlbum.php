<?php

namespace backend\modules\photoGallery\models;

use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\photoGallery\helpers\PhotoGalleryUrlHelper;
use common\components\model\Translateable;
use common\models\EntityToFile;
use kartik\widgets\DateTimePicker;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * @property PhotoGalleryAlbum $parent
 */
class PhotoGalleryAlbum extends \common\models\PhotoGalleryAlbum implements BackendModel, Translateable
{
    use TranslateableTrait;

    // Date formats used for this model
    const DATETIME_FORMAT_FOR_FORM_PAGE = 'dd.MM.yyyy HH:mm';
    const DATE_FORMAT_FOR_INDEX_PAGE = 'dd.MM.yyyy';

    // String representation of timestamp date values
    public $createdAtString;

    // Attribute for image upload
    public $imageUpload;

    // Attribute for images upload
    public $imagesUpload;

    public $sign;

    /**
     * @param null | int $except
     *
     * @return array
     */
    private static function _getAlbumsForParentship($except = null): array
    {
        $query = PhotoGalleryAlbum::find()
            ->select(['label'])
            ->andWhere(['parent_id' => null])
            ->orderBy(['position' => SORT_ASC])
            ->indexBy('id');

        if ($except) {
            $query->andWhere(['!=', 'id', $except]);
        }

        return $query->column();
    }

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = Yii::$app->getSecurity()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['parent_id', 'published', 'position'], 'integer'],
            [['description', 'content'], 'string'],
            [['label', 'alias', 'author'], 'string', 'max' => 255],
            [
                ['parent_id'],
                'exist',
                'targetClass'     => PhotoGalleryAlbum::className(),
                'targetAttribute' => 'id',
            ],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [
                ['createdAtString'],
                'date',
                'format' => static::DATETIME_FORMAT_FOR_FORM_PAGE,
            ],
            [['sign'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('back/photo', 'ID'),
            'parent_id'       => Yii::t('back/photo', 'Parent album'),
            'label'           => Yii::t('back/photo', 'Label'),
            'alias'           => Yii::t('back/photo', 'Alias'),
            'description'     => Yii::t('back/photo', 'Description'),
            'content'         => Yii::t('back/photo', 'Content'),
            'author'          => Yii::t('back/photo', 'Author'),
            'published'       => Yii::t('back/photo', 'Published'),
            'position'        => Yii::t('back/photo', 'Position'),
            'createdAtString' => Yii::t('back/photo', 'Created date'),
            'imageUpload'     => Yii::t('back/photo', 'Album image'),
            'imagesUpload'    => Yii::t('back/photo', 'Album images'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            ['timestamp' => ['class' => TimestampBehavior::className()]]
        );
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        // Convert timestamp dates into string format readable for humans
        $this->createdAtString = $this->_convertTimestampToString($this->created_at);
    }

    /**
     * @return ActiveQuery
     */
    public function getParent(): ActiveQuery
    {
        return $this->hasOne(PhotoGalleryAlbum::className(), ['id' => 'parent_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/photo', 'Photo Gallery Album');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    'label',
                    'alias',
                    [
                        'attribute' => 'parent_id',
                        'value'     => function (PhotoGalleryAlbum $data) {
                            return $data->parent->label ?? false;
                        },
                        'filter'    => PhotoGalleryAlbum::getItems(),
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'label',
                    'alias',
                    [
                        'attribute' => 'parent_id',
                        'value'     => $this->parent->label ?? false,
                    ],
                    [
                        'attribute' => 'description',
                        'format'    => 'html',
                    ],
                    [
                        'attribute' => 'content',
                        'format'    => 'html',
                    ],
                    'createdAtString',
                    'author',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return PhotoGalleryAlbumSearch
     */
    public function getSearchModel()
    {
        return new PhotoGalleryAlbumSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'imageUpload'     => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'         => $this,
                    'attribute'     => 'imageUpload',
                    'saveAttribute' => static::PHOTO_ALBUM_IMAGE,
                    'multiple'      => false,
                ]),
            ],
            'imagesUpload'    => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'                 => $this,
                    'attribute'             => 'imagesUpload',
                    'saveAttribute'         => static::PHOTO_ALBUM_IMAGES,
                    'multiple'              => true,
                    'showMetaDataBtn'       => true,
                    'renderMetaDataFormUrl' => PhotoGalleryUrlHelper::getShowMetaDataFormUrl(['id' => '']),
                ]),
            ],
            'label'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_name form-control' : 'form-control',
                ],
            ],
            'alias'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_alias form-control' : 'form-control',
                ],
            ],
            'parent_id'       => [
                'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items'   => static::_getAlbumsForParentship($this->id),
                'options' => ['prompt' => Yii::t('back/photo', 'Select parent album')],
            ],
            'description'     => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'content'         => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'createdAtString' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'author'          => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'published'       => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position'        => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign'            => [
                'type'  => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];
    }


    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            // Convert string date values into timestamp values
            $this->created_at = $this->_convertFromStringToTimestamp($this->createdAtString);

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * Method converts timestamp date value from DB to string representation
     *
     * @param int $timestamp
     *
     * @return string
     */
    private function _convertTimestampToString($timestamp): string
    {
        return Yii::$app->getFormatter()->asDatetime(
            $timestamp,
            static::DATETIME_FORMAT_FOR_FORM_PAGE
        );
    }

    /**
     * Method accepts string date representation and converts it to timestamp value
     * If no date value given then method return current time in timestamp
     *
     * @param string | null $date
     *
     * @return int
     */
    private function _convertFromStringToTimestamp($date): int
    {
        return $date
            ? (int)Yii::$app->getFormatter()->asTimestamp($date)
            : time();
    }
}
