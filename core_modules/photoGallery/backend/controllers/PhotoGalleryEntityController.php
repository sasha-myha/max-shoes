<?php

namespace backend\modules\photoGallery\controllers;

use backend\components\BackendController;
use backend\modules\photoGallery\models\PhotoGalleryEntity;
use common\components\model\Translateable;
use Yii;

/**
 * PhototGalleryEntityController implements the CRUD actions for PhotoGalleryEntity model.
 */
class PhotoGalleryEntityController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PhotoGalleryEntity::className();
    }

    /**
     * Method renders meta data form
     *
     * @param int $id
     *
     * @return string
     */
    public function actionMetaForm($id): string
    {
        if (!Yii::$app->getRequest()->isAjax) {
            return $this->redirect('index');
        }

        if (!$id || !is_numeric($id)) {
            return $this->renderAjax('//templates/no-file');
        }

        $model = $this->_getPhotoAlbumEntityModelByFileId((int)$id);

        return $this->_renderMetaDataForm($model);
    }

    /**
     * Method saves meta data form
     *
     * @param $id
     *
     * @return string
     */
    public function actionSaveMetaData($id): string
    {
        if (!Yii::$app->getRequest()->isAjax) {
            return $this->redirect('index');
        }

        $model = $this->_getPhotoAlbumEntityModelByFileId((int)$id);

        if ($this->loadModels($model) && $model->save()) {
            return $this->renderAjax('//templates/meta-data-save-complete');
        }

        return $this->_renderMetaDataForm($model);
    }

    /**
     * Method returns existing meta data model OR new one
     *
     * @param int $id
     *
     * @return PhotoGalleryEntity
     */
    private function _getPhotoAlbumEntityModelByFileId(int $id): PhotoGalleryEntity
    {
        if (!($model = PhotoGalleryEntity::getEntityByFileId((int)$id))) {
            $model = new PhotoGalleryEntity();
            $model->file_id = $id;
        }

        return $model;
    }

    /**
     * @param PhotoGalleryEntity $model
     *
     * @return string
     */
    private function _renderMetaDataForm(PhotoGalleryEntity $model): string
    {
        return $this->renderAjax('meta-data-form', [
            'model'             => $model,
            'action'            => $model->getMetaDataSaveUrl(),
            'translationModels' => $model instanceof Translateable ? $model->getTranslationModels() : [],
        ]);
    }
}
