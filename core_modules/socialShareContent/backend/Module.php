<?php

namespace backend\modules\socialShareContent;

use common\components\interfaces\CoreModuleBackendInterface;

class Module extends \yii\base\Module implements CoreModuleBackendInterface
{
    public $controllerNamespace = 'backend\modules\socialShareContent\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
