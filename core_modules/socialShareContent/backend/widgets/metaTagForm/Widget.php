<?php
namespace backend\modules\socialShareContent\widgets\metaTagForm;

/**
 * Class Widget
 */
class Widget extends \yii\base\Widget
{

    /**
     * @var \yii\db\ActiveRecord
     */
    public $model;

    public $form;

    /**
     * @return null|string
     */
    public function run()
    {
        /**
         * @var \backend\modules\socialShareContent\behavior\SocialShareContentBehavior $behavior
         */
        $behavior = $this->model->getBehavior('socialShareContent');
        if (!$behavior || !$this->form || !$this->model->socialShareContent) {
            return null;
        }

        return $this->render('default', [
            'model' => $this->model->socialShareContent,
            'form' => $this->form,
        ]);
    }
}
