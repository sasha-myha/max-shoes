<?php
namespace backend\modules\socialShareContent\behavior;


use backend\components\Translateable;
use backend\modules\configuration\components\ConfigurationModel;
use backend\modules\socialShareContent\models\SocialShareContent;
use common\helpers\LanguageHelper;
use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Class SocialShareContentBehavior
 */
class SocialShareContentBehavior extends Behavior
{
    /**
     * @var SocialShareContent
     */
    public $socialShareContent;


    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT => 'loadSocialShareContent',
            ActiveRecord::EVENT_AFTER_INSERT => 'saveSocialShareContent',
            ActiveRecord::EVENT_AFTER_UPDATE => 'saveSocialShareContent',
            ActiveRecord::EVENT_AFTER_FIND => 'loadSocialShareContent',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteExistedSocialShareContent',
            ConfigurationModel::EVENT_AFTER_SAVE => 'saveSocialShareContent'
        ];
    }

    /** @inheritdoc */
    public function attach($owner)
    {
        parent::attach($owner);
        $this->loadSocialShareContent();
    }

    /**
     * Load exist meta tags after model find
     */
    public function loadSocialShareContent()
    {
        $this->socialShareContent = $this->getExistedSocialShareContent();
    }

    public function saveSocialShareContent()
    {
        $existing = $this->getExistedSocialShareContent();
        if ($this->loadModels($existing)) {
            $existing->save();
        }
    }

    public function deleteExistedSocialShareContent()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        \Yii::$app->db->createCommand()
            ->delete(SocialShareContent::tableName(), [
                'model_id' => $model->id,
                'model_name' => $model->formName()
            ])
            ->execute();
    }

    /**
     * @return SocialShareContent
     */
    protected function getExistedSocialShareContent()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        $modelName = $model->formName();
        $modelId = $model->id;
        $socialShareContent = SocialShareContent::find()
            ->andWhere([
                'model_name' => $modelName,
                'model_id' => $modelId
            ])
            ->one();
        if (!$socialShareContent) {
            $socialShareContent = new SocialShareContent();
            $socialShareContent->model_id = $modelId;
            $socialShareContent->model_name = $modelName;
        }
        $this->loadModels($socialShareContent);

        return $socialShareContent;
    }

    /**
     * @param \yii\db\ActiveRecord $model
     *
     * @return bool
     */
    public function loadModels($model)
    {
        $loaded = true;
        if ($model instanceof Translateable) {
            $languages = LanguageHelper::getLanguageModels();

            $models = [];
            foreach ($languages as $language) {
                if ($language->locale !== LanguageHelper::getDefaultLanguage()->locale) {
                    $models[$language->locale]= $model->getTranslation($language->locale);
                }
            }

            if (!empty($models)) {
                $loaded &= Model::loadMultiple($models, Yii::$app->request->post());
            }
        }

        $loaded &= $model->load(Yii::$app->request->post());

        return $loaded;
    }
}
