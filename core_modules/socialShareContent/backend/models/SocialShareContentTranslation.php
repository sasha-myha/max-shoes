<?php

namespace backend\modules\socialShareContent\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "social_share_content_translation".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $title
 * @property string $description
 */
class SocialShareContentTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_share_content_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/social_share_content', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/social_share_content', 'Language') . ' [' . $this->language . ']',
            'title' => Yii::t('back/social_share_content', 'Title') . ' [' . $this->language . ']',
            'description' => Yii::t('back/social_share_content', 'Description') . ' [' . $this->language . ']',
        ];
    }
}
