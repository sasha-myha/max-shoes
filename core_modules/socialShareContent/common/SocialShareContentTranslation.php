<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @inheritdoc
 */
class SocialShareContentTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_share_content_translation}}';
    }
}
