<?php

return [
    'label' => Yii::t('back/contact', 'contact'),
    'items' => [
        [
            'label' => Yii::t('back/contact', 'page_content'),
            'url' => ['/contact/contact-page/update'],
        ],
        [
            'label' => Yii::t('back/contact', 'contacts'),
            'url' => ['/contact/contact/index'],
        ],
    ],
];
