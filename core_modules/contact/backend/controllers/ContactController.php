<?php

namespace backend\modules\contact\controllers;

use backend\components\BackendController;
use backend\modules\contact\models\Contact;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Contact::className();
    }
}
