<?php

use console\components\Migration;

/**
 * Class m170302_173048_create_contact_table migration
 */
class m170302_173048_create_contact_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%contact}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey()->comment('ID'),
                'label' => $this->string()->notNull()->comment('Label'),
                'address' => $this->text()->null()->comment('Address'),
                'phone' => $this->string()->null()->comment('Phone'),
                'email' => $this->string()->null()->comment('Email'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer(10)->notNull()->comment('Created At'),
                'updated_at' => $this->integer(10)->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-contact-position',
            'contact',
            'position',
            false
        );
        $this->createIndex(
            'idx-contact-published',
            'contact',
            'published',
            false
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
