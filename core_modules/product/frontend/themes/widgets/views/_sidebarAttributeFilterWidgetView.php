<?php
// Attribute filters
use yii\helpers\Html;

foreach ($attributeFilters as $filterLabel => $filterData): ?>
    <b><?= $filterLabel ?></b>
    <ul>
        <?php foreach ($filterData['values'] as $value => $displayValue):

            $checked = false;
            $disabled = true;
            $selectedFilter = $getData[strval($filterData['alias'])] ?? null;
            if (isset($selectedFilter)) {
                $disabled = false;
                $selectedFilter = explode(',', urldecode($selectedFilter));

                $value = (string)$value;

                if (in_array($value, $selectedFilter)) {
                    $checked = true;
                }
            }

            //if checkbox in one block with active or it in activeAttributeList
            if (isset($activeAttributeFilters[$filterLabel]['values'][$value])) {
                $disabled = false;
            }

            $valueJson = json_encode([
                'id' => strval($filterData['attribute_id']),
                'value' => $value,
                'alias' => strval($filterData['alias']),
            ]);
            ?>
            <li>
                <?=
                Html::beginTag('div', ['class' => 'checkbox']) .
                Html::checkbox('BaseFiltersForm[options][]', $checked, [
                    'id' => 'option' . Yii::$app->security->generateRandomString(5),
                    'value' => $valueJson,
                    'label' => $displayValue,
                    'data-class' => 'base-filter',
                    'uncheck' => null,
                    'disabled' => $disabled,
                ]) .
                Html::endTag('div');
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endforeach; ?>