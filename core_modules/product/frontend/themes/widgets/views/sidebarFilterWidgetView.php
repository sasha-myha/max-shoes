<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 24.07.2017
 * Time: 16:03
 */
/** @var string $pageLabel page title*/
/** @var integer $categoryBoundaryMinPrice min price for all products for this filters choose (without price filter)*/
/** @var integer $categoryBoundaryMaxPrice max price for all products for this filters choose (without price filter)*/
/** @var integer $categoryMinPrice min price for all products for this filters choose (with price filter)*/
/** @var integer $categoryMaxPrice max price for all products for this filters choose (with price filter)*/
/** @var BaseFiltersForm $baseFiltersForm sidebar form*/
/** @var mixed[] $categoryBrands brands of current category*/
/** @var mixed[] $attributeFilters attributes of current category*/
/** @var mixed[] $activeCategoryBrands brands with products of current category*/
/** @var mixed[] $activeAttributeFilters attributes with products of current category*/
/** @var ActiveDataProvider $dataProvider products data provider*/
/** @var string $itemView item view for different views of product: product and packing or only product*/

// ----- SIDE BAR FILTER -----
use frontend\modules\product\models\BaseFiltersForm;
use frontend\modules\product\widgets\SidebarAttributeFilterWidget;
use frontend\modules\product\widgets\SidebarBrandFilterWidget;
use frontend\modules\product\widgets\SidebarPriceFilterWidget;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;

$form = ActiveForm::begin([
    'action' => \common\models\ProductCategory::getCategoryViewUrl(['alias' => $model->alias]),
    'id' => 'base-filters-form',
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ],
    ],
]);

?>
<?php
// price filter
echo SidebarPriceFilterWidget::widget([
    'categoryBoundaryMinPrice' => $categoryBoundaryMinPrice,
    'categoryBoundaryMaxPrice' => $categoryBoundaryMaxPrice,
    'categoryMinPrice' => $categoryMinPrice,
    'categoryMaxPrice' => $categoryMaxPrice,
    'baseFiltersForm' => $baseFiltersForm,
    'form' => $form,
]);
?>


<?php
// brand filter
echo SidebarBrandFilterWidget::widget([
    'categoryBrands' => $categoryBrands,
    'activeCategoryBrands' => $activeCategoryBrands,
]);
?>



<?php
// brand filter
echo SidebarAttributeFilterWidget::widget([
    'attributeFilters' => $attributeFilters,
    'activeAttributeFilters' => $activeAttributeFilters,
]);

ActiveForm::end();
// ----- / SIDE BAR FILTER -----
?>