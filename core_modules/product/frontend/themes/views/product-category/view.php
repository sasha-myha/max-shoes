<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 13.03.2017
 * Time: 01:43
 */

/** @var string $pageLabel page title*/
/** @var integer $categoryBoundaryMinPrice min price for all products for this filters choose (without price filter)*/
/** @var integer $categoryBoundaryMaxPrice max price for all products for this filters choose (without price filter)*/
/** @var integer $categoryMinPrice min price for all products for this filters choose (with price filter)*/
/** @var integer $categoryMaxPrice max price for all products for this filters choose (with price filter)*/
/** @var BaseFiltersForm $baseFiltersForm sidebar form*/
/** @var mixed[] $categoryBrands brands of current category*/
/** @var mixed[] $attributeFilters attributes of current category*/
/** @var mixed[] $activeCategoryBrands brands with products of current category*/
/** @var mixed[] $activeAttributeFilters attributes with products of current category*/
/** @var ActiveDataProvider $dataProvider products data provider*/
/** @var string $itemView item view for different views of product: product and packing or only product*/

use frontend\modules\product\models\BaseFiltersForm;
use yii\data\ActiveDataProvider;

//pjax refresh sidebar filter panel and category product list panel
\yii\widgets\Pjax::begin([
    'id' => 'pjax-catalog-list',
    'timeout' => 25000,
    'linkSelector' => '.pjax_link, .pagination li a',
    'enablePushState' => true,
]);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1> <?= $pageLabel ?> </h1>
        </div>
        <div class="col-md-3">
            <?= \frontend\modules\product\widgets\SidebarFilterWidget::widget([
                'model' => $model,
                'categoryBoundaryMinPrice' => $categoryBoundaryMinPrice,
                'categoryBoundaryMaxPrice' => $categoryBoundaryMaxPrice,
                'categoryMinPrice' => $categoryMinPrice,
                'categoryMaxPrice' => $categoryMaxPrice,
                'baseFiltersForm' => $baseFiltersForm,
                'categoryBrands' => $categoryBrands,
                'activeCategoryBrands' => $activeCategoryBrands,
                'attributeFilters' => $attributeFilters,
                'activeAttributeFilters' => $activeAttributeFilters,
            ])?>

        </div>
        <div class="col-md-9">
            <?php
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => $itemView,
            ]);
            ?>
        </div>
    </div>
</div>

<?php
\yii\widgets\Pjax::end();
?>

