<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 28.03.2017
 * Time: 8:26
 */
use yii\helpers\Html;

echo Html::beginTag('a',['href' => \common\models\Product::getSingleUrl(['id' => $model->id])]);
echo Html::beginTag('div',['class' => 'bg-info']);
echo Html::tag('b',$model->label);
echo Html::tag('p',$model->price);
echo Html::endTag('a');

echo Html::tag('hr');

foreach ($model->packings as $packing){
    $packingLinkText = $packing->packing_link_text == '' ? $packing->label : $packing->packing_link_text;
    echo Html::beginTag('a',['href' => \common\models\Product::getSingleUrl(['id' => $packing->id])]);
    echo Html::tag('p',$packingLinkText);
    echo Html::endTag('a');
}

echo Html::endTag('div');

