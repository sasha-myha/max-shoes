<?php

namespace frontend\modules\product;

use common\components\interfaces\CoreModuleFrontendInterface;

class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    public $controllerNamespace = 'frontend\modules\product\controllers';

    const PACKING_AS_OPTION = 0;
    const PACKING_AS_PRODUCT = 1;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
        \Yii::configure($this, require(\Yii::getAlias('@common') . '/models/ProductConfig.php'));
    }
}
