<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%product_price_type}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductPriceTypeTranslation[] $translations
 */
class ProductPriceType extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_price_type}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductPriceTypeTranslation::className(), ['model_id' => 'id']);
    }
}
