<?php

namespace common\models;

use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%landing_page}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $alias
 * @property string $origin_url
 *
 */
class ProductPresetPage extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_preset_page}}';
    }


    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

}
