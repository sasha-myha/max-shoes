<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;

/**
 * This is the model class for table "{{%product_attribute}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $attribute_type
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductAttributeTranslation[] $translations
 */
class ProductAttribute extends ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;


    CONST TYPE_TEXT_FIELD = 1;
    CONST TYPE_DROP_DOWN_FIELD = 2;
    CONST TYPE_MULTI_SELECT_FIELD = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductAttributeTranslation::className(), ['model_id' => 'id']);
    }
}
