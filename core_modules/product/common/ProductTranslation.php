<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $packing_link_text
 * @property string $label
 * @property string $short_desc
 * @property string $content
 */
class ProductTranslation extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_translation}}';
    }
}
