<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_storehouse_phone}}".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $storehouse_id
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductStorehouse $storehouse
 */
class ProductStorehousePhone extends ActiveRecord 
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_storehouse_phone}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStorehouse()
    {
        return $this->hasOne(ProductStorehouse::className(), ['id' => 'storehouse_id']);
    }
}
