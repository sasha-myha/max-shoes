<?php

namespace common\models;

use Yii;
use common\components\model\ActiveRecord;

/**
 * This is the model class for table "{{%product_availability_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class ProductAvailabilityTranslation extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_availability_translation}}';
    }
}
