<?php
/* @var $this yii\web\View */
/* @var $model \common\components\model\ActiveRecord */
/* @var $form yii\bootstrap\ActiveForm */

/** @var \backend\modules\product\models\ProductCategory $node */
/** @var \backend\components\FormBuilder $form */

$model = $node;
$translationModels = [];
if ($model instanceof \common\components\model\Translateable) {
    $translationModels = $model->getTranslationModels();
}

?>
<div class="menu-form">
    <?php
    $items = [];
    $formConfig = $model->getFormConfig();

    if (isset($formConfig['form-set'])) {
        $i = 0;
        foreach ($formConfig['form-set'] as $tabName => $tabConfig) {
            $class = 'tab_content_' . ++$i;
            $items[] = [
                'label' => $tabName,
                'content' => $form->prepareRows($model, $tabConfig, $translationModels, true),
                'options' => [
                    'class' => $class,
                ],
                'linkOptions' => [
                    'class' => $class,
                ],
            ];
        }
    } else {
        $items[] = [
            'label' => 'Content',
            'content' => $form->prepareRows($model, $formConfig, $translationModels),
            'active' => true,
            'options' => [
                'class' => 'tab_content_content',
            ],
            'linkOptions' => [
                'class' => 'tab_content',
            ],
        ];
    }

    $seo = $model->getBehavior('seo');
    if ($seo && $seo instanceof \notgosu\yii2\modules\metaTag\components\MetaTagBehavior) {
        $seo = \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model, ]);
        $items[] = [
            'label' => 'SEO',
            'content' => $seo,
            'active' => false,
            'options' => [
                'class' => 'tab_seo_content',
            ],
            'linkOptions' => [
                'class' => 'tab_seo',
            ],
        ];
    }
    ?>
    <?= \yii\bootstrap\Tabs::widget(['items' => $items]) ?>
</div>
