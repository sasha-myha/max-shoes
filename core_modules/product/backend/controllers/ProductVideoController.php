<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductVideo;

/**
 * ProductVideoController implements the CRUD actions for ProductVideo model.
 */
class ProductVideoController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductVideo::className();
    }
}
