<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductCategoryToAttributeGroup;

/**
 * ProductCategoryToAttributeGroupController implements the CRUD actions for ProductCategoryToAttributeGroup model.
 */
class ProductCategoryToAttributeGroupController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductCategoryToAttributeGroup::className();
    }
}
