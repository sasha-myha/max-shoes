<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductAttributeToAttributeGroup;

/**
 * ProductAttributeToAttributeGroupController implements the CRUD actions for ProductAttributeToAttributeGroup model.
 */
class ProductAttributeToAttributeGroupController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductAttributeToAttributeGroup::className();
    }
}
