<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductDelivery;
use yii\db\Query;
use yii\web\Response;

/**
 * ProductDeliveryController implements the CRUD actions for ProductDelivery model.
 */
class ProductDeliveryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductDelivery::className();
    }

    public function actionGetSelectItems($search = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (is_null($search)) {
            $search = '';
        }

        $query = new Query();
        $query->select('id, label AS text')
            ->from(ProductDelivery::tableName())
            ->where(['like', 'label', $search])
            ->limit(5);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out['results'] = array_values($data);

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ProductDelivery::find($id)->name];
        }
        return $out;
    }
}
