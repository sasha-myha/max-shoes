<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductToSimilar;

/**
 * ProductToSimilarController implements the CRUD actions for ProductToSimilar model.
 */
class ProductToSimilarController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductToSimilar::className();
    }
}
