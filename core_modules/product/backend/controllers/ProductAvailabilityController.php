<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductAvailability;

/**
 * ProductAvailabilityController implements the CRUD actions for ProductAvailability model.
 */
class ProductAvailabilityController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductAvailability::className();
    }
}
