<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductCategory;

/**
 * ProductCategoryController implements the CRUD actions for ProductCategory model.
 */
class ProductCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductCategory::className();
    }

    public function actionTree()
    {
        return $this->render('tree');
    }
}
