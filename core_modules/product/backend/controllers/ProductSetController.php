<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductSet;

/**
 * ProductSetController implements the CRUD actions for ProductSet model.
 */
class ProductSetController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductSet::className();
    }
}
