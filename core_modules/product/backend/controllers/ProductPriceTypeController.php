<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductPriceType;

/**
 * ProductPriceTypeController implements the CRUD actions for ProductPriceType model.
 */
class ProductPriceTypeController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductPriceType::className();
    }
}
