<?php

namespace backend\modules\product\controllers;

use backend\components\BackendController;
use backend\modules\product\models\ProductToCategory;

/**
 * ProductToCategoryController implements the CRUD actions for ProductToCategory model.
 */
class ProductToCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProductToCategory::className();
    }
}
