<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_price_type_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class ProductPriceTypeTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_price_type_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-price-type', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-price-type', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-price-type', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
         ];
    }
}
