<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use backend\components\TranslateableTrait;
use backend\modules\product\assets\ProductAsset;
use backend\modules\product\components\ManyToManyProductBehavior;
use backend\modules\product\helpers\Select2Helper;
use backend\modules\product\widgets\oneToManyWidget\OneToManyWidget;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\models\ProductAttribute as CommonProductAttribute;
use creocoder\translateable\TranslateableBehavior;
use kartik\widgets\Select2;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/**
 * This is the model class for table "{{%product_attribute}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property integer $attribute_type
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductAttributeTranslation[] $translations
 */
class ProductAttribute extends ActiveRecord implements BackendModel, Translateable
{

    public $groupIds;
    public $groupIdsValue;
    public $options;

    use TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_attribute}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['alias'], 'checkAlias'],
            [['attribute_type', 'published', 'position'], 'integer'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['groupIds', 'options'], 'safe']
        ];
    }

    public function checkAlias($attribute, $params)
    {
        if ($this->alias == 'alias') {
            $this->addError($attribute, Yii::t('back/product-attribute', 'Alias can\'t has name \'alias\''));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-attribute', 'ID'),
            'label' => Yii::t('back/product-attribute', 'Label'),
            'alias' => Yii::t('back/product-attribute', 'Alias'),
            'attribute_type' => Yii::t('back/product-attribute', 'Attribute type'),
            'published' => Yii::t('back/product-attribute', 'Published'),
            'position' => Yii::t('back/product-attribute', 'Position'),
            'created_at' => Yii::t('back/product-attribute', 'Created At'),
            'updated_at' => Yii::t('back/product-attribute', 'Updated At'),
            'groupIds' => Yii::t('back/product-attribute', 'Attribute groups'),
            'options' => Yii::t('back/product-attribute', 'Options'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'manyToManyProduct' => [
                'class' => ManyToManyProductBehavior::className(),
                'currentObj' => $this,
                'currentRelatedColumn' => 'attribute_id',
                'manyToManyConfig' => [
                    'attribute_group' => [
                        'attribute' => 'groupIds',
                        'valueAttribute' => 'groupIdsValue',
                        'relatedColumn' => 'attribute_group_id',
                        'linkTableName' => ProductAttributeToAttributeGroup::tableName(),
                        'relatedObjTableName' => ProductAttributeGroup::tableName(),
                        'isStringReturn' => false
                    ],

                ]

            ]
        ];
    }

    /**
     * @return array
     */
    public function getAttributeTypes()
    {
        return [
            CommonProductAttribute::TYPE_TEXT_FIELD => Yii::t('back/product-attribute', 'Type text'),
            CommonProductAttribute::TYPE_DROP_DOWN_FIELD => Yii::t('back/product-attribute', 'Type dropdown'),
            CommonProductAttribute::TYPE_MULTI_SELECT_FIELD => Yii::t('back/product-attribute', 'Type multiselect'),
        ];
    }

    /**
     * @return mixed|null
     */
    public function getAttributeType()
    {
        $types = $this->getAttributeTypes();
        return $types[$this->attribute_type] ?? null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductAttributeTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-attribute', 'Product Attribute');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    [
                        'attribute' => 'attribute_type',
                        'value' => function ($model) {
                            /** @var ProductAttribute $model */
                            return $model->getAttributeTypes()[$model->attribute_type] ?? null;
                        }
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'attribute_type',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductAttributeSearch();
    }

    /**
     * @return string
     */
    public static function getGroupUrl()
    {
        return '/product/product-attribute-group/get-select-items';
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias form-control'
                ],
            ],
            'attribute_type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => $this->getAttributeTypes(),
                'options' => [
                    'data-type' => 'product_attribute_type_select',
                    'data-options-block' => 'field-productattribute-options',
                    'data-hide-options' => CommonProductAttribute::TYPE_TEXT_FIELD,
                ]
            ],
            'options' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => OneToManyWidget::className(),
                'options' => [
                    'data' => $this->getOptionsValue(),

                ]
            ],
            'groupIds' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'label' => $this->getAttributeLabel('groupIds'),
                'value' => Select2Helper::getAjaxSelect2Widget(
                    $this,
                    'groupIds',
                    static::getGroupUrl(),
                    $this->groupIdsValue,
                    [
                        'multiple' => true,
                    ]
                )
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }

    public function getOptions()
    {
        return $this->hasMany(ProductAttributeOption::className(), ['attribute_id' => 'id']);
    }

    public function getOptionsValue()
    {
        return ArrayHelper::map(
            ProductAttributeOption::find()
                ->where(['attribute_id' => $this->id])
                ->orderBy(['position' => SORT_ASC])
                ->all(),
            'id',
            'label'
        );
    }

    /**
     *
     */
    protected function saveOptions()
    {
        $position = 0;

        $updatedIds = [];
        if (($this->options != null) && ($this->attribute_type != CommonProductAttribute::TYPE_TEXT_FIELD)) {
            foreach ($this->options as $option) {
                if ($option != '') {

                    //find exist attribute
                    $existOption = ProductAttributeOption::find()
                        ->where([
                            'attribute_id' => $this->id,
                            'label' => $option,
                        ])
                        ->one();

                    if (isset($existOption)) {
                        $existOption->position = $position;
                        $existOption->save();
                        $updatedIds[] = $existOption->id;
                    } else {
                        $newOption = new ProductAttributeOption();
                        $newOption->label = $option;
                        $newOption->attribute_id = $this->id;
                        $newOption->position = $position;
                        $newOption->save();

                        $updatedIds[] = $newOption->id;
                    }

                    $position++;
                }
            }

            ProductAttributeOption::deleteAll([
                'AND',
                'attribute_id = :attr_id ',
                ['not in', 'id', $updatedIds]
            ],
                [
                    ':attr_id' => $this->id
                ]);

            ProductEav::deleteAll([
                'AND',
                'attribute_id = :attr_id ',
                ['not in', 'value', $updatedIds]
            ],
                [
                    ':attr_id' => $this->id
                ]);

        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveOptions();
    }

}
