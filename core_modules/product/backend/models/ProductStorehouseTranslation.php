<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_storehouse_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $address
* @property string $work_time
* @property string $short_desc
* @property string $content
*/
class ProductStorehouseTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_storehouse_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-storehouse', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-storehouse', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-storehouse', 'Label') . ' [' . $this->language . ']',
            'address' => Yii::t('back/product-storehouse', 'Address') . ' [' . $this->language . ']',
            'work_time' => Yii::t('back/product-storehouse', 'Work time') . ' [' . $this->language . ']',
            'short_desc' => Yii::t('back/product-storehouse', 'Short Description') . ' [' . $this->language . ']',
            'content' => Yii::t('back/product-storehouse', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['label', 'address', 'work_time', 'short_desc'], 'string', 'max' => 255],
         ];
    }
}
