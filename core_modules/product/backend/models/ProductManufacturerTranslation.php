<?php

namespace backend\modules\product\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%product_manufacturer_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $short_desc
* @property string $content
*/
class ProductManufacturerTranslation extends ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_manufacturer_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-manufacturer', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-manufacturer', 'Language') . ' [' . $this->language . ']',
            'short_desc' => Yii::t('back/product-manufacturer', 'Short description') . ' [' . $this->language . ']',
            'content' => Yii::t('back/product-manufacturer', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['short_desc'], 'string', 'max' => 255],
         ];
    }
}
