<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_to_attribute_group}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attribute_group_id
 *
 * @property Product $product
 */
class ProductToAttributeGroup extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_attribute_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attribute_group_id'], 'required'],
            [['product_id', 'attribute_group_id'], 'integer'],
            [['product_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-to-attribute-group', 'ID'),
            'product_id' => Yii::t('back/product-to-attribute-group', 'Product'),
            'attribute_group_id' => Yii::t('back/product-to-attribute-group', 'Attribute'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-to-attribute-group', 'Product To Attribute Group');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'product_id',
                    'attribute_group_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'product_id',
                    'attribute_group_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductToAttributeGroupSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'product_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Product::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'attribute_group_id' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
        ];

        return $config;
    }
}
