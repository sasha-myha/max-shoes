<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_delivery_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $short_desc
*/
class ProductDeliveryTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_delivery_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-delivery', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-delivery', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-delivery', 'Label') . ' [' . $this->language . ']',
            'short_desc' => Yii::t('back/product-delivery', 'Short description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'short_desc'], 'string', 'max' => 255],
         ];
    }
}
