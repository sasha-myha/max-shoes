<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use common\components\model\ActiveRecord;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;

/**
 * This is the model class for table "{{%product_to_category}}".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $category_id
 *
 * @property ProductCategory $category
 * @property Product $product
 */
class ProductToCategory extends ActiveRecord implements BackendModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_to_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id'], 'required'],
            [['product_id', 'category_id'], 'integer'],
            [['product_id'], 'exist', 'targetClass' => \common\models\Product::className(), 'targetAttribute' => 'id'],
            [
                ['category_id'],
                'exist',
                'targetClass' => \common\models\ProductCategory::className(),
                'targetAttribute' => 'id'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-to-category', 'ID'),
            'product_id' => Yii::t('back/product-to-category', 'Product id'),
            'category_id' => Yii::t('back/product-to-category', 'Category id'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-to-category', 'Product To Category');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'product_id',
                    'category_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'product_id',
                    'category_id',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductToCategorySearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'product_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\Product::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'category_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => \common\models\ProductCategory::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
        ];

        return $config;
    }
}
