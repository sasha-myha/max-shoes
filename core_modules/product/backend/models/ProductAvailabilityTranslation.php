<?php

namespace backend\modules\product\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_availability_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 */
class ProductAvailabilityTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_availability_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product-availability', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product-availability', 'Language') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product-availability', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 255],
        ];
    }
}
