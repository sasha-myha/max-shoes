<?php

namespace backend\modules\product\models;

use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\product\assets\ProductAsset;
use backend\modules\product\components\NestedSetsBehavior;
use backend\modules\product\helpers\StringHelper;
use backend\modules\product\widgets\attributesWidget\AttributesWidget;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use common\models\EntityToFile;
use common\models\ProductCategory as CommonProductCategory;
use creocoder\translateable\TranslateableBehavior;
use kartik\tree\models\TreeTrait;
use metalguardian\formBuilder\ActiveFormBuilder;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Inflector;

/**
 * This is the model class for table "{{%product_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $alias
 * @property string $short_desc
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $left
 * @property integer $depth
 * @property integer $right
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProductCategoryTranslation[] $translations
 */
class ProductCategory extends ActiveRecord implements BackendModel, Translateable
{
    use TranslateableTrait;
    use TreeTrait;

    /**
     * main brand image
     * @var
     */
    public $titleImage;

    public $attributeGroupsSelected;
    public $attributesSelected;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }

        $this->published = 1;
    }

    /**
     * @var string the classname for the TreeQuery that implements the NestedSetQueryBehavior.
     * If not set this will default to `kartik    ree\models\TreeQuery`.
     */
    public static $treeQueryClass; // change if you need to set your own TreeQuery

    /**
     * @var bool whether to HTML encode the tree node names. Defaults to `true`.
     */
    public $encodeNodeNames = true;

    /**
     * @var bool whether to HTML purify the tree node icon content before saving.
     * Defaults to `true`.
     */
    public $purifyNodeIcons = true;

    /**
     * @var array activation errors for the node
     */
    public $nodeActivationErrors = [];

    /**
     * @var array node removal errors
     */
    public $nodeRemovalErrors = [];

    /**
     * @var bool attribute to cache the `active` state before a model update. Defaults to `true`.
     */
    public $activeOrig = true;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'alias'], 'required'],
            [['content'], 'string'],
            [['sign','attributeGroupsSelected','attributesSelected'], 'safe'],
            [['published', 'position', 'left', 'depth', 'right'], 'integer'],
            [['label', 'alias', 'short_desc'], 'string', 'max' => 255],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/product-category', 'ID'),
            'label' => Yii::t('back/product-category', 'Label'),
            'alias' => Yii::t('back/product-category', 'Alias'),
            'short_desc' => Yii::t('back/product-category', 'Short description'),
            'content' => Yii::t('back/product-category', 'Content'),
            'published' => Yii::t('back/product-category', 'Published'),
            'position' => Yii::t('back/product-category', 'Position'),
            'left' => Yii::t('back/product-category', 'Left'),
            'depth' => Yii::t('back/product-category', 'Depth'),
            'right' => Yii::t('back/product-category', 'Right'),
            'created_at' => Yii::t('back/product-category', 'Created At'),
            'updated_at' => Yii::t('back/product-category', 'Updated At'),
            'titleImage' => Yii::t('back/product-brand', 'Title image'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'short_desc',
            'content',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => MetaTagBehavior::className(),
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                'leftAttribute' => 'left',
                'rightAttribute' => 'right',
                'depthAttribute' => 'depth',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName()])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }


    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new ProductCategoryQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProductCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/product-category', 'Product Category');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroups()
    {
        return $this->hasMany(
            ProductAttributeGroup::className(),
            ['id' => 'attribute_group_id']
        )->viaTable(
            ProductCategoryToAttributeGroup::tableName(),
            ['category_id' => 'id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributesObj()
    {
        return $this->hasMany(
            ProductAttribute::className(),
            ['id' => 'attribute_id']
        )->viaTable(
            ProductCategoryToAttribute::tableName(),
            ['category_id' => 'id']
        );
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    'alias',
                    'short_desc',
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'short_desc',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProductCategorySearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $module = \Yii::$app->getModule('product');
        $config = [
            'id' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_name_dynamic form-control'
                ],
            ],
            'alias' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class' => 's_alias_dynamic form-control'
                ],
            ],
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleImage',
                    'saveAttribute' => CommonProductCategory::SAVE_ATTRIBUTE_IMAGES,
                    'aspectRatio' => 420 / 210,
                    'multiple' => false,
                ])
            ],
            'short_desc' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
                'options' => [
                    'model' => $this,
                    'attribute' => 'content',
                ]
            ],
            'attributes' => [
                'type' => $module->params['useEav'] ? ActiveFormBuilder::INPUT_WIDGET : ActiveFormBuilder::INPUT_RAW,
                'label' => $module->params['useEav'] ? Yii::t('back/product', 'Attributes') : false,
                'widgetClass' => AttributesWidget::className(),
                'options' => [
                    'showTable' => false,
                ]
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    /**
     *
     */
    protected function saveAttributes()
    {
        //delete old
        ProductCategoryToAttributeGroup::deleteAll(['category_id' => $this->id]);
        ProductCategoryToAttribute::deleteAll(['category_id' => $this->id]);


        //save attributes
        if (isset($this->attributeGroupsSelected)) {
            $attributesGroup = $this->attributeGroupsSelected;

            foreach ($attributesGroup as $groupId) {
                $productToGroup = new ProductCategoryToAttributeGroup();
                $productToGroup->category_id = $this->id;
                $productToGroup->attribute_group_id = $groupId;
                $productToGroup->save();
            }
        }

        if (isset($this->attributesSelected)) {
            $attributesIds = $this->attributesSelected;

            foreach ($attributesIds as $attrId) {
                $productToGroup = new ProductCategoryToAttribute();
                $productToGroup->category_id = $this->id;
                $productToGroup->attribute_id = $attrId;
                $productToGroup->save();
            }
        }
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->alias = Inflector::slug(Inflector::transliterate($this->alias));
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        $module = \Yii::$app->getModule('product');

        if ((isset($module)) && ($module->params['useEav'])) {
            $this->saveAttributes();
        }

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
