<?php

namespace backend\modules\product\models;

use Yii;

/**
* This is the model class for table "{{%product_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $packing_link_text
* @property string $label
* @property string $short_desc
* @property string $content
*/
class ProductTranslation extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%product_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('back/product', 'Related model id') . ' [' . $this->language . ']',
            'language' => Yii::t('back/product', 'Language') . ' [' . $this->language . ']',
            'packing_link_text' => Yii::t('back/product', 'Packing link text') . ' [' . $this->language . ']',
            'label' => Yii::t('back/product', 'Label') . ' [' . $this->language . ']',
            'short_desc' => Yii::t('back/product', 'Short description') . ' [' . $this->language . ']',
            'content' => Yii::t('back/product', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['packing_link_text', 'label', 'short_desc'], 'string', 'max' => 255],
         ];
    }
}
