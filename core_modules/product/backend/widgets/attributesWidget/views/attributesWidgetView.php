<?php
/**
 * Created by PhpStorm.
 * User: art
 * Date: 26.02.2017
 * Time: 16:58
 */
/** @var mixed[] $selectedAttributeGroups */
/** @var mixed[] $selectedAttributeGroupsId */
/** @var mixed[] $selectedAttributes */
/** @var mixed[] $selectedAttributesId */
/** @var string $groupName */
/** @var string $attributeGroupUrl */
/** @var string $attributeName */
/** @var string $attributeId */
/** @var string $groupId */
/** @var string $attributesByGroupUrl */
/** @var string $attributesByIdUrl */
/** @var string $attributeUrl */
/** @var string $attributeTableId */
/** @var string $getValueUrl */
/** @var boolean $showTable */

use yii\bootstrap\Html;

echo \yii\helpers\Html::beginTag('div', ['class' => 'product-attribute-config-field']);
echo Html::tag('div', Yii::t('back/product', 'Choose attributes group (for specific attributes auto selection):'));

echo \kartik\widgets\Select2::widget([
    'data' => $selectedAttributeGroups,
    'value' => $selectedAttributeGroupsId,
    'name' => $groupName,
    'options' => [
        'id' => $groupId,
        'multiple' => true,
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0,
        'ajax' => [
            'url' => $attributeGroupUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }'),
            'cache' => true
        ],
    ],
    'pluginEvents' => [
        'change' => "function() { updateAttributes($(this).val(),'" . $attributeId  . "'); }",
    ]
]);

echo Html::tag('div', Yii::t('back/product', 'Or choose (complete) attributes manually:'));
echo \kartik\widgets\Select2::widget([
    'data' => $selectedAttributes,
    'value' => $selectedAttributesId,
    'name' => $attributeName,
    'options' => [
        'id' => $attributeId,
        'multiple' => true,
        'data-attributes-by-group-url' => $attributesByGroupUrl,
        'data-attributes-by-id-url' => $attributesByIdUrl,
        'data-table-id' => $attributeTableId,
        'data-group-id' => $groupId,
        'data-class-name' => $className,
        'class' => 'choose-product-attribute'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 0,
        'ajax' => [
            'url' => $attributeUrl,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new \yii\web\JsExpression('function(params) { return {q:params.term}; }'),
            'cache' => true
        ],
    ],
    'pluginEvents' => [
        'change' => "function() { updateAttributesTable($(this),'" . $attributeTableId . "'); }",
    ]
]);


echo Html::beginTag(
    'table',
    [
        'id' => $attributeTableId,
        'class' => 'table attribute_table',
        'data-model-id' => $model->id,
        'data-get-value-url' => $getValueUrl,
        'data-class-name' => $className,
        'style' => [
            'display' => $showTable ? 'contents' : 'none',
        ]
    ]
);
echo Html::beginTag('tr');
echo Html::beginTag('th');
echo Yii::t('back/attributes-widget-view', 'label');
echo Html::endTag('th');

echo Html::beginTag('th', ['class' => 'product-attribute-th']);
echo Yii::t('back/attributes-widget-view', 'value');
echo Html::endTag('th');
echo Html::endTag('tr');
echo Html::endTag('table');


echo \yii\helpers\Html::endTag('div');
$this->registerJS(' $("#' . $attributeId . '").trigger("change"); updateRelatedIndex("'.$className.'","'.$attributeId.'","'.$groupId.'");');
