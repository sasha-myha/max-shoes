<?php

use console\components\Migration;

/**
 * Class m170310_115513_create_product_price_type_table_translation migration
 */
class m170310_115513_create_product_price_type_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_price_type_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_price_type}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_price_type_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_price_type_translation-model_id-product_price_type-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
