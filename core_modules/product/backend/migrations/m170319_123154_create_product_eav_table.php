<?php

use console\components\Migration;

/**
 * Class m170319_123154_create_product_eav_table migration
 */
class m170319_123154_create_product_eav_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_eav}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->notNull()->comment('Product'),
                'attribute_id' => $this->integer()->notNull()->comment('Attribute'),
                'value' => $this->string()->null()->comment('Value'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product_eav-product_id-product-id',
            $this->tableName,
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product_eav-attribute_id-product_attribute-id',
            $this->tableName,
            'attribute_id',
            '{{%product_attribute}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_eav-product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product_eav-attribute_id-product_attribute-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
