<?php

use console\components\Migration;

/**
 * Class m170308_225608_create_product_delivery_table_translation migration
 */
class m170308_225608_create_product_delivery_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_delivery_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_delivery}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'short_desc' => $this->string()->comment('Short description'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_delivery_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_delivery_translation-model_id-product_delivery-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
