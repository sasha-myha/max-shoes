<?php

use console\components\Migration;

/**
 * Class m170710_134733_create_product_preset_page_table migration
 */
class m170710_134733_create_product_preset_page_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_preset_page}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'origin_url' => $this->string()->notNull()->comment('Origin URL'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
