<?php

use console\components\Migration;

/**
 * Class m170310_140001_create_product_table migration
 */
class m170310_140001_create_product_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'is_packing' => $this->boolean()->notNull()->defaultValue(0)->comment('Is packing'),
                'parent_product_id' => $this->integer()->null()->comment('Parent product'),
                'packing_link_text' => $this->string()->null()->comment('Packing link text'),
                'sku' => $this->string()->null()->unique()->comment('SKU'),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'brand_id' => $this->integer()->null()->comment('Brand'),
                'manufacturer_id' => $this->integer()->null()->comment('Manufacturer'),
                'availability_id' => $this->integer()->null()->comment('Availability'),
                'short_desc' => $this->string()->null()->comment('Short description'),
                'content' => $this->text()->null()->comment('Content'),
                'price' => $this->decimal(10, 2)->null()->comment('Price'),
                'old_price' => $this->decimal(10, 2)->null()->comment('Old Price'),
                'price_type' => $this->integer()->null()->comment('Price type'),
                'rating_total' => $this->float()->null()->comment('Rating total'),
                'rating_voice_count' => $this->integer()->null()->comment('Rating voice count'),
                'show_as_separate' => $this->boolean()->notNull()->defaultValue(1)->comment('Show as separate'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->addForeignKey(
            'fk-product-parent_product_id-product-id',
            $this->tableName,
            'parent_product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product-brand_id-product_brand-id',
            $this->tableName,
            'brand_id',
            '{{%product_brand}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product-manufacturer_id-product_manufacturer-id',
            $this->tableName,
            'manufacturer_id',
            '{{%product_manufacturer}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-product-availability_id-product_availability-id',
            $this->tableName,
            'availability_id',
            '{{%product_availability}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product-parent_product_id-product-id', $this->tableName);
        $this->dropForeignKey('fk-product-brand_id-product_brand-id', $this->tableName);
        $this->dropForeignKey('fk-product-manufacturer_id-product_manufacturer-id', $this->tableName);
        $this->dropForeignKey('fk-product-availability_id-product_availability-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
