<?php

use console\components\Migration;

/**
 * Class m170310_115513_create_product_price_type_table migration
 */
class m170310_115513_create_product_price_type_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_price_type}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
