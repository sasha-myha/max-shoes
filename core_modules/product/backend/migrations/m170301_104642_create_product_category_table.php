<?php

use console\components\Migration;

/**
 * Class m170301_104642_create_product_category_table migration
 */
class m170301_104642_create_product_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),
                'short_desc' => $this->string()->null()->comment('Short description'),
                'content' => $this->text()->null()->comment('Content'),
                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'left' => $this->integer()->notNull()->comment('Left'),
                'depth' => $this->integer()->notNull()->comment('Depth'),
                'right' => $this->integer()->notNull()->comment('Right'),
                'root' => $this->integer()->comment('Root'),
                'icon' => $this->string()->comment('Icon'),
                'icon_type' => $this->boolean()->defaultValue(1)->comment('Icon'),
                'active' => $this->boolean()->defaultValue(true)->comment('Active'),
                'selected' => $this->boolean()->defaultValue(false)->comment('Selected'),
                'disabled' => $this->boolean()->defaultValue(false)->comment('Disabled'),
                'readonly' => $this->boolean()->defaultValue(false)->comment('Readonly'),
                'visible' => $this->boolean()->defaultValue(true)->comment('Visible'),
                'collapsed' => $this->boolean()->defaultValue(false)->comment('Collapsed'),
                'movable_u' => $this->boolean()->defaultValue(true)->comment('Movable up'),
                'movable_d' => $this->boolean()->defaultValue(true)->comment('Movable down'),
                'movable_l' => $this->boolean()->defaultValue(true)->comment('Movable left'),
                'movable_r' => $this->boolean()->defaultValue(true)->comment('Movable right'),
                'removable' => $this->boolean()->defaultValue(true)->comment('Removable'),
                'removable_all' => $this->boolean()->defaultValue(false)->comment('Removable all'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
