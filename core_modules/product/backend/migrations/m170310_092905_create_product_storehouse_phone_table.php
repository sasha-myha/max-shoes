<?php

use console\components\Migration;

/**
 * Class m170310_092905_create_product_storehouse_phone_table migration
 */
class m170310_092905_create_product_storehouse_phone_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%product_storehouse_phone}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'phone' => $this->string()->notNull()->comment('Phone'),
                'storehouse_id' => $this->integer()->notNull()->comment('Storehouse'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
        $this->createIndex(
            'idx-product_storehouse_phone-storehouse_id',
            'product_storehouse_phone',
            'storehouse_id',
            false
        );
        $this->addForeignKey(
            'fk-product_storehouse_phone-storehouse_id-product_storehouse-id',
            $this->tableName,
            'storehouse_id',
            '{{%product_storehouse}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_storehouse_phone-storehouse_id-product_storehouse-id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
