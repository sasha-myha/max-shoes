<?php

use console\components\Migration;

/**
 * Class m170319_090641_create_product_attribute_table_translation migration
 */
class m170319_090641_create_product_attribute_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_attribute_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_attribute}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_attribute_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_attribute_translation-model_id-product_attribute-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
