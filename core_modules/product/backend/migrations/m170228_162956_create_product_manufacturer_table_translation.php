<?php

use console\components\Migration;

/**
 * Class m170228_162956_create_product_manufacturer_table_translation migration
 */
class m170228_162956_create_product_manufacturer_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_manufacturer_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_manufacturer}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'short_desc' => $this->string()->comment('Short description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_m_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_m_translation-model_id-p_m-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
