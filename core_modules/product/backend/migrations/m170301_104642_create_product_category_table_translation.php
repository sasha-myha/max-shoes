<?php

use console\components\Migration;

/**
 * Class m170301_104642_create_product_category_table_translation migration
 */
class m170301_104642_create_product_category_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_category_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
                'short_desc' => $this->string()->comment('Short description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_category_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_category_translation-model_id-product_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
