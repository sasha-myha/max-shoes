<?php

use console\components\Migration;

/**
 * Class m170228_141941_create_product_brand_table_translation migration
 */
class m170228_141941_create_product_brand_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_brand_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_brand}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'short_desc' => $this->string()->comment('Short Description'),
                'content' => $this->text()->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-product_brand_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-product_brand_translation-model_id-product_brand-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
