<?php

use console\components\Migration;

/**
 * Class m170319_141915_create_product_attribute_group_table_translation migration
 */
class m170319_141915_create_product_attribute_group_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%product_attribute_group_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%product_attribute_group}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_a_g_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_a_g_translation-model_id-p_a_g-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
