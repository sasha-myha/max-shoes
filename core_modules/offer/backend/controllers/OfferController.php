<?php

namespace backend\modules\offer\controllers;

use backend\components\BackendController;
use backend\modules\offer\models\Offer;

/**
 * OfferController implements the CRUD actions for Offer model.
 */
class OfferController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Offer::className();
    }
}
