<?php

use console\components\Migration;

/**
 * Class m170313_092755_create_offer_table migration
 */
class m170313_092755_create_offer_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%offer}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),
                'alias' => $this->string()->notNull()->comment('Alias'),

                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(true)->comment('Published'),
                'position'  => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'begin_date' => $this->integer()->notNull()->comment('Begin date'),
                'end_date'    => $this->integer()->notNull()->comment('End date'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
