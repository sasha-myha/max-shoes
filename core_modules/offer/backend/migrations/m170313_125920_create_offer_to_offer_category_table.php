<?php

use console\components\Migration;

/**
 * Class m170313_125920_create_offer_to_offer_category_table migration
 */
class m170313_125920_create_offer_to_offer_category_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%offer_to_offer_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'offer_id'          => $this->integer()->unsigned()->notNull(),
                'offer_category_id' => $this->integer()->unsigned()->notNull(),
            ],
            $this->tableOptions
        );

        $this->createIndex('pk-offer_id-offer_category_id', $this->tableName, ['offer_id', 'offer_category_id'], true);
        $this->createIndex('key-offer_category_id', $this->tableName, 'offer_category_id');

        $this->addForeignKey(
            'fk-offer_to_offer_category-offer_id-to-offer-id',
            $this->tableName,
            'offer_id',
            '{{%offer}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-offer_category_id-to-offer_category-id',
            $this->tableName,
            'offer_category_id',
            '{{%offer_category}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
