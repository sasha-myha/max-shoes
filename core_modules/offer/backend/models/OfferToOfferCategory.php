<?php

namespace backend\modules\offer\models;

class OfferToOfferCategory extends \common\models\OfferToOfferCategory
{
    /**
     * Method saves offer categories to offer
     *
     * @param array $categories
     */
    public static function saveCategories($categories)
    {
        static::getDb()->createCommand()
            ->batchInsert(
                static::tableName(),
                ['offer_id', 'offer_category_id'],
                $categories
            )
            ->execute();
    }
}