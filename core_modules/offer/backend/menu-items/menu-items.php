<?php

return [
    'label' => Yii::t('back/offer', 'Offer'),
    'items' => [
        [
            'label' => Yii::t('back/offer', 'Offer category'),
            'url' => ['/offer/offer-category/index'],
        ],
        [
            'label' => Yii::t('back/offer', 'Offer'),
            'url' => ['/offer/offer/index'],
        ],
    ]
];
