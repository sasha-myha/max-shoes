<?php

namespace common\models;

use common\components\model\ActiveRecord;
use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;

/**
 * This is the model class for table "{{%offer_category}}".
 *
 * @property integer $id
 * @property string  $label
 * @property string  $alias
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class OfferCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_category}}';
    }

    /**
     * List of attributes to translate
     *
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return ['label'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(OfferCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo'           => [
                'class' => MetaTagBehavior::className(),
            ],
        ];
    }
}
