<?php

namespace frontend\modules\offer\models;

use common\components\Translate;
use common\models\OfferToOfferCategory;
use frontend\modules\offer\components\OfferUrlHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class Offer
 *
 * @package frontend\modules\offer\models
 *
 * @property OfferCategory[] $categories
 */
class Offer extends \common\models\Offer
{
    use Translate;

    /**
     * Method returns offer for given alias
     * It is possible to narrow scope only to active offers with second optional parameter
     *
     * @param string $alias
     * @param bool   $active
     *
     * @return Offer | null
     */
    public static function getOfferByAlias($alias, $active = false)
    {
        $query = static::_getSearchQuery()
            ->andWhere(['t.alias' => $alias])
            ->with(['categories']);

        if ($active) {
            $time = time();
            $query->andWhere(['>', 't.end_date', $time]);
            $query->andWhere(['<', 't.begin_date', $time]);
        }

        return $query->one();
    }

    /**
     * Method returns ActiveDataProvider for published offers
     *
     * @param null $categoryId specifies category of requested offers
     * @param bool $active     narrows search scope only to active offers
     * @param int  $pageSize   specify entities on a single page
     *
     * @return \yii\data\ActiveDataProvider
     */
    public static function getOffersDataProvider($categoryId = null, $active = false, $pageSize = 10)
    {
        $query = static::_getSearchQuery()
            ->joinWith(['categories']);

        $query->andFilterWhere(['categories.id' => $categoryId]);

        if ($active) {
            $time = time();
            $query->andWhere(['>', 't.end_date', $time]);
            $query->andWhere(['<', 't.begin_date', $time]);
        }

        $query->distinct();

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => ['pageSize' => $pageSize],
        ]);
    }

    /**
     * Basic search query for published offers
     *
     * @return ActiveQuery
     */
    private static function _getSearchQuery()
    {
        return static::find()
            ->alias('t')
            ->where(['t.published' => true])
            ->orderBy(['t.created_at' => SORT_DESC]);
    }

    /**
     * Method returns url to offer view page
     *
     * @return string
     */
    public function getOfferUrl()
    {
        return OfferUrlHelper::getOfferUrl(['offerUrl' => $this->alias]);
    }

    /**
     * Relation to offer categories
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(OfferCategory::className(), ['id' => 'offer_category_id'])
            ->alias('categories')
            ->andWhere(['categories.published' => true])
            ->orderBy(['categories.position' => SORT_ASC])
            ->viaTable(
                OfferToOfferCategory::tableName(),
                ['offer_id' => 'id']
            );
    }

    /**
     * Method returns created date converted to given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getCreatedDate($format = 'dd.MM.yyyy HH:mm')
    {
        return $this->_convertTimestampToString($this->created_at, $format);
    }

    /**
     * Method returns begin date converted to given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getBeginDate($format = 'dd.MM.yyyy HH:mm')
    {
        return $this->_convertTimestampToString($this->begin_date, $format);
    }

    /**
     * Method returns end date converted to given format
     *
     * @param string $format
     *
     * @return string
     */
    public function getEndDate($format = 'dd.MM.yyyy HH:mm')
    {
        return $this->_convertTimestampToString($this->end_date, $format);
    }

    /**
     * Method converts timestamp date value to its string representation
     *
     * @param int    $timestamp
     * @param string $format
     *
     * @return string
     */
    private function _convertTimestampToString($timestamp, $format)
    {
        return Yii::$app->getFormatter()->asDatetime($timestamp, $format);
    }
}