<?php

namespace frontend\modules\offer;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * offer module definition class
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\offer\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

        $this->viewPath = '@app/themes/basic/modules/offer';
    }
}
