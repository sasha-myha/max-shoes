<?php

namespace frontend\modules\offer\controllers;

use frontend\components\FrontendController;
use frontend\modules\offer\models\Offer;
use frontend\modules\offer\models\OfferCategory;
use Yii;
use yii\web\NotFoundHttpException;

class OfferController extends FrontendController
{
    /**
     * Method renders offer category page for given alias or throws NotFoundHttpException if no category was found
     *
     * @param string $categoryAlias
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewCategory($categoryAlias)
    {
        if (($category = OfferCategory::getCategoryByAlias($categoryAlias)) === null) {
            throw new NotFoundHttpException(Yii::t('front/offer', 'Category_not_found'));
        }

        //todo: render your view here
    }

    /**
     * Method renders offer page for given alias or throws NotFoundHttpException if no offer was found
     *
     * @param string $offerAlias
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionViewOffer($offerAlias)
    {
        if (($offer = Offer::getOfferByAlias($offerAlias)) === null) {
            throw new NotFoundHttpException(Yii::t('front/offer', 'Offer_not_found'));
        }

        //todo: render your view here
    }
}