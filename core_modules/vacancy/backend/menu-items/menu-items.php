<?php

return [
    'label' => Yii::t('back/vacancy', 'vacancy'),
    'items' => [
        [
            'label' => Yii::t('back/vacancy', 'vacancy_static_content'),
            'url' => ['/vacancy/vacancy-static-content/update'],
        ],
        [
            'label' => Yii::t('back/vacancy', 'vacancy_category'),
            'url' => ['/vacancy/vacancy-category/index'],
        ],
        [
            'label' => Yii::t('back/vacancy', 'vacancy'),
            'url' => ['/vacancy/vacancy/index'],
        ],
        [
            'label' => Yii::t('back/vacancy', 'vacancy_request'),
            'url' => ['/vacancy/vacancy-request/index'],
        ],
    ]
];
