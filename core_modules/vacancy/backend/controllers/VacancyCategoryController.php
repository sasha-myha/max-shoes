<?php

namespace backend\modules\vacancy\controllers;

use backend\components\BackendController;
use backend\modules\vacancy\models\VacancyCategory;

/**
 * VacancyController implements the CRUD actions for Vacancy model.
 */
class VacancyCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return VacancyCategory::className();
    }
}
