<?php

namespace frontend\modules\vacancy\models;

use common\components\model\Model;
use common\models\VacancyRequest;
use Yii;


/**
 * Class VacancyRequestForm
 *
 * @package frontend\modules\vacancy\models
 */
class VacancyRequestForm extends Model
{
    /**
     * @var int
     */
    public $vacancy_id;
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $content;

    /**
     * @var int
     */
    public $file_id;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string', 'max' => 100],
            [['content'], 'string'],
            [['vacancy_id'], 'integer'],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('front/vacancy_request_form', 'name'),
            'email' => Yii::t('front/vacancy_request_form', 'email'),
            'content' => Yii::t('front/vacancy_request_form', 'content'),
            'file_id' => Yii::t('front/vacancy_request_form', 'file'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'file' => [
                'class' => \metalguardian\fileProcessor\behaviors\UploadBehavior::className(),
                'attribute' => 'file_id',
                'required' => false,
                'validator' => [
                    'extensions' => ['png', 'gif', 'jpg', 'jpeg', 'doc', 'docx', 'txt', 'rtf'],
                    'maxSize' => 204800
                ]
            ],
        ];
    }


    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $model = new VacancyRequest();
            $attributes = $this->getAttributes();
            foreach ($attributes as $attribute => $value) {
                if ($model->hasAttribute($attribute)) {
                    $model->$attribute = $value;
                }
            }

            return $model->save(false);
        }

        return false;
    }
}
