<?php

namespace common\models;

use common\components\ConfigurationComponent;
use common\models\base\StaticPage;
use Yii;

/**
 * This is class for static page model.
 */
class VacancyStaticContent extends StaticPage
{
    const TITLE = 'vacancyStaticContentTitle';
    const CONTENT = 'vacancyStaticContentContent';

	const SAVE_ATTRIBUTE_IMAGES = 'VacancyStaticContentImages';

    public $title;
    public $content;

    /**
    * @return array
    */
    public function getKeys()
    {
        return [
            self::TITLE,
            self::CONTENT,
        ];
    }

    /**
    * @return $this
    */
    public function get()
    {
        /** @var ConfigurationComponent $config */
        $config = Yii::$app->config;
        $this->title = $config->get(self::TITLE);
        $this->content = $config->get(self::CONTENT);

        return $this;
    }
}
