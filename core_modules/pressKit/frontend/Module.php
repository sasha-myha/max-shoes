<?php

namespace frontend\modules\pressKit;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * pressKit module definition class
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\pressKit\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
