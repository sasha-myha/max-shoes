<?php

namespace common\models;

use creocoder\translateable\TranslateableBehavior;
use notgosu\yii2\modules\metaTag\components\MetaTagBehavior;

/**
 * This is the model class for table "{{%video}}".
 *
 * @property integer                        $id
 * @property integer                        $album_id
 * @property string                         $label
 * @property string                         $alias
 * @property string                         $description
 * @property string                         $content
 * @property string                         $video_link
 * @property integer                        $published
 * @property integer                        $position
 * @property integer                        $created_at
 * @property integer                        $updated_at
 *
 * @property VideoGalleryVideoTranslation[] $translations
 */
class VideoGalleryVideo extends \common\components\model\ActiveRecord
{
    const VIDEO_IMAGE = 'video-image';
    const VIDEO_WEBM = 'video-webm';
    const VIDEO_MP4 = 'video-mp4';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%video}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'content',
            'video_link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(VideoGalleryVideoTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class'                 => TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo'           => ['class' => MetaTagBehavior::className()],
        ];
    }
}
