<?php

namespace backend\modules\videoGallery\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VideoAlbumSearch represents the model behind the search form about `VideoAlbum`.
 */
class VideoAlbumSearch extends VideoAlbum
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'published', 'position'], 'integer'],
            [['label', 'alias', 'description', 'content', 'author', 'createdAtString'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VideoAlbumSearch::find()
            ->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parent_id' => $this->parent_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        if ($this->createdAtString) {
            $createdDate = new \DateTime($this->createdAtString);

            $query->andFilterWhere(['>', 't.created_at', $createdDate->getTimestamp()]);

            $createdDate->modify('+ 1 day');
            $query->andFilterWhere(['<', 't.created_at', $createdDate->getTimestamp()]);
        }

        return $dataProvider;
    }
}
