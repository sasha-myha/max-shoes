<?php

namespace backend\modules\videoGallery\models;

use backend\components\BackendModel;
use backend\components\ImperaviContent;
use backend\components\TranslateableTrait;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\components\model\Translateable;
use kartik\widgets\DateTimePicker;
use metalguardian\formBuilder\ActiveFormBuilder;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 *
 * @property VideoAlbum $album
 */
class Video extends \common\models\VideoGalleryVideo implements BackendModel, Translateable
{
    use TranslateableTrait;

    // Date formats used for this model
    const DATETIME_FORMAT_FOR_FORM_PAGE = 'dd.MM.yyyy HH:mm';
    const DATE_FORMAT_FOR_INDEX_PAGE = 'dd.MM.yyyy';

    // String representation of timestamp date values
    public $createdAtString;
    // Attribute for image upload
    public $imageUpload;
    // Attribute for webm upload
    public $webmUpload;
    // Attribute for mp4 upload
    public $mp4Upload;

    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = Yii::$app->getSecurity()->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'published', 'position'], 'integer'],
            [['label'], 'required'],
            [['description', 'content', 'video_link'], 'string'],
            [['label', 'alias'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'targetClass' => VideoAlbum::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'safe'],
            [
                ['createdAtString'],
                'date',
                'format' => static::DATETIME_FORMAT_FOR_FORM_PAGE,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('back/video', 'ID'),
            'album_id'        => Yii::t('back/video', 'Album'),
            'label'           => Yii::t('back/video', 'Label'),
            'alias'           => Yii::t('back/video', 'Alias'),
            'description'     => Yii::t('back/video', 'Description'),
            'content'         => Yii::t('back/video', 'Content'),
            'video_link'      => Yii::t('back/video', 'Video link'),
            'published'       => Yii::t('back/video', 'Published'),
            'position'        => Yii::t('back/video', 'Position'),
            'createdAtString' => Yii::t('back/video', 'Created date'),
            'imageUpload'     => Yii::t('back/video', 'Image upload'),
            'webmUpload'      => Yii::t('back/video', 'Webm upload'),
            'mp4Upload'       => Yii::t('back/video', 'Mp4 upload'),
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->createdAtString = $this->_convertTimestampToString($this->created_at);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            ['timestamp' => ['class' => TimestampBehavior::className()]]
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(VideoAlbum::className(), ['id' => 'album_id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return Yii::t('back/video', 'Videos');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    'label',
                    [
                        'attribute' => 'album_id',
                        'value' => function (Video $data) {
                            return $data->album->label ?? false;
                        },
                        'filter' => VideoAlbum::getItems(),
                    ],
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    [
                        'attribute' => 'album_id',
                        'value'     => $this->album->label ?? false,
                    ],
                    'label',
                    'alias',
                    [
                        'attribute' => 'description',
                        'format'    => 'html',
                    ],
                    [
                        'attribute' => 'content',
                        'format'    => 'html',
                    ],
                    'video_link:url',
                    'createdAtString',
                    'published:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new VideoSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        $config = [
            'imageUpload'     => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'                 => $this,
                    'attribute'             => 'imageUpload',
                    'saveAttribute'         => static::VIDEO_IMAGE,
                    'multiple'              => false,
                    'allowedFileExtensions' => ['jpeg', 'png', 'jpg'],
                ]),
            ],
            'album_id'        => [
                'type'    => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items'   => VideoAlbum::getItems(),
                'options' => ['prompt' => Yii::t('back/video', 'Select album')],
            ],
            'label'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_name form-control' : 'form-control',
                ],
            ],
            'alias'           => [
                'type'    => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,
                    'class'     => $this->isNewRecord ? 's_alias form-control' : 'form-control',
                ],
            ],
            'description'     => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'content'         => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'mp4Upload'       => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'                 => $this,
                    'attribute'             => 'mp4Upload',
                    'saveAttribute'         => static::VIDEO_MP4,
                    'multiple'              => false,
                    'allowedFileExtensions' => ['mp4'],
                ]),
            ],
            'webmUpload'      => [
                'type'  => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                    'model'                 => $this,
                    'attribute'             => 'webmUpload',
                    'saveAttribute'         => static::VIDEO_WEBM,
                    'multiple'              => false,
                    'allowedFileExtensions' => ['webm'],
                ]),
            ],
            'video_link'      => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => ImperaviContent::className(),
            ],
            'createdAtString' => [
                'type'        => ActiveFormBuilder::INPUT_WIDGET,
                'widgetClass' => DateTimePicker::className(),
                'options'     => [
                    'pluginOptions' => [
                        'format'         => 'dd.mm.yyyy hh:ii',
                        'todayHighlight' => true,
                        'default'        => time(),
                    ],
                ],
            ],
            'published'       => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position'        => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'sign'            => [
                'type'  => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
        ];

        return $config;
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->created_at = $this->_convertStringToTimestamp($this->createdAtString);

            return true;
        }

        return false;
    }

    /**
     * Method converts timestamp date value from DB to string representation
     *
     * @param int $timestamp
     *
     * @return string
     */
    private function _convertTimestampToString($timestamp): string
    {
        return Yii::$app->getFormatter()->asDatetime(
            $timestamp ?? time(),
            static::DATETIME_FORMAT_FOR_FORM_PAGE
        );
    }

    /**
     * Method accepts string date representation and converts it to timestamp value
     * If no date value given then method return current time in timestamp
     *
     * @param string | null $date
     *
     * @return int
     */
    private function _convertStringToTimestamp($date): int
    {
        return $date
            ? Yii::$app->getFormatter()->asTimestamp($date)
            : time();
    }
}
