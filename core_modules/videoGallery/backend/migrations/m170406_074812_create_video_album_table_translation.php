<?php

use console\components\Migration;

/**
 * Class m170406_074812_create_video_album_table_translation migration
 */
class m170406_074812_create_video_album_table_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%video_album_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%video_album}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label'       => $this->string()->defaultValue(null)->comment('Label'),
                'description' => $this->text()->defaultValue(null)->comment('Description'),
                'content'     => $this->text()->defaultValue(null)->comment('Content'),
                'author'      => $this->string()->defaultValue(null)->comment('Author'),
            ],
            $this->tableOptions
        );


        $this->addPrimaryKey('pk-video_album_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-video_album_translation-model_id-video_album-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

