<?php

namespace backend\modules\videoGallery\controllers;

use backend\components\BackendController;
use backend\modules\videoGallery\models\Video;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Video::className();
    }
}
