<?php

namespace frontend\modules\videoGallery\controllers;

use frontend\components\FrontendController;
use frontend\modules\videoGallery\models\VideoAlbum;
use yii\web\NotFoundHttpException;
use Yii;

class VideoAlbumController extends FrontendController
{
    /**
     * Method renders list of video albums
     *
     * @return string
     */
    public function actionList()
    {
        $dataProvider = VideoAlbum::getAlbumDataProvider();

        //TODO: render this list
    }

    /**
     * Method renders video album for given alias
     *
     * @param string $albumAlias
     *
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionView(string $albumAlias)
    {
        if (!($model = VideoAlbum::getAlbumByAlias($albumAlias))) {
            throw new NotFoundHttpException(Yii::t('front/videoAlbum', 'Video_album_not_found.'));
        }

        //TODO: render this album
    }
}