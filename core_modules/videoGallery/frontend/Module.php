<?php

namespace frontend\modules\videoGallery;

use common\components\interfaces\CoreModuleFrontendInterface;

/**
 * videoGallery module definition class
 */
class Module extends \yii\base\Module implements CoreModuleFrontendInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\videoGallery\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

        $this->viewPath = '@app/themes/basic/modules/videoGallery';

    }
}
