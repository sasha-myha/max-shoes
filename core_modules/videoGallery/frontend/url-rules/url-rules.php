<?php

return [
    'video/albums/<albumAlias:([a-z0-9\-]+)>' => 'videoGallery/video-album/view',
    'video/albums'                            => 'videoGallery/video-album/list',

    'video/video/<videoAlias:([a-z0-9\-]+)>' => 'videoGallery/video/view',
    'video/video'                            => 'videoGallery/video/list',
];
